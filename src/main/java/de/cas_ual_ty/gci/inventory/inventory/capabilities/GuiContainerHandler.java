package de.cas_ual_ty.gci.inventory.inventory.capabilities;



import de.cas_ual_ty.gci.inventory.inventory.ContainerCont;
import de.cas_ual_ty.gci.inventory.inventory.GUIContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import static de.cas_ual_ty.gci.inventory.inventory.capabilities.ContainerCapabilityEventHandler.CONTAINER_CAP;

public class GuiContainerHandler implements IGuiHandler {

    public static final int INVENTORY_GUI_ID = 0;


    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        //Обьект инвентаря храниться в КАПе, значит будем брать его из нее
        ICAPContainer inv = player.getHeldItemMainhand().getCapability(CAPContainerProvider.CONTAINER_CAP, null);
        if(ID == INVENTORY_GUI_ID && inv != null) {
            return new ContainerCont(player.inventory, inv.getInventory(), player);
        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        //Обьект инвентаря храниться в КАПе, значит будем брать его из нее
        ICAPContainer inv = player.getHeldItemMainhand().getCapability(CAPContainerProvider.CONTAINER_CAP, null);
        if(ID == INVENTORY_GUI_ID  && inv != null) {
            return new GUIContainer(player, player.inventory, inv.getInventory());
        }
        return null;
    }

}

