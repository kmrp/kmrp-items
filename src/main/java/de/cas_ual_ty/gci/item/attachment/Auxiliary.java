package de.cas_ual_ty.gci.item.attachment;

import de.cas_ual_ty.gci.item.ItemToggleable;

public class Auxiliary extends Attachment
{
	protected float driftModifierWhenShiftAndStill;
	protected float inaccuracyModifierWhenShiftAndStill;
	protected boolean isAllowingReloadWhileZoomed;
	protected int extraFireRate;
	protected Laser laser;
	protected boolean flashActive;
	protected Integer toggleableToId;
	protected String transferableToRl;




	public Auxiliary(int id, String rl)
	
	{
		super(id, rl);
		
		this.driftModifierWhenShiftAndStill = 1F;
		this.inaccuracyModifierWhenShiftAndStill = 1F;
		this.isAllowingReloadWhileZoomed = false;
		this.extraFireRate = 0;
		this.laser = null;
		this.flashActive = false;
		this.toggleableToId = null;
		this.transferableToRl = null;
	}
	
	@Override
	public EnumAttachmentType getType()
	{
		return EnumAttachmentType.AUXILIARY;
	}
	
	public float getDriftModifierWhenShiftAndStill()
	{
		return this.driftModifierWhenShiftAndStill;
	}
	
	public float getInaccuracyModifierWhenShiftAndStill()
	{
		return this.inaccuracyModifierWhenShiftAndStill;
	}
	
	public boolean getIsAllowingReloadWhileZoomed()
	{
		return this.isAllowingReloadWhileZoomed;
	}
	
	public Auxiliary setDriftModifierWhenShiftAndStill(float driftModifierWhenShiftAndStill)
	{
		this.driftModifierWhenShiftAndStill = driftModifierWhenShiftAndStill;
		return this;
	}
	
	public Auxiliary setInaccuracyModifierWhenShiftAndStill(float inaccuracyModifierWhenShiftAndStill)
	{
		this.inaccuracyModifierWhenShiftAndStill = inaccuracyModifierWhenShiftAndStill;
		return this;
	}
	
	public Auxiliary setIsAllowingReloadWhileZoomed(boolean isAllowingReloadWhileZoomed)
	{
		this.isAllowingReloadWhileZoomed = isAllowingReloadWhileZoomed;
		return this;
	}

	public Auxiliary setToggleableTo(Integer id) {
		this.toggleableToId = id;
		return this;
	}

	public Integer getToggleableTo() {
		return this.toggleableToId;
	}

	public Auxiliary setTransferableToRl(String id) {
		this.transferableToRl = id;
		return this;
	}


	public String getTransferableToRl() {
		return this.transferableToRl;
	}

	public Laser getLaser()
	{
		return this.laser;
	}

	public Auxiliary setLaser(Laser laser)
	{
		this.laser = laser;
		return this;
	}

	public static class Laser
	{
		protected float r;
		protected float g;
		protected float b;

		protected double maxRange;

		protected boolean isBeam;
		protected boolean isPoint;
		protected boolean isRangeFinder;

		public Laser(float r, float g, float b, double maxRange, boolean isBeam, boolean isPoint, boolean isRangeFinder)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.maxRange = maxRange;
			this.isBeam = isBeam;
			this.isPoint = isPoint;
			this.isRangeFinder = isRangeFinder;
		}

		public float getR()
		{
			return this.r;
		}

		public float getG()
		{
			return this.g;
		}

		public float getB()
		{
			return this.b;
		}

		public double getMaxRange()
		{
			return this.maxRange;
		}

		public boolean isBeam()
		{
			return this.isBeam;
		}

		public boolean isPoint()
		{
			return this.isPoint;
		}

		public boolean isRangeFinder()
		{
			return this.isRangeFinder;
		}
	}

	public boolean isFlashActive() {return this.flashActive;}
	public Auxiliary setFlashActive()
	{
		this.flashActive = true;
		return this;
	}
}
