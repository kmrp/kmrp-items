package de.cas_ual_ty.gci.item.attachment;

public class Underbarrel extends Attachment
{
	protected float driftModifier;
	protected float inaccuracyModifierMoving;
	protected float inaccuracyModifierStill;
	protected Laser laser;
	protected boolean flashActive;
	protected Integer toggleableToId;
	protected String transferableToRl;

	public Underbarrel(int id, String rl)
	{
		super(id, rl);
		
		this.driftModifier = 1F;
		this.inaccuracyModifierMoving = 1F;
		this.inaccuracyModifierStill = 1F;
		this.laser = null;
		this.flashActive = false;
		this.toggleableToId = null;
		this.transferableToRl = null;
	}
	
	@Override
	public EnumAttachmentType getType()
	{
		return EnumAttachmentType.UNDERBARREL;
	}
	
	public float getDriftModifier()
	{
		return this.driftModifier;
	}
	
	public float getInaccuracyModifierMoving()
	{
		return this.inaccuracyModifierMoving;
	}
	
	public float getInaccuracyModifierStill()
	{
		return this.inaccuracyModifierStill;
	}
	
	public Underbarrel setDriftModifier(float driftModifier)
	{
		this.driftModifier = driftModifier;
		return this;
	}
	
	public Underbarrel setInaccuracyModifierMoving(float inaccuracyModifierMoving)
	{
		this.inaccuracyModifierMoving = inaccuracyModifierMoving;
		return this;
	}
	
	public Underbarrel setInaccuracyModifierStill(float inaccuracyModifierStill)
	{
		this.inaccuracyModifierStill = inaccuracyModifierStill;
		return this;
	}

	public Underbarrel setToggleableTo(Integer id) {
		this.toggleableToId = id;
		return this;
	}

	public Integer getToggleableTo() {
		return this.toggleableToId;
	}

	public Underbarrel setTransferableToRl(String id) {
		this.transferableToRl = id;
		return this;
	}


	public String getTransferableToRl() {
		return this.transferableToRl;
	}


	public Laser getLaser()
	{
		return this.laser;
	}

	public Underbarrel setLaser(Laser laser)
	{
		this.laser = laser;
		return this;
	}

	public static class Laser
	{
		protected float r;
		protected float g;
		protected float b;

		protected double maxRange;

		protected boolean isBeam;
		protected boolean isPoint;
		protected boolean isRangeFinder;

		public Laser(float r, float g, float b, double maxRange, boolean isBeam, boolean isPoint, boolean isRangeFinder)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.maxRange = maxRange;
			this.isBeam = isBeam;
			this.isPoint = isPoint;
			this.isRangeFinder = isRangeFinder;
		}

		public float getR()
		{
			return this.r;
		}

		public float getG()
		{
			return this.g;
		}

		public float getB()
		{
			return this.b;
		}

		public double getMaxRange()
		{
			return this.maxRange;
		}

		public boolean isBeam()
		{
			return this.isBeam;
		}

		public boolean isPoint()
		{
			return this.isPoint;
		}

		public boolean isRangeFinder()
		{
			return this.isRangeFinder;
		}
	}

	public boolean isFlashActive() {return this.flashActive;}
	public Underbarrel setFlashActive()
	{
		this.flashActive = true;
		return this;
	}

}
