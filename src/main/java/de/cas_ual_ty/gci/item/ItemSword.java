package de.cas_ual_ty.gci.item;


import de.cas_ual_ty.gci.GunCus;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;

import javax.annotation.Nullable;

public class ItemSword extends ItemGCI {



    private String modelRL;

    public ItemSword(String rl)
    {
        this(rl, null);
    }

    public ItemSword(String rl, @Nullable CreativeTabs tab)
    {
        super(rl, GunCus.KMRP_TAB);
        this.setMaxStackSize(1);
    }

}
