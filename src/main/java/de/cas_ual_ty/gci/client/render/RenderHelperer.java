package de.cas_ual_ty.gci.client.render;

import de.cas_ual_ty.gci.client.EventHandlerClient;
import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.Vector3d;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

import javax.vecmath.Vector2d;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RenderHelperer
{
    public static Function<Double, Vec3d> curve;
    public static List<Vec3d> path;
    public static Vec3d theBlock;

    public static void debugDrawing(Entity entity, float partialTicks, World world)
    {
        GlStateManager.pushMatrix();
        translateToZeroCoord(Minecraft.getMinecraft().getRenderPartialTicks());
        GlStateManager.disableLighting();
        GlStateManager.depthMask(false);
        GlStateManager.glLineWidth(2.0F);
        GlStateManager.disableTexture2D();

        path = makePath(entity, 5, 2);

        drawPolyChain(path, Color.green);

        drawCube(theBlock);

        GlStateManager.enableLighting();
        GlStateManager.depthMask(true);
        GlStateManager.enableTexture2D();



        GlStateManager.popMatrix();
    }

    public static void translateToZeroCoord(float partialTicks)
    {
        Entity player = Minecraft.getMinecraft().player;
        double x = player.lastTickPosX + (player.posX - player.lastTickPosX) * partialTicks;
        double y = player.lastTickPosY + (player.posY - player.lastTickPosY) * partialTicks;
        double z = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * partialTicks;
        GlStateManager.translate(-x, -y, -z);
    }

    public static void drawLine(Vec3d first, Color color, BufferBuilder buffer)
    {
        buffer.pos(first.x, first.y, first.z).color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()).endVertex();
    }

    private static void drawCube(Vec3d theBlock) {
        if (theBlock == null) return;
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder b = tessellator.getBuffer();
        double x = theBlock.x;
        double y = theBlock.y;
        double z = theBlock.z;

        b.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION);

        b.pos(x + 0.25D, y + 0.25D, z + 0.25D).endVertex();
        b.pos(x + 0.25D, y + 0.25D, z - 0.25D).endVertex();
        b.pos(x - 0.25D, y + 0.25D, z - 0.25D).endVertex();
        b.pos(x - 0.25D, y + 0.25D, z + 0.25D).endVertex();

        b.pos(x + 0.25D, y - 0.25D, z + 0.25D).endVertex();
        b.pos(x - 0.25D, y - 0.25D, z + 0.25D).endVertex();
        b.pos(x - 0.25D, y - 0.25D, z - 0.25D).endVertex();
        b.pos(x + 0.25D, y - 0.25D, z - 0.25D).endVertex();

        b.pos(x + 0.25D, y - 0.25D, z + 0.25D).endVertex();
        b.pos(x + 0.25D, y - 0.25D, z - 0.25D).endVertex();
        b.pos(x + 0.25D, y + 0.25D, z - 0.25D).endVertex();
        b.pos(x + 0.25D, y + 0.25D, z + 0.25D).endVertex();

        b.pos(x - 0.25D, y - 0.25D, z + 0.25D).endVertex();
        b.pos(x - 0.25D, y + 0.25D, z + 0.25D).endVertex();
        b.pos(x - 0.25D, y + 0.25D, z - 0.25D).endVertex();
        b.pos(x - 0.25D, y - 0.25D, z - 0.25D).endVertex();

        b.pos(x - 0.25D, y - 0.25D, z + 0.25D).endVertex();
        b.pos(x + 0.25D, y - 0.25D, z + 0.25D).endVertex();
        b.pos(x + 0.25D, y + 0.25D, z + 0.25D).endVertex();
        b.pos(x - 0.25D, y + 0.25D, z + 0.25D).endVertex();

        b.pos(x - 0.25D, y - 0.25D, z - 0.25D).endVertex();
        b.pos(x - 0.25D, y + 0.25D, z - 0.25D).endVertex();
        b.pos(x + 0.25D, y + 0.25D, z - 0.25D).endVertex();
        b.pos(x + 0.25D, y - 0.25D, z - 0.25D).endVertex();

        tessellator.draw();
    }


    public static void drawPolyChain(List<Vec3d> path, Color color)
    {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        buffer.begin(3, DefaultVertexFormats.POSITION_COLOR);
        int num = 0;
        for (Vec3d vec3d : path) {
            switch (num) {
                case 0:
                    color = Color.GRAY;
                    break;
                case 1:
                    color = Color.GREEN;
                    break;
                case 2:
                    color = Color.YELLOW;
                    break;
                case 3:
                    color = Color.BLUE;
                    break;
                case 4:
                    color = Color.RED;
                    break;
                case 5:
                    color = Color.PINK;
                    break;
                case 6:
                    color = Color.CYAN;
                    break;
                case 7:
                    color = Color.ORANGE;
                    break;
                default:
                    if (num % 2 == 0) color = Color.BLACK;
                    else color = Color.WHITE;
            }
            drawLine(vec3d, color, buffer);
            num++;
            if (num > 7) num = 1;
        }
        tessellator.draw();
    }
    public static Vec3d rotateVectorAroundY(Vec3d vector, double degrees) {
        double rad = Math.toRadians(degrees);

        double currentX = vector.x;
        double currentZ = vector.z;

        double cosine = Math.cos(rad);
        double sine = Math.sin(rad);

        return new Vec3d((cosine * currentX - sine * currentZ), vector.y, (sine * currentX + cosine * currentZ));
    }

    public static List<Vec3d> makePath(Entity entityPlayer, double initialRange, int initialloss) {
        return makePath(entityPlayer, 0, 0, initialRange, initialloss);
    }

    public static List<Vec3d> makePath(Entity entityPlayer, double pitchoffset, double yawoffset, double initialRange, int initialloss)
    {
        double pitch = entityPlayer.rotationPitch*-1 + pitchoffset;
        double yaw  = entityPlayer.rotationYaw+90 + yawoffset;
        if (yaw > 180) yaw -= 360;
        if (yaw < -180) yaw += 360;
        if (pitch > 90) pitch = 90;
        if (pitch < -90) pitch = -90;
        Vec3d start = new Vec3d(entityPlayer.posX, entityPlayer.posY + entityPlayer.getEyeHeight(), entityPlayer.posZ).add(EventHandlerClient.getOffsetForHand((EntityPlayer) entityPlayer, EnumHand.MAIN_HAND));
        List<Vec3d> r = new ArrayList<>();
        r.add(start);
        Vec3d prevStep = new Vec3d(start.x, start.y, start.z);
        double range = initialRange;
        int loss = initialloss;
        double y = 0;
        int cycle = 0;
        BlockPos blockPos = null;
        int lastHitBlock = 0;
        BlockPos lastHitBlockPos = null;
        double distance = 0;
        double lastDistance = 0;
        int blocksHit = 0;
        int counter = 0;
        while (y > -64 && cycle < 512) {
            cycle++;
            if (range != 0.01) {
                if (pitch <= -45) {
                    range = (double) (initialRange * 1.5);
                }
                if (pitch <= -90) {
                    pitch = -90;
                    range = initialRange * 2;
                }
            }
            if (pitch <= -90) {
                pitch = -90;
            }
            if (yaw > 180) yaw -= 360;
            else if (yaw < -180) yaw += 360;

            if (distance > 50) {
                EventHandlerClient.rangeD = (int) distance;
                theBlock = prevStep;
                return r;
            }

            Vec3d step = new Vec3d(prevStep.x + range * Math.cos(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch)), prevStep.y + range * Math.sin(Math.toRadians(pitch)), prevStep.z + range * Math.sin(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch)));
            boolean bounced = false;
            if (range == 0.01) {
                range = (double) initialRange / 2;
                bounced = true;
            }
            RayTraceResult rtr = EventHandlerClient.findBlockOnPathWithCollision(entityPlayer.world, null, prevStep, step);

            if (rtr != null && rtr.typeOfHit == RayTraceResult.Type.BLOCK && (blockPos == null || rtr.sideHit == EnumFacing.UP || (blockPos.getX() != rtr.getBlockPos().getX() || blockPos.getY() != rtr.getBlockPos().getY() || blockPos.getZ() != rtr.getBlockPos().getZ()))) {
                blockPos = rtr.getBlockPos();
                //System.out.println(entityPlayer.world.getBlockState(blockPos).getBlock().getUnlocalizedName());
                if (lastHitBlock == 0) {
                    lastHitBlockPos = blockPos;
                    lastDistance = distance + prevStep.distanceTo(rtr.hitVec);
                    switch (rtr.sideHit) {
                        case DOWN:
                            lastHitBlockPos = lastHitBlockPos.down();
                            break;
                        case UP:
                            lastHitBlockPos = lastHitBlockPos.up();
                            break;
                        case WEST:
                            lastHitBlockPos = lastHitBlockPos.west();
                            break;
                        case EAST:
                            lastHitBlockPos = lastHitBlockPos.east();
                            break;
                        case SOUTH:
                            lastHitBlockPos = lastHitBlockPos.south();
                            break;
                        case NORTH:
                            lastHitBlockPos = lastHitBlockPos.north();
                            break;
                    }
                }
                if (prevStep.distanceTo(rtr.hitVec) <= 1.4 && lastHitBlock > 1) {
                    r.add(rtr.hitVec);
                    System.out.println("end");
                    EventHandlerClient.rangeD = (int) lastDistance;
                    theBlock = prevStep;
                    return r;
                }

                //System.out.println("hit");
                lastHitBlock++;

                blocksHit++;
                //System.out.println(blocksHit);
                distance += prevStep.distanceTo(rtr.hitVec);
                switch (rtr.sideHit) {
                    case DOWN:
                        pitch = -5;
                        r.add(rtr.hitVec);
                        break;
                    case UP:
                        r.add(rtr.hitVec);
                        EventHandlerClient.rangeD = (int) distance;
                        theBlock = rtr.hitVec;
                        return r;
                    case WEST:
                    case EAST:
                        yaw = 180 - yaw;
                        if (yaw > 180) yaw -= 360;
                        else if (yaw < -180) yaw += 360;
                        break;
                    case SOUTH:
                    case NORTH:
                        yaw = yaw * -1;
                        break;
                }
                range = 0.01;
                r.add(rtr.hitVec);
                prevStep = rtr.hitVec;
                y = prevStep.y;
                continue;

            } else {
                if ((rtr == null || rtr.typeOfHit != RayTraceResult.Type.BLOCK)) {
                    BlockPos blockPosss = new BlockPos(prevStep.x, prevStep.y, prevStep.z);
                    //System.out.println("unhit " + (rtr == null) + " " + lastHitBlock + " " + entityPlayer.world.getBlockState(blockPosss).getBlock().getUnlocalizedName() + " " + blockPosss + " " + pitch);
                    lastHitBlock = 0;
                    lastHitBlockPos = null;

                }
            }
            r.add(step);
            if (bounced) {
                if (pitch < -75) pitch = -75;
                loss = 15;
            }
            distance += prevStep.distanceTo(step);
            prevStep = step;
            pitch -= loss;
            y = prevStep.y;
        }
        System.out.println(blocksHit);
        System.out.println((int) distance);
        EventHandlerClient.rangeD = (int) distance;
        theBlock = null;
        return r;
    }

    public static int getDistance(Entity entityPlayer, double pitchoffset, double yawoffset) {
        double pitch = entityPlayer.rotationPitch*-1;
        double yaw  = entityPlayer.rotationYaw+90;
        if (yaw > 180) yaw -= 360;
        if (yaw < -180) yaw += 360;
        if (pitch > 90) pitch = 90;
        if (pitch < -90) pitch = -90;
        Vec3d start = new Vec3d(entityPlayer.posX, entityPlayer.posY + entityPlayer.getEyeHeight(), entityPlayer.posZ).add(EventHandlerClient.getOffsetForHand((EntityPlayer) entityPlayer, EnumHand.MAIN_HAND));
        List<Vec3d> r = new ArrayList<>();
        r.add(start);
        Vec3d prevStep = new Vec3d(start.x, start.y, start.z);
        double range = 5;
        int initialRange = 5;
        int loss = 1;
        double y = 0;
        int cycle = 0;
        BlockPos blockPos = null;
        int lastHitBlock = 0;
        BlockPos lastHitBlockPos = null;
        double distance = 0;
        double lastDistance = 0;
        int blocksHit = 0;
        int counter = 0;
        while (y > -64 && cycle < 512) {
            cycle++;
            if (range != 0.01) {
                if (pitch <= -45) {
                    range = (double) (initialRange * 1.5);
                }
                if (pitch <= -90) {
                    pitch = -90;
                    range = initialRange * 2;
                }
            }
            if (pitch <= -90) {
                pitch = -90;
            }
            if (yaw > 180) yaw -= 360;
            else if (yaw < -180) yaw += 360;

            if (distance > 50) {
                EventHandlerClient.rangeD = (int) distance;
                return (int) distance;
            }

            Vec3d step = new Vec3d(prevStep.x + range * Math.cos(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch)), prevStep.y + range * Math.sin(Math.toRadians(pitch)), prevStep.z + range * Math.sin(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch)));
            boolean bounced = false;
            if (range == 0.01) {
                range = (double) initialRange / 2;
                bounced = true;
            }
            RayTraceResult rtr = EventHandlerClient.findBlockOnPathWithCollision(entityPlayer.world, null, prevStep, step);

            if (rtr != null && rtr.typeOfHit == RayTraceResult.Type.BLOCK && (blockPos == null || rtr.sideHit == EnumFacing.UP || (blockPos.getX() != rtr.getBlockPos().getX() || blockPos.getY() != rtr.getBlockPos().getY() || blockPos.getZ() != rtr.getBlockPos().getZ()))) {
                blockPos = rtr.getBlockPos();
                //System.out.println(entityPlayer.world.getBlockState(blockPos).getBlock().getUnlocalizedName());
                if (lastHitBlock == 0) {
                    lastHitBlockPos = blockPos;
                    lastDistance = distance + prevStep.distanceTo(rtr.hitVec);
                    switch (rtr.sideHit) {
                        case DOWN:
                            lastHitBlockPos = lastHitBlockPos.down();
                            break;
                        case UP:
                            lastHitBlockPos = lastHitBlockPos.up();
                            break;
                        case WEST:
                            lastHitBlockPos = lastHitBlockPos.west();
                            break;
                        case EAST:
                            lastHitBlockPos = lastHitBlockPos.east();
                            break;
                        case SOUTH:
                            lastHitBlockPos = lastHitBlockPos.south();
                            break;
                        case NORTH:
                            lastHitBlockPos = lastHitBlockPos.north();
                            break;
                    }
                }
                if (prevStep.distanceTo(rtr.hitVec) <= 1.4 && lastHitBlock > 2) {
                    r.add(rtr.hitVec);
                    System.out.println("end");
                    EventHandlerClient.rangeD = (int) lastDistance;
                    return (int) distance;
                }

                //System.out.println("hit");
                lastHitBlock++;

                blocksHit++;
                //System.out.println(blocksHit);
                distance += prevStep.distanceTo(rtr.hitVec);
                switch (rtr.sideHit) {
                    case DOWN:
                        pitch = -5;
                        r.add(rtr.hitVec);
                        break;
                    case UP:
                        r.add(rtr.hitVec);
                        EventHandlerClient.rangeD = (int) distance;
                        return (int) distance;
                    case WEST:
                    case EAST:
                        yaw = 180 - yaw;
                        if (yaw > 180) yaw -= 360;
                        else if (yaw < -180) yaw += 360;
                        break;
                    case SOUTH:
                    case NORTH:
                        yaw = yaw * -1;
                        break;
                }
                range = 0.01;
                r.add(rtr.hitVec);
                prevStep = rtr.hitVec;
                y = prevStep.y;
                continue;

            } else {
                if ((rtr == null || rtr.typeOfHit != RayTraceResult.Type.BLOCK)) {
                    BlockPos blockPosss = new BlockPos(prevStep.x, prevStep.y, prevStep.z);
                    //System.out.println("unhit " + (rtr == null) + " " + lastHitBlock + " " + entityPlayer.world.getBlockState(blockPosss).getBlock().getUnlocalizedName() + " " + blockPosss + " " + pitch);
                    lastHitBlock = 0;
                    lastHitBlockPos = null;

                }
            }
            r.add(step);
            if (bounced) {
                if (pitch < -75) pitch = -75;
                loss = 15;
            }
            distance += prevStep.distanceTo(step);
            prevStep = step;
            pitch -= loss;
            y = prevStep.y;
        }
        System.out.println(blocksHit);
        System.out.println((int) distance);
        EventHandlerClient.rangeD = (int) distance;
        return (int) distance;
    }

    public static BlockPos getBlockPos(Entity entityPlayer, double pitchoffset, double yawoffset) {
        double pitch = entityPlayer.rotationPitch*-1;
        double yaw  = entityPlayer.rotationYaw+90;
        if (yaw > 180) yaw -= 360;
        if (yaw < -180) yaw += 360;
        if (pitch > 90) pitch = 90;
        if (pitch < -90) pitch = -90;
        Vec3d start = new Vec3d(entityPlayer.posX, entityPlayer.posY + entityPlayer.getEyeHeight(), entityPlayer.posZ).add(EventHandlerClient.getOffsetForHand((EntityPlayer) entityPlayer, EnumHand.MAIN_HAND));
        List<Vec3d> r = new ArrayList<>();
        r.add(start);
        Vec3d prevStep = new Vec3d(start.x, start.y, start.z);
        double range = 5;
        int initialRange = 5;
        int loss = 1;
        double y = 0;
        int cycle = 0;
        BlockPos blockPos = null;
        int lastHitBlock = 0;
        BlockPos lastHitBlockPos = null;
        double distance = 0;
        double lastDistance = 0;
        int blocksHit = 0;
        int counter = 0;
        while (y > -64 && cycle < 512) {
            cycle++;
            if (range != 0.01) {
                if (pitch <= -45) {
                    range = (double) (initialRange * 1.5);
                }
                if (pitch <= -90) {
                    pitch = -90;
                    range = initialRange * 2;
                }
            }
            if (pitch <= -90) {
                pitch = -90;
            }
            if (yaw > 180) yaw -= 360;
            else if (yaw < -180) yaw += 360;

            if (distance > 50) {
                EventHandlerClient.rangeD = (int) distance;
                return new BlockPos(prevStep);
            }

            Vec3d step = new Vec3d(prevStep.x + range * Math.cos(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch)), prevStep.y + range * Math.sin(Math.toRadians(pitch)), prevStep.z + range * Math.sin(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch)));
            boolean bounced = false;
            if (range == 0.01) {
                range = (double) initialRange / 2;
                bounced = true;
            }
            RayTraceResult rtr = EventHandlerClient.findBlockOnPathWithCollision(entityPlayer.world, null, prevStep, step);

            if (rtr != null && rtr.typeOfHit == RayTraceResult.Type.BLOCK && (blockPos == null || rtr.sideHit == EnumFacing.UP || (blockPos.getX() != rtr.getBlockPos().getX() || blockPos.getY() != rtr.getBlockPos().getY() || blockPos.getZ() != rtr.getBlockPos().getZ()))) {
                blockPos = rtr.getBlockPos();
                //System.out.println(entityPlayer.world.getBlockState(blockPos).getBlock().getUnlocalizedName());
                if (lastHitBlock == 0) {
                    lastHitBlockPos = blockPos;
                    lastDistance = distance + prevStep.distanceTo(rtr.hitVec);
                    switch (rtr.sideHit) {
                        case DOWN:
                            lastHitBlockPos = lastHitBlockPos.down();
                            break;
                        case UP:
                            lastHitBlockPos = lastHitBlockPos.up();
                            break;
                        case WEST:
                            lastHitBlockPos = lastHitBlockPos.west();
                            break;
                        case EAST:
                            lastHitBlockPos = lastHitBlockPos.east();
                            break;
                        case SOUTH:
                            lastHitBlockPos = lastHitBlockPos.south();
                            break;
                        case NORTH:
                            lastHitBlockPos = lastHitBlockPos.north();
                            break;
                    }
                }
                if (prevStep.distanceTo(rtr.hitVec) <= 1.4 && lastHitBlock > 1) {
                    r.add(rtr.hitVec);
                    System.out.println("end");
                    EventHandlerClient.rangeD = (int) lastDistance;
                    return lastHitBlockPos;
                }

                //System.out.println("hit");
                lastHitBlock++;

                blocksHit++;
                //System.out.println(blocksHit);
                distance += prevStep.distanceTo(rtr.hitVec);
                switch (rtr.sideHit) {
                    case DOWN:
                        pitch = -5;
                        r.add(rtr.hitVec);
                        break;
                    case UP:
                        r.add(rtr.hitVec);
                        EventHandlerClient.rangeD = (int) distance;
                        return blockPos;
                    case WEST:
                    case EAST:
                        yaw = 180 - yaw;
                        if (yaw > 180) yaw -= 360;
                        else if (yaw < -180) yaw += 360;
                        break;
                    case SOUTH:
                    case NORTH:
                        yaw = yaw * -1;
                        break;
                }
                range = 0.01;
                r.add(rtr.hitVec);
                prevStep = rtr.hitVec;
                y = prevStep.y;
                continue;

            } else {
                if ((rtr == null || rtr.typeOfHit != RayTraceResult.Type.BLOCK)) {
                    BlockPos blockPosss = new BlockPos(prevStep.x, prevStep.y, prevStep.z);
                    //System.out.println("unhit " + (rtr == null) + " " + lastHitBlock + " " + entityPlayer.world.getBlockState(blockPosss).getBlock().getUnlocalizedName() + " " + blockPosss + " " + pitch);
                    lastHitBlock = 0;
                    lastHitBlockPos = null;

                }
            }
            r.add(step);
            if (bounced) {
                if (pitch < -75) pitch = -75;
                loss = 15;
            }
            distance += prevStep.distanceTo(step);
            prevStep = step;
            pitch -= loss;
            y = prevStep.y;
        }
        System.out.println(blocksHit);
        System.out.println((int) distance);
        EventHandlerClient.rangeD = (int) distance;
        return null;
    }

    public static Function<Double, Vec3d> bezierCurve(List<Vec3d> path)
    {
        Vec3d first = path.get(0);
        Vec3d last = path.get(path.size() - 1);
        float angle = (float) Math.atan2(last.x - first.x, last.z - first.z);

        List<Vec3d> rotatedPath = path.stream().map(v -> v.rotateYaw(-angle)).collect(Collectors.toList());

        return t -> {
            double z = 0;
            double y = 0;

            int n = rotatedPath.size() - 1;

            Vec3d vec = rotatedPath.get(0);
            double pow_1_minus_t_n = Math.pow((1 - t), n);
            z += vec.z * pow_1_minus_t_n;
            y += vec.y * pow_1_minus_t_n;

            double factorial_n = factorial(n);

            for (int index = 1; index < rotatedPath.size(); index++) {
                Vec3d item = rotatedPath.get(index);
                z += factorial_n / factorial(index) / factorial(n - index) * item.z * Math.pow((1 - t), n - index) * Math.pow(t, index);
                y += factorial_n / factorial(index) / factorial(n - index) * item.y * Math.pow((1 - t), n - index) * Math.pow(t, index);
            }
            return new Vec3d(vec.x, y, z).rotateYaw(angle);
        };
    }

    public static Int2IntMap factorialCache = new Int2IntOpenHashMap();

    public static double factorial(int num)
    {
        return factorialCache.computeIfAbsent(num, num1 -> factorial(num1, 1));
    }

    public static int factorial(int num, int acc)
    {
        if (num <= 1) {
            return acc;
        } else {
            return factorial(num - 1, acc * num);
        }
    }
}