package de.cas_ual_ty.gci.client.render.layers;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.inventory.CustomInventory;
import de.cas_ual_ty.gci.inventory.capabilities.CAPCustomInventoryProvider;
import de.cas_ual_ty.gci.inventory.capabilities.ICAPCustomInventory;
import de.cas_ual_ty.gci.item.ItemGCI3D;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.item.ItemToggleable;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import noppes.mpm.ModelData;

public class RightShoulderLayer implements LayerRenderer<EntityPlayer> {
    @Override
    public void doRenderLayer(EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {


        ICAPCustomInventory cap = player.getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null);
        CustomInventory inv = cap.getInventory();
        ItemStack is = inv.getStackInSlot(1);
        if (is != ItemStack.EMPTY) {
            GlStateManager.pushMatrix();
            ModelData md = player.getCapability(ModelData.MODELDATA_CAPABILITY, null);
            if (md != null) {

/*
            float addYLegs = playerScales.getCompoundTag("LegsConfig").getFloat("TransY");
*/
                    float addYBody = md.body.transY;
                    float addXArms = md.arm2.transX;
                    /*            float addXBody = TESTING1.getCompoundTag("BodyConfig").getFloat("ScaleX");*/
                    /*float addZBody = TESTING1.getCompoundTag("BodyConfig").getFloat("ScaleZ") - 1F;*/
                    GlStateManager.translate(-addXArms, addYBody, 0.00F);
                    /*GlStateManager.translate(0.00F + addXBody, 0.00F, 0.00F + addZBody);*/
            }
            //Рендер предмета(itemgun)
            if (is.getItem() instanceof ItemGun || is.getItem() instanceof ItemGCI3D || is.getItem() instanceof ItemToggleable) {

/*        //Позиция предмета
        GlStateManager.translate(0.20F, 0.55F, 0.20F);
        //Вращение предмета
        GlStateManager.rotate(135F, 0, 0, 20);
        //Размеры предмета
        GlStateManager.scale(0.85F, 0.85F, 0.85F);*/
/*            GlStateManager.translate(0, 0.35F, 0.25F);
            //Вращение предмета
            GlStateManager.rotate(360F, 0, 0, 20);
            //Размеры предмета
            GlStateManager.scale(1F, 1F, 1F);*/

                GlStateManager.translate(-0.37F, 0.35F, 0.25F); //менять x чтобы двигать левее-правее

                if (GunCus.GUN_HMG.equals(is.getItem())) {
                    GlStateManager.translate(0F, 0F, 0.1F);
                }

                if (GunCus.GUN_GL.equals(is.getItem())) {
                    GlStateManager.translate(0F, 0F, -0.1F);
                }

                if (GunCus.GUN_VYKHLOP.equals(is.getItem())) {
                    GlStateManager.translate(0F, 0F, 0.05F);
                }

                if (GunCus.GUN_GL.equals(is.getItem())) {
                    GlStateManager.translate(0F, 0F, -0.05F);
                }


                GlStateManager.rotate(90F, 0, 1, 0);
                GlStateManager.rotate(-45F, 0, 0, 20);
                GlStateManager.rotate(-180F, 0, 0, 20);
                GlStateManager.rotate(180F, 1, 0, 0);
                GlStateManager.rotate(90F, 0, 0, 1);

                //Размеры предмета
                GlStateManager.scale(1F, 1F, 1F);

                //Условие: Если игрок присел, то мы меняем положение нашего колчана.
                if (player.isSneaking()) {
                    GlStateManager.rotate(-30F, 0, 0, 1);
                    GlStateManager.translate(0, -0.20F, 0F);
                }
                Minecraft.getMinecraft().getRenderItem().renderItem(is, player, ItemCameraTransforms.TransformType.FIXED, false);
            } /*else if (!is.getItem().isFull3D()) {
            GlStateManager.translate(0, 0.25F, 0.15F);
            //Вращение предмета
            GlStateManager.rotate(180F, 0, 0, 20);
            //Размеры предмета
            GlStateManager.scale(0.85F, 0.85F, 0.85F);

            //Условие: Если игрок присел, то мы меняем положение нашего колчана.
            if (player.isSneaking()) {
                GlStateManager.rotate(-30F, 1, 0, 0);
                GlStateManager.translate(0, -0.155F, 0.04F);
            }
            Minecraft.getMinecraft().getRenderItem().renderItem(is, player, ItemCameraTransforms.TransformType.FIXED, false);
        }*/
            GlStateManager.popMatrix();
        }
    }

    @Override
    public boolean shouldCombineTextures() {
        return false;
    }
}
