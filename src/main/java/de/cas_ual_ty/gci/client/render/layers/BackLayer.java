package de.cas_ual_ty.gci.client.render.layers;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.client.EventHandlerClient;
import de.cas_ual_ty.gci.inventory.CustomInventory;
import de.cas_ual_ty.gci.inventory.capabilities.CAPCustomInventoryProvider;
import de.cas_ual_ty.gci.inventory.capabilities.ICAPCustomInventory;
import de.cas_ual_ty.gci.item.ItemGCI3D;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.item.ItemShield;
import de.cas_ual_ty.gci.item.ItemToggleable;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import noppes.mpm.ModelData;

public class BackLayer implements LayerRenderer<EntityPlayer> {
    public ResourceLocation podswordRl = new ResourceLocation(GunCus.MOD_ID + ":" + "podsword");


    @Override
    public void doRenderLayer(EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
        ICAPCustomInventory cap = player.getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null);
        CustomInventory inv = cap.getInventory();
        ItemStack is = inv.getStackInSlot(0);
        //Рендер предмета(itemgun)

        if (is != ItemStack.EMPTY) {
            if (GunCus.GUN_NRPGUN.equals(is.getItem())) return;
            GlStateManager.pushMatrix();
            ModelData md = player.getCapability(ModelData.MODELDATA_CAPABILITY, null);
            if (md != null) {

/*
            float addYLegs = playerScales.getCompoundTag("LegsConfig").getFloat("TransY");
*/
                    float addYBody = md.body.transY;
                    /*            float addZBody = TESTING1.getCompoundTag("BodyConfig").getFloat("ScaleZ");*/
                    /*            float addZBody = TESTING1.getCompoundTag("BodyConfig").getFloat("ScaleZ");*/
                    GlStateManager.translate(0.00F, addYBody, 0.00F);

            }


            if (is.getItem() instanceof ItemGun || is.getItem() instanceof ItemGCI3D || is.getItem() instanceof ItemToggleable) {

                //Позиция предмета
                //Рукоятью вверх
/*        GlStateManager.translate(0.00F, 0.30F, 0.25F);
        //Вращение предмета
        GlStateManager.rotate(-15F, 0, 0, 20);
        //Размеры предмета
        GlStateManager.scale(1F, 1F, 1F);*/
                GlStateManager.translate(0.00F, 0.50F, 0.05F);

                if (GunCus.GUN_VYKHLOP.equals(is.getItem())) {
                    GlStateManager.translate(0F, 0.1F, 0F);
                }

                //Вращение предмета265
                GlStateManager.rotate(265F, 0, 0, 20);
                GlStateManager.rotate(180F, 1, 0, 0);
                //Размеры предмета
                GlStateManager.scale(1F, 1F, 1F);
/*            GlStateManager.translate(0, 0.35F, 0.25F);
            //Вращение предмета
            GlStateManager.rotate(360F, 0, 0, 20);
            //Размеры предмета
            GlStateManager.scale(1F, 1F, 1F);*/

                /*            GlStateManager.translate(-0.37F, 0.35F, 0.25F); //менять x чтобы двигать левее-правее*/
                //Вращение предмета
/*            GlStateManager.rotate(315F, 0, 0, 20);

            GlStateManager.rotate(180F, 1, 0, 0);
            GlStateManager.rotate(180F, 0, 1, 0);*/

/*            GlStateManager.rotate(90F, 0, 1, 0);
            GlStateManager.rotate(-45F, 0, 0, 20);
            GlStateManager.rotate(-180F, 0, 0, 20);
            GlStateManager.rotate(180F, 1, 0, 0);
            GlStateManager.rotate(90F, 0, 0, 1);*/

                //Размеры предмета
                //GlStateManager.scale(1F, 1F, 1F);

                //Условие: Если игрок присел, то мы меняем положение нашего колчана.0.155
                if (player.isSneaking()) {
                    GlStateManager.rotate(-30F, 0, 1, 0);
                    GlStateManager.translate(-0.180F, 0F, -0.13F);
                }
                Minecraft.getMinecraft().getRenderItem().renderItem(is, player, ItemCameraTransforms.TransformType.FIXED, false);
            } else if (is.getItem() instanceof ItemShield) {
                GlStateManager.rotate(180F, 0, 0, 20);
                GlStateManager.rotate(180F, 0, 10, 0);
                GlStateManager.translate(0.00F, -0.3F, -0.175F);

                if (player.isSneaking()) {
                    GlStateManager.rotate(30F, 1, 0, 0);
                    //GlStateManager.translate(-0.180F, 0F, -0.13F);
                    GlStateManager.translate(0, -0.180F, -0.03F);
                }
                Minecraft.getMinecraft().getRenderItem().renderItem(is, player, ItemCameraTransforms.TransformType.FIXED, false);
            } else {
                GlStateManager.translate(0, 0.35F, 0.15F);
                //Вращение предмета
                GlStateManager.rotate(180F, 0, 0, 20);
                //Размеры предмета
                GlStateManager.scale(0.75F, 0.75F, 0.75F);
                if (is.getItem().equals(ForgeRegistries.ITEMS.getValue(podswordRl))) {
                    GlStateManager.translate(0, 0F, -0.1F);
                    GlStateManager.rotate(180F, 1, 0, 0);
                    GlStateManager.rotate(180F, 0, 0, 1);
                }
                //Условие: Если игрок присел, то мы меняем положение нашего колчана.
                if (player.isSneaking()) {
                    GlStateManager.rotate(-30F, 1, 0, 0);
                    GlStateManager.translate(0, -0.155F, 0.1F);
                }
                Minecraft.getMinecraft().getRenderItem().renderItem(is, player, ItemCameraTransforms.TransformType.FIXED, false);
            }
            GlStateManager.popMatrix();
        }
    }

    @Override
    public boolean shouldCombineTextures() {
        return false;
    }
}