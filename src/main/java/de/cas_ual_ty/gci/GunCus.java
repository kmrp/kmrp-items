package de.cas_ual_ty.gci;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import de.cas_ual_ty.gci.block.BlockArmorTable;
import de.cas_ual_ty.gci.block.BlockGCI;
import de.cas_ual_ty.gci.block.BlockGunTable;
import de.cas_ual_ty.gci.block.BlockMovingLightSource;
import de.cas_ual_ty.gci.item.*;
import de.cas_ual_ty.gci.item.armor.ArmorPiece;
import de.cas_ual_ty.gci.item.attachment.Accessory;
import de.cas_ual_ty.gci.item.attachment.Accessory.Laser;
import de.cas_ual_ty.gci.item.attachment.Ammo;
import de.cas_ual_ty.gci.item.attachment.Attachment;
import de.cas_ual_ty.gci.item.attachment.Auxiliary;
import de.cas_ual_ty.gci.item.attachment.Barrel;
import de.cas_ual_ty.gci.item.attachment.Magazine;
import de.cas_ual_ty.gci.item.attachment.Optic;
import de.cas_ual_ty.gci.item.attachment.Paint;
import de.cas_ual_ty.gci.item.attachment.Underbarrel;
import de.cas_ual_ty.gci.item.dyeable.ItemDyeable;
import de.cas_ual_ty.gci.item.dyeable.ItemDyeableFood;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.registries.IForgeRegistry;

import static de.cas_ual_ty.gci.SoundEventGCI.soundEventList;


@Mod(modid = GunCus.MOD_ID, version = GunCus.MOD_VERSION, name = GunCus.MOD_NAME)
public class GunCus
{

	public static final String MOD_ID = "gci";
	public static final String MOD_NAME = "Gun Customization: Infinity";
	public static final String MOD_VERSION = "0.6-1.12.2";

	public static final Random RANDOM = new Random();

	public static final CreativeTabsGCI TAB_GUNCUS = new CreativeTabsGCI(GunCus.MOD_ID);
	public static final KMRPTab KMRP_TAB = new KMRPTab("KMRP");

	public static final SoundEventGCI SOUND_SHOOT 			= new SoundEventGCI(0, "shoot");
	public static final SoundEventGCI SOUND_SHOOT_SILENCED 	= new SoundEventGCI(1, "shoot_silenced");
	public static final SoundEventGCI SOUND_SHOOT_SNIPER 	= new SoundEventGCI(2, "shoot_sniper");
	public static final SoundEventGCI SOUND_RELOAD 			= new SoundEventGCI(3, "reload");
	public static final SoundEventGCI SOUND_SHOOT_SILENCED_KMRP = new SoundEventGCI(4, "suppressorshot");
	public static final SoundEventGCI SOUND_SHOOT_HEAVY = new SoundEventGCI(5, "highcalshot");
	public static final SoundEventGCI SOUND_SHOOT_MEDIUM = new SoundEventGCI(6, "medcalshot");
	public static final SoundEventGCI SOUND_SHOOT_LIGHT = new SoundEventGCI(7, "smallcalshot");
	public static final SoundEventGCI SOUND_BULLETCRACK = new SoundEventGCI(8, "bulletcrack");
	public static final SoundEventGCI SOUND_SWING = new SoundEventGCI(9, "swing");
	public static final SoundEventGCI SOUND_MACEHIT = new SoundEventGCI(10, "macehit");
	public static final SoundEventGCI SOUND_BLADEHIT = new SoundEventGCI(11, "bladehit");
	public static final SoundEventGCI SOUND_KLIN = new SoundEventGCI(12, "klin");
	public static final SoundEventGCI SOUND_PUNCH = new SoundEventGCI(13, "punch");
	public static final SoundEventGCI SOUND_WHIP = new SoundEventGCI(14, "whip");
	public static final SoundEventGCI SOUND_PARIROVANIE = new SoundEventGCI(15, "parirovanie");
	public static final SoundEventGCI SOUND_BULLETSTICKBODY = new SoundEventGCI(16, "bulletstickbody");
	public static final SoundEventGCI SOUND_SHIELDBEATSOUND = new SoundEventGCI(17, "parirovanie");
	public static final SoundEventGCI SOUND_PUNCHMISS = new SoundEventGCI(18, "punchmiss");
	public static final SoundEventGCI SOUND_RING = new SoundEventGCI(19, "ring");
	public static final SoundEventGCI SOUND_RING_LOW = new SoundEventGCI(20, "ring_low");
	public static final SoundEventGCI SOUND_FLY = new SoundEventGCI(21, "flysound");
	public static final SoundEventGCI SOUND_HIT = new SoundEventGCI(22, "hitsound");
	public static final SoundEventGCI SOUND_GUITAR_HIT = new SoundEventGCI(23, "guitar_hit");
	public static final SoundEventGCI SOUND_GUITAR_MISS = new SoundEventGCI(24, "guitar_miss");

	public static final BlockGCI BLOCK_GUN_TABLE = new BlockGunTable("gun_table");
	public static final BlockGCI BLOCK_ARMOR_TABLE = new BlockArmorTable("armor_table");
	public static final BlockGCI BLOCK_BOCHA = new BlockGCI("bocha", KMRP_TAB, Material.IRON);
	public static final BlockGCI BLOCK_KUSTVERSTACK = new BlockGCI("kustverstack", KMRP_TAB, Material.IRON);
	public final static BlockMovingLightSource MOVING_LIGHT_SOURCE = new BlockMovingLightSource("movinglightsource");



	public static final ItemCartridge CARTRIDGE_5_56x45mm 		= new ItemCartridge("cartridge_5_56x45mm").setDamage(5F);
	public static final ItemCartridge CARTRIDGE_5_45x39mm 		= new ItemCartridge("cartridge_5_45x39mm").setDamage(5F);
	public static final ItemCartridge CARTRIDGE_12G_BUCKSHOT 	= new ItemCartridge("cartridge_12g_buckshot").setDamage(1.5F).setProjectileAmmount(6);
	public static final ItemCartridge CARTRIDGE_12G_DART 		= new ItemCartridge("cartridge_12g_dart").setDamage(1.5F).setProjectileAmmount(6);
	public static final ItemCartridge CARTRIDGE_12G_FRAG 		= new ItemCartridge("cartridge_12g_frag").setDamage(5F);
	public static final ItemCartridge CARTRIDGE_12G_SLUG 		= new ItemCartridge("cartridge_12g_slug").setDamage(5F);
	public static final ItemCartridge CARTRIDGE__44_magnum 		= new ItemCartridge("cartridge__44_magnum").setDamage(8F);
	public static final ItemCartridge CARTRIDGE__338_magnum 	= new ItemCartridge("cartridge__338_magnum").setDamage(8F);

	public static final Optic 		OPTIC_DEFAULT 				= new Optic(0, "optic_default");
	public static final Accessory 	ACCESSORY_DEFAULT 			= new Accessory(0, "accessory_default");
	public static final Barrel 		BARREL_DEFAULT 				= new Barrel(0, "barrel_default");
	public static final Underbarrel UNDERBARREL_DEFAULT 		= new Underbarrel(0, "underbarrel_default");
	public static final Auxiliary 	AUXILIARY_DEFAULT 			= new Auxiliary(0, "auxiliary_default");
	public static final Ammo 		AMMO_DEFAULT 				= new Ammo(0, "ammo_default");
	public static final Magazine 	MAGAZINE_DEFAULT 			= new Magazine(0, "magazine_default");
	public static final Paint 		PAINT_DEFAULT 				= new Paint(0, "paint_default");

	public static final Optic 		OPTIC_REFLEX 				= new Optic(1, "optic_reflex");
	public static final Optic 		OPTIC_COYOTE 				= new Optic(2, "optic_coyote");
	public static final Optic 		OPTIC_KOBRA 				= new Optic(3, "optic_kobra");
	public static final Optic 		OPTIC_HOLO 					= new Optic(4, "optic_holo");
	public static final Optic 		OPTIC_HD33 					= new Optic(5, "optic_hd33");
	public static final Optic 		OPTIC_PKAS 					= new Optic(6, "optic_pkas");
	//	public static final Optic 		OPTIC_IRNV 					= new Optic(7, "optic_irnv").setOpticType(EnumOpticType.NIGHT_VISION);
	//	public static final Optic 		OPTIC_FLIR 					= new Optic(8, "optic_flir").setZoom(2F).setOpticType(EnumOpticType.THERMAL);
	public static final Optic 		OPTIC_M145 					= new Optic(9, "optic_m145").setZoom(3.4F);
	public static final Optic 		OPTIC_PRISMA 				= new Optic(10, "optic_prisma").setZoom(3.4F);
	public static final Optic 		OPTIC_PKA 					= new Optic(11, "optic_pka").setZoom(3.4F);
	public static final Optic 		OPTIC_ACOG 					= new Optic(12, "optic_acog").setZoom(4F);
	public static final Optic 		OPTIC_JGM4 					= new Optic(13, "optic_jgm4").setZoom(4F);
	public static final Optic 		OPTIC_PSO1 					= new Optic(14, "optic_pso1").setZoom(4F);
	public static final Optic 		OPTIC_CL6X 					= new Optic(15, "optic_cl6x").setZoom(6F);
	public static final Optic 		OPTIC_PKS07 				= new Optic(16, "optic_pks07").setZoom(7F);
	public static final Optic 		OPTIC_RIFLE 				= new Optic(17, "optic_rifle").setZoom(8F);
	public static final Optic 		OPTIC_HUNTER 				= new Optic(18, "optic_hunter").setZoom(20F);
	public static final Optic 		OPTIC_BALLISTIC 			= new Optic(19, "optic_ballistic").setZoom(40F);

	// Carbine scopes
	public static final Optic 		OPTIC_CARBINE_COLIMATOR_SCOPE		= new Optic(20, "optic_carbine_colimator_scope");
	public static final Optic 		OPTIC_CARBINE_SCOPE 				= new Optic(21, "optic_carbine_scope").setZoom(20F);

	// Universal scopes
	public static final Optic 		OPTIC_UNI_SCOPE_HOLO 				= new Optic(22, "optic_uni_scope_holo");
	public static final Optic 		OPTIC_UNI_SCOPE_HOLO_MAGNIF			= new Optic(23, "optic_uni_scope_holo_magnif").setZoom(3.4F);
	public static final Optic 		OPTIC_UNI_SCOPE_REFLEX 				= new Optic(24, "optic_uni_scope_reflex");
	public static final Optic 		OPTIC_UNI_SCOPE_SNIP				= new Optic(25, "optic_uni_scope_snip").setZoom(8F);
	public static final Optic 		OPTIC_UNI_SCOPE_SNIP_RNG			= new Optic(26, "optic_uni_scope_snip_rng").setZoom(8F).setLaser(new Optic.Laser(0F, 0F, 0F, 150D, false, false, true));;

	// Doublebarrel planka
	public static final Optic 		OPTIC_DOUBLEBARREL_PLANKA			= new Optic(27, "optic_doublebarrel_planka");

	public static final Optic 		OPTIC_GL_SIGHT_DOWN					= new Optic(28, "optic_gl_sight_down").setToggleableTo(29);
	public static final Optic 		OPTIC_GL_SIGHT_UP					= new Optic(29, "optic_gl_sight_up").setToggleableTo(28);

	public static final Optic 		OPTIC_UNI_SCOPE_ASSAULT				= new Optic(30, "optic_uni_scope_assault");
	public static final Optic 		OPTIC_UNI_SCOPE_HOLO_SMALL			= new Optic(31, "optic_uni_scope_holo_small");
	public static final Optic 		OPTIC_UNI_SCOPE_REFLEX_SMALL		= new Optic(32, "optic_uni_scope_reflex_small");

	public static final Optic 		MONOCULAR_HUNT				= new Optic(33, "monocular_hunt").setZoom(8F);
	public static final Optic 		MONOCULAR_NV				= new Optic(34, "monocular_nv").setZoom(8F).setOpticType(Optic.EnumOpticType.NIGHT_VISION);
	public static final Optic 		MONOCULAR_WAR				= new Optic(35, "monocular_war").setZoom(8F);
	public static final Optic 		MONOCULAR				= new Optic(36, "monocular").setZoom(8F);
	public static final Optic 		MONOCULAR_RANGE				= new Optic(37, "monocular_range").setZoom(8F).setLaser(new Optic.Laser(0F, 0F, 0F, 150D, false, false, true));

	public static final Optic 		OPTIC_UNI_BACK_SCOPE		= new Optic(38, "optic_uni_back_scope");

	public static final Optic 		BINOCULAR		= new Optic(39, "binocular").setZoom(8F);

	public static final Optic 		SPYGLASS_OFF		= new Optic(40, "spyglass_off").setToggleableTo(41);
	public static final Optic 		SPYGLASS_ON		= new Optic(41, "spyglass_on").setZoom(8F).setToggleableTo(40);
	public static final Optic 		SPRING_SIGHT		= new Optic(42, "spring_sight");
	public static final Optic 		SPRING_GRIP_SIGHT		= new Optic(43, "spring_grip_sight");


	public static final Accessory 	ACCESSORY_VARIABLE_ZOOM 	= new Accessory(1, "accessory_variable_zoom").setExtraZoom(14F);
	public static final Accessory 	ACCESSORY_MAGNIFIER 		= new Accessory(2, "accessory_magnifier").setZoomModifier(1.5F);
	// ACCESSORY_FLASH_LIGHT
	// ACCESSORY_TACTICAL LIGHT
	public static final Accessory 	ACCESSORY_LASER_SIGHT 		= new Accessory(5, "accessory_laser_sight").setLaser(new Laser(1F, 0F, 0F, 50D, true, true, false));
	public static final Accessory 	ACCESSORY_TRI_BEAM_LASER 	= new Accessory(6, "accessory_tri_beam_laser").setLaser(new Laser(1F, 0F, 0F, 80D, true, true, false));
	public static final Accessory 	ACCESSORY_GREEN_LASER_SIGHT = new Accessory(7, "accessory_green_laser_sight").setLaser(new Laser(0F, 1F, 0F, 50D, true, true, false));
	// ACCESSORY_LASER_LIGHT_COMBO
	public static final Accessory 	ACCESSORY_RANGE_FINDER 		= new Accessory(9, "accessory_range_finder").setLaser(new Laser(0F, 0.5F, 1F, 125D, false, true, true));
	public static final Accessory 	STOCK_UNI_FOLDING_OFF 		= new Accessory(10, "stock_uni_folding_off").setToggleableTo(11).setStockFolded(true);;
	public static final Accessory 	STOCK_UNI_FOLDING_ON		= new Accessory(11, "stock_uni_folding_on").setToggleableTo(10);
	public static final Accessory 	STOCK_UNI_TELESCOPIC		= new Accessory(12, "stock_uni_telescopic");
	public static final Accessory 	STOCK_RPK_BASE 				= new Accessory(13, "stock_rpk_base");
	public static final Accessory 	STOCK_REM_BASE 				= new Accessory(14, "stock_rem_base");
	public static final Accessory 	STOCK_REM_SHORT				= new Accessory(15, "stock_rem_short");
	public static final Accessory 	STOCK_SMG_BASE 				= new Accessory(16, "stock_smg_base");
	public static final Accessory 	STOCK_HSMG_BASE 			= new Accessory(17, "stock_hsmg_base");

	public static final Accessory 	STOCK_DOUBLEBARREL_SHORT	= new Accessory(18, "stock_doublebarrel_short");
	public static final Accessory 	STOCK_DOUBLEBARREL_FULL							= new Accessory(19, "stock_doublebarrel_full");
	public static final Accessory 	STOCK_DOUBLEBARREL_PISTOL 						= new Accessory(20, "stock_doublebarrel_pistol");
	public static final Accessory 	STOCK_DOUBLEBARREL_PISTOL_TELESCOPIC			= new Accessory(21, "stock_doublebarrel_pistol_telescopic");
	public static final Accessory 	STOCK_DOUBLEBARREL_PISTOL_FOLDING_ON 			= new Accessory(22, "stock_doublebarrel_pistol_folding_on");
	public static final Accessory 	STOCK_DOUBLEBARREL_PISTOL_FOLDING_OFF 			= new Accessory(23, "stock_doublebarrel_pistol_folding_off").setStockFolded(true);;

	public static final Accessory 	STOCK_FAL_BASE 									= new Accessory(24, "stock_fal_base");

	public static final Accessory 	STOCK_UNI_PISTOL 								= new Accessory(25, "stock_uni_pistol");

	public static final Accessory 	STOCK_TMP_BASE 									= new Accessory(26, "stock_tmp_base");

	public static final Accessory 	STOCK_UMP_BASE 									= new Accessory(27, "stock_ump_base");

	public static final Accessory 	STOCK_MOSSBERG_BASE 							= new Accessory(28, "stock_mossberg_base");
	public static final Accessory 	STOCK_MOSSBERG_OBREZ 							= new Accessory(29, "stock_mossberg_obrez").setStockFolded(true);;

	public static final Accessory 	STOCK_PISTOL_UNI              			= new Accessory(30, "stock_pistol_uni");
	public static final Accessory 	STOCK_PISTOL_UNI_TELESCOPIC				= new Accessory(31, "stock_pistol_uni_telescopic");
	public static final Accessory 	STOCK_PISTOL_UNI_FOLDING_ON				= new Accessory(32, "stock_pistol_uni_folding_on").setToggleableTo(33);
	public static final Accessory 	STOCK_PISTOL_UNI_FOLDING_OFF				= new Accessory(33, "stock_pistol_uni_folding_off").setToggleableTo(32).setStockFolded(true);;

	public static final Accessory 	STOCK_RAILGUN_BASE				= new Accessory(34, "stock_railgun_base");

	public static final Accessory 	SWORD_FIREMODULE				= new Accessory(35, "sword_firemodule");

	public static final Accessory 	STOCK_SCORPIO_UNDEPLETED				= new Accessory(36, "stock_scorpio_undepleted").setToggleableTo(37).setStockFolded(true);;
	public static final Accessory 	STOCK_SCORPIO_DEPLETED				= new Accessory(37, "stock_scorpio_depleted").setToggleableTo(36);

	public static final Accessory 	STOCK_MC255_PISTOL_FOLDING_OFF				= new Accessory(38, "stock_mc255_pistol_folding_off").setToggleableTo(39).setStockFolded(true);;
	public static final Accessory 	STOCK_MC255_PISTOL_FOLDING_ON				= new Accessory(39, "stock_mc255_pistol_folding_on").setToggleableTo(38);
	public static final Accessory 	STOCK_MC255_PISTOL_TELESCOPIC				= new Accessory(40, "stock_mc255_pistol_telescopic");
	public static final Accessory 	STOCK_MC255_SHORT				= new Accessory(41, "stock_mc255_short");
	public static final Accessory 	STOCK_MC255_STOCK				= new Accessory(42, "stock_mc255_stock");
	public static final Accessory 	STOCK_MC255_PISTOL				= new Accessory(43, "stock_mc255_pistol");

	public static final Accessory 	STOCK_SPAS_UNDEPLETED				= new Accessory(44, "stock_spas_undepleted").setToggleableTo(45);
	public static final Accessory 	STOCK_SPAS_DEPLETED				= new Accessory(45, "stock_spas_depleted").setToggleableTo(44);

	public static final Accessory 	STOCK_PPAUTO_DEFAULT_METAL				= new Accessory(46, "stock_ppauto_default_metal");
	public static final Accessory 	STOCK_PPAUTO_DEFAULT_WOOD				= new Accessory(47, "stock_ppauto_default_wood");

	public static final Accessory 	STOCK_CRAB_METAL				= new Accessory(48, "stock_crab_metal");
	public static final Accessory 	STOCK_CRAB_WOOD				= new Accessory(49, "stock_crab_wood");

	public static final Accessory 	STOCK_CRAB_BASE				= new Accessory(50, "stock_crab_base");

	public static final Accessory 	STOCK_WINCHESTER_BASE				= new Accessory(51, "stock_winchester_base");
	public static final Accessory 	STOCK_WINCHESTER_SAWOFF				= new Accessory(52, "stock_winchester_sawoff").setStockFolded(true);

	public static final Accessory 	STOCK_MOSSBERG2_BASE				= new Accessory(53, "stock_mossberg2_base");

	public static final Accessory 	STOCK_PPS_UNFOLDED				= new Accessory(54, "stock_pps_unfolded").setToggleableTo(55);;
	public static final Accessory 	STOCK_PPS_FOLDED				= new Accessory(55, "stock_pps_folded").setToggleableTo(54).setStockFolded(true);

	public static final Accessory 	STOCK_APS_BASE				= new Accessory(56, "stock_aps_base").setToggleableTo(54);

	public static final Accessory 	STOCK_SCORPIONEW_UNDEPLETED				= new Accessory(57, "stock_scorpionew_undepleted").setToggleableTo(58).setStockFolded(true);;
	public static final Accessory 	STOCK_SCORPIONEW_DEPLETED				= new Accessory(58, "stock_scorpionew_depleted").setToggleableTo(57);

	public static final Accessory 	STOCK_OWEN_BASE				= new Accessory(59, "stock_owen_base");

	public static final Accessory 	STOCK_TOMMY_BASE				= new Accessory(60, "stock_tommy_base");

	public static final Accessory 	STOCK_MOSIN_BASE				= new Accessory(61, "stock_mosin_base");

	public static final Accessory 	STOCK_M1A1_BASE				= new Accessory(62, "stock_m1a1_base");

	public static final Accessory 	STOCK_AMERICAN_BASE				= new Accessory(63, "stock_american_base");

	public static final Accessory 	STOCK_SJORGEN_BASE				= new Accessory(64, "stock_sjorgen_base");

	public static final Accessory 	STOCK_BORCHADT_BASE				= new Accessory(65, "stock_borchadt_base");

	public static final Accessory 	STOCK_PITCH_BASE				= new Accessory(66, "stock_pitch_base");

	public static final Accessory 	STOCK_BIGGUN_BASE				= new Accessory(67, "stock_biggun_base");

	public static final Accessory 	STOCK_FG42_BASE				= new Accessory(68, "stock_fg42_base");
	public static final Accessory 	STOCK_GIGASHOT_BASE				= new Accessory(69, "stock_gigashot_base");
	public static final Accessory 	STOCK_GIGASHOT_CURSED				= new Accessory(70, "stock_gigashot_cursed");
	public static final Accessory 	STOCK_GIGASHOT_EXTRACURSED				= new Accessory(71, "stock_gigashot_extra-cursed");
	public static final Accessory 	STOCK_GIGASHOT_HANDLECURSED				= new Accessory(72, "stock_gigashot_handle-cursed");
	public static final Accessory 	STOCK_GIGASHOT_HANDLENORMAL				= new Accessory(73, "stock_gigashot_handle-normal");

	public static final Accessory 	NADE_SHOMPOL_BASE				= new Accessory(74, "nade_shompol_base");

	public static final Accessory 	BITA_HANDLE_CLOTH				= new Accessory(75, "bita_handle_cloth");
	public static final Accessory 	BITA_HANDLE_LEATHER				= new Accessory(76, "bita_handle_leather");
	public static final Accessory 	BITA_HANDLE_RUBBER				= new Accessory(77, "bita_handle_rubber");
	public static final Accessory 	BITA_HANDLE_RUBBERALT			= new Accessory(78, "bita_handle_rubberalt");

	public static final Accessory 	STOCK_LONESTAR_BASE			= new Accessory(79, "stock_lonestar_base");

	public static final Accessory 	STOCK_HELLCAT_WOOD			= new Accessory(80, "stock_hellcat_wood");

	public static final Accessory 	STOCK_G3R1_WOOD			= new Accessory(81, "stock_g3r1_wood");
	public static final Accessory 	STOCK_AKSU_WOOD			= new Accessory(82, "stock_aksu_wood");

	public static final Accessory 	STOCK_VAG72_BASE			= new Accessory(83, "stock_vag72_base");
	public static final Accessory 	STOCK_VAG72_AXE			= new Accessory(84, "stock_vag72_axe");

	public static final Accessory 	STOCK_AKS_BASE			= new Accessory(85, "stock_aks_base");

	public static final Accessory 	WASP_STOCK			= new Accessory(86, "wasp_stock");
	public static final Accessory 	STOCK_S1			= new Accessory(87, "stock_s1");
	public static final Accessory 	STAPLER_STOCK			= new Accessory(88, "stapler_stock");
	public static final Accessory 	BULL_STOCK			= new Accessory(89, "bull_stock");
	public static final Accessory 	SPRING_STOCK			= new Accessory(90, "spring_stock");
	public static final Accessory 	NETTLE_STOCK			= new Accessory(91, "nettle_stock");
	public static final Accessory 	PERFORATOR_STOCK			= new Accessory(92, "perforator_stock");
	public static final Accessory 	SHEEV_STOCK			= new Accessory(93, "sheev_stock");

	public static final Barrel 		BARREL_HEAVY_BARREL 		= new Barrel(1, "barrel_heavy_barrel").setExtraDamage(1.0F);
	public static final Barrel 		BARREL_SUPPRESSOR 			= new Barrel(2, "barrel_suppressor").setIsSilenced(true).setIsFlashHider(true).setBulletSpeedModifier(0.7F);
	public static final Barrel 		BARREL_LS06_SUPPRESSOR 		= new Barrel(3, "barrel_ls06_suppressor").setIsSilenced(true).setIsFlashHider(true).setBulletSpeedModifier(0.7F);
	public static final Barrel 		BARREL_PBS4_SUPPRESSOR 		= new Barrel(4, "barrel_pbs4_suppressor").setIsSilenced(true).setIsFlashHider(true).setBulletSpeedModifier(0.7F);
	public static final Barrel 		BARREL_R2_SUPPRESSOR 		= new Barrel(5, "barrel_r2_suppressor").setIsSilenced(true).setIsFlashHider(true).setBulletSpeedModifier(0.7F);
	public static final Barrel 		BARREL_FLASH_HIDER 			= new Barrel(6, "barrel_flash_hider").setIsFlashHider(true);
	public static final Barrel 		BARREL_COMPENSATOR 			= new Barrel(7, "barrel_compensator").setDriftModifier(1.25F).setInaccuracyModifier(1.2F);
	public static final Barrel 		BARREL_MUZZLE_BRAKE 		= new Barrel(8, "barrel_muzzle_brake").setDriftModifier(1.25F).setInaccuracyModifier(1.2F);
	public static final Barrel 		BARREL_DUCKBILL 			= new Barrel(9, "barrel_duckbill").setVerticalSpreadModifier(0.4F);
	public static final Barrel 		BARREL_MODIFIED_CHOKE 		= new Barrel(10, "barrel_modified_choke").setVerticalSpreadModifier(0.8F).setHorizontalSpreadModifier(0.8F);
	public static final Barrel 		BARREL_FULL_CHOKE 			= new Barrel(11, "barrel_full_choke").setVerticalSpreadModifier(0.6F).setHorizontalSpreadModifier(0.6F);
	public static final Barrel 		BARREL_MEWT_SUPPRESSOR 		= new Barrel(12, "barrel_mewt_suppressor").setIsSilenced(true).setIsFlashHider(true).setBulletSpeedModifier(0.7F);
    //universal kmrp barrel modules
	public static final Barrel 		BARREL_UNI_COMPENSATOR 	    = new Barrel(13, "barrel_uni_compensator").setIsSilenced(false).setIsFlashHider(true).setBulletSpeedModifier(0.7F);
	public static final Barrel 		BARREL_UNI_SILENCER 		= new Barrel(14, "barrel_uni_silencer").setIsSilenced(true).setIsFlashHider(true).setBulletSpeedModifier(0.7F);
	public static final Barrel 		BARREL_UNI_SUPRESSOR 		= new Barrel(15, "barrel_uni_supressor").setIsSilenced(true).setIsFlashHider(true).setBulletSpeedModifier(0.7F);

	public static final Barrel 		STVOL_DOUBLEBARREL_VERTIC_LONG		= new Barrel(16, "stvol_doublebarrel_vertic_long");
	public static final Barrel 		STVOL_DOUBLEBARREL_VERTIC_SHORT 	= new Barrel(17, "stvol_doublebarrel_vertic_short");
	public static final Barrel 		STVOL_DOUBLEBARREL_HORIZ_LONG		= new Barrel(18, "stvol_doublebarrel_horiz_long");
	public static final Barrel 		STVOL_DOUBLEBARREL_HORIZ_SHORT		= new Barrel(19, "stvol_doublebarrel_horiz_short");

	public static final Barrel 		BARREL_VYKHLOP_SILENCER 		= new Barrel(20, "barrel_vykhlop_silencer").setIsSilenced(true).setIsFlashHider(true).setBulletSpeedModifier(0.7F);
	public static final Barrel 		BARREL_VYKHLOP_COMPENSATOR 	    = new Barrel(21, "barrel_vykhlop_compensator").setIsSilenced(false).setIsFlashHider(true).setBulletSpeedModifier(0.7F);

	public static final Barrel 		BARREL_RIG_TBG		= new Barrel(22, "barrel_rig_tbg");
	public static final Barrel 		BARREL_RIG_FRAG		= new Barrel(23, "barrel_rig_frag");

	public static final Barrel 		BARREL_MC255_SILENCERLONGBARREL		= new Barrel(24, "barrel_mc255_silencerlongbarrel").setIsSilenced(true);
	public static final Barrel 		BARREL_MC255_SILENCERSHORTBARREL		= new Barrel(25, "barrel_mc255_silencershortbarrel").setIsSilenced(true);
	public static final Barrel 		BARREL_MC255_LONG		= new Barrel(26, "barrel_mc255_long");
	public static final Barrel 		BARREL_MC255_SHORT		= new Barrel(27, "barrel_mc255_short");

	public static final Barrel 		KATANA_SHEATH		= new Barrel(28, "katana_sheath");

	public static final Barrel 		BARREL_CRAB_GARBAGE_SILENCER 		= new Barrel(29, "barrel_crab_garbage_silencer").setIsSilenced(true).setIsFlashHider(true).setBulletSpeedModifier(0.7F);

	public static final Barrel 		BARREL_NOTLUGER_LONG		= new Barrel(30, "barrel_notluger_long");
	public static final Barrel 		BARREL_AUTOMAG_SHORT		= new Barrel(31, "barrel_automag_short");

	public static final Barrel 		BARREL_TOMMY_SHORT		= new Barrel(32, "barrel_tommy_short");
	public static final Barrel 		BARREL_TOMMY_LONG		= new Barrel(33, "barrel_tommy_long");

	public static final Barrel 		BARREL_UNI_GRENADELAUNCHER		= new Barrel(34, "barrel_uni_grenadelauncher");

	public static final Barrel 		HEAD_MACE_DISKS		= new Barrel(35, "head_mace_disks");
	public static final Barrel 		HEAD_MACE_ROUND		= new Barrel(36, "head_mace_round");
	public static final Barrel 		HEAD_MACE_SQUAER		= new Barrel(37, "head_mace_squaer");

	public static final Barrel 		RAG_BOTTLE_BLACK		= new Barrel(38, "rag_bottle_black");
	public static final Barrel 		RAG_BOTTLE_WHITE		= new Barrel(39, "rag_bottle_white");
	public static final Barrel 		RAG_BOTTLE_RED		= new Barrel(40, "rag_bottle_red");
	public static final Barrel 		CAP_BOTTLE_BASE		= new Barrel(41, "cap_bottle_base");

	public static final Barrel 		BARREL_BIGGUN_BASE  	= new Barrel(42, "barrel_biggun_base");

	public static final Barrel 		BARREL_GIGASHOT_HORISONTALFULLSCREENED  	= new Barrel(43, "barrel_gigashot_horisontal-fullscreened");
	public static final Barrel 		BARREL_GIGASHOT_HORISONTALSAWEDOFF  	= new Barrel(44, "barrel_gigashot_horisontal-sawedoff");
	public static final Barrel 		BARREL_GIGASHOT_HORISONTALSAWEDOFFEVENMORE  	= new Barrel(44, "barrel_gigashot_horisontal-sawedoffevenmore");
	public static final Barrel 		BARREL_GIGASHOT_HORISONTALSEMISCREENED  	= new Barrel(45, "barrel_gigashot_horisontal-semiscreened");
	public static final Barrel 		BARREL_GIGASHOT_HORISONTALUNSCREENED  	= new Barrel(46, "barrel_gigashot_horisontal-unscreened");
	public static final Barrel 		BARREL_GIGASHOT_MONO  	= new Barrel(47, "barrel_gigashot_mono");
	public static final Barrel 		BARREL_GIGASHOT_MULTI  	= new Barrel(48, "barrel_gigashot_multi");
	public static final Barrel 		BARREL_GIGASHOT_VERTICAL 	= new Barrel(49, "barrel_gigashot_vertical");
	public static final Barrel 		BARREL_GIGASHOT_VERTICALSCREENED  	= new Barrel(50, "barrel_gigashot_vertical-screened");

	public static final Barrel 		NADE_BLACK  	= new Barrel(51, "nade_black");
	public static final Barrel 		NADE_BLU  	= new Barrel(52, "nade_blu");
	public static final Barrel 		NADE_RED  	= new Barrel(53, "nade_red");
	public static final Barrel 		NADE_GBLACK  	= new Barrel(54, "nade_gblack");
	public static final Barrel 		NADE_GBLU  	= new Barrel(55, "nade_gblu");
	public static final Barrel 		NADE_GRED  	= new Barrel(56, "nade_gred");

	public static final Barrel 		METALRINGS  	= new Barrel(57, "metalrings");
	public static final Barrel 		STUNNER  	= new Barrel(58, "stunner");

	public static final Barrel 		BARREL_HOOK  	= new Barrel(59, "barrel_hook");

	public static final Barrel 		BITA_NAILS  	= new Barrel(60, "bita_nails");
	public static final Barrel 		BITA_NONLETHALNAILS  	= new Barrel(61, "bita_nonlethalnails");
	public static final Barrel 		BITA_WIRE  	= new Barrel(62, "bita_wire");

	public static final Barrel 		SHOVEL_HEAD  	= new Barrel(63, "shovel_head");
	public static final Barrel 		SHOVEL_HEAD_SERRATED  	= new Barrel(64, "shovel_head_serrated");

	public static final Barrel 		CANE_HADNLE_ROUND  	= new Barrel(65, "cane_handle_round");
	public static final Barrel 		CANE_HADNLE_BALL  	= new Barrel(66, "cane_handle_ball");
	public static final Barrel 		CANE_HADNLE_CUBE  	= new Barrel(67, "cane_handle_cube");
	public static final Barrel 		CANE_HADNLE_EAGLE  	= new Barrel(68, "cane_handle_eagle");
	public static final Barrel 		CANE_HADNLE_PROPER  	= new Barrel(69, "cane_handle_proper");

	public static final Barrel 		MGBAR_VAG72_BIG  	= new Barrel(70, "mgbar_vag72_big");
	public static final Barrel 		MGBAR_VAG72_MED  	= new Barrel(71, "mgbar_vag72_med");
	public static final Barrel 		MGBAR_VAG72_SMALL  	= new Barrel(72, "mgbar_vag72_small");

	public static final Barrel 		BRUSH_MOP  	= new Barrel(73, "brush_mop");
	public static final Barrel 		MECHA_MOP  	= new Barrel(74, "mecha_mop");
	public static final Barrel 		MOP_MOP  	= new Barrel(75, "mop_mop");

	public static final Barrel 		WASP_BARREL_LONG  	= new Barrel(76, "wasp_barrel_long");
	public static final Barrel 		WASP_BARREL_SHORT  	= new Barrel(77, "wasp_barrel_short");
	public static final Barrel 		PISTOL_BARREL_S1  	= new Barrel(78, "pistol_barrel_s1");
	public static final Barrel 		RIFLE_BARREL_S1  	= new Barrel(79, "rifle_barrel_s1");
	public static final Barrel 		STAPLER_BARREL_LONG  	= new Barrel(80, "stapler_barrel_long");
	public static final Barrel 		STAPLER_BARREL_SHORT 	= new Barrel(81, "stapler_barrel_short");
	public static final Barrel 		SHEEV_BARREL_LONG  	= new Barrel(82, "sheev_barrel_long");
	public static final Barrel 		SHEEV_BARREL_SHORT 	= new Barrel(83, "sheev_barrel_short");

	public static final Underbarrel UNDERBARREL_ERGO_GRIP 		= new Underbarrel(1, "underbarrel_ergo_grip").setInaccuracyModifierMoving(0.75F);
	public static final Underbarrel UNDERBARREL_ANGLED_GRIP 	= new Underbarrel(2, "underbarrel_angled_grip").setInaccuracyModifierStill(0.75F);
	public static final Underbarrel UNDERBARREL_STUBBY_GRIP 	= new Underbarrel(3, "underbarrel_stubby_grip").setDriftModifier(0.75F);
	public static final Underbarrel UNDERBARREL_VERTICAL_GRIP 	= new Underbarrel(4, "underbarrel_vertical_grip").setInaccuracyModifierMoving(0.75F);
	public static final Underbarrel UNDERBARREL_FOLDING_GRIP 	= new Underbarrel(5, "underbarrel_folding_grip").setInaccuracyModifierStill(0.75F);
	public static final Underbarrel UNDERBARREL_POTATO_GRIP 	= new Underbarrel(6, "underbarrel_potato_grip").setDriftModifier(0.75F);

	//universal kmrp underbarrel modules
	public static final Underbarrel UNDERBARREL_UNI_BAYONET     = new Underbarrel(7, "underbarrel_uni_bayonet").setTransferableToRl("left_uni_bayonet");
	public static final Underbarrel UNDERBARREL_UNI_FLAMETHROWER = new Underbarrel(8, "underbarrel_uni_flamethrower");
	public static final Underbarrel UNDERBARREL_UNI_FLASH_OFF   = new Underbarrel(9, "underbarrel_uni_flash_off").setToggleableTo(10).setTransferableToRl("left_uni_flash_off");
	public static final Underbarrel UNDERBARREL_UNI_FLASH_ON    = new Underbarrel(10, "underbarrel_uni_flash_on").setToggleableTo(9).setTransferableToRl("left_uni_flash_on").setFlashActive();
	public static final Underbarrel UNDERBARREL_UNI_GRENADELAUNCHER = new Underbarrel(11, "underbarrel_uni_grenadelauncher");
	public static final Underbarrel UNDERBARREL_UNI_GRIP        = new Underbarrel(12, "underbarrel_uni_grip");
	public static final Underbarrel UNDERBARREL_UNI_GRIP_FLASH_OFF = new Underbarrel(13, "underbarrel_uni_grip_flash_off").setToggleableTo(14);
	public static final Underbarrel UNDERBARREL_UNI_GRIP_FLASH_ON  = new Underbarrel(14, "underbarrel_uni_grip_flash_on").setToggleableTo(13).setFlashActive();
	public static final Underbarrel UNDERBARREL_UNI_GRIP_LCU_OFF   = new Underbarrel(15, "underbarrel_uni_grip_lcu_off").setToggleableTo(16);
	public static final Underbarrel UNDERBARREL_UNI_GRIP_LCU_ON    = new Underbarrel(16, "underbarrel_uni_grip_lcu_on").setToggleableTo(15).setLaser(new Underbarrel.Laser(1F, 0F, 0F, 150D, false, true, false));
	public static final Underbarrel UNDERBARREL_UNI_LCU_OFF        = new Underbarrel(17, "underbarrel_uni_lcu_off").setToggleableTo(18).setTransferableToRl("left_uni_lcu_off");
	public static final Underbarrel UNDERBARREL_UNI_LCU_ON         = new Underbarrel(18, "underbarrel_uni_lcu_on").setToggleableTo(17).setTransferableToRl("left_uni_lcu_on").setLaser(new Underbarrel.Laser(1F, 0F, 0F, 150D, false, true, false));
	public static final Underbarrel UNDERBARREL_UNI_SHOTGUN        = new Underbarrel(19, "underbarrel_uni_shotgun");
	public static final Underbarrel UNDERBARREL_UNI_BIPOD_OFF        = new Underbarrel(20, "underbarrel_uni_bipod_off").setToggleableTo(21);
	public static final Underbarrel UNDERBARREL_UNI_BIPOD_ON        = new Underbarrel(21, "underbarrel_uni_bipod_on").setToggleableTo(20);
	public static final Underbarrel UNDERBARREL_MAMMOTH_LCU_OFF        = new Underbarrel(22, "underbarrel_mammoth_lcu_off").setToggleableTo(23);
	public static final Underbarrel UNDERBARREL_MAMMOTH_LCU_ON         = new Underbarrel(23, "underbarrel_mammoth_lcu_on").setToggleableTo(22).setLaser(new Underbarrel.Laser(1F, 0F, 0F, 150D, false, true, false));
	public static final Underbarrel UNDERBARREL_UNI_GRIP_SMALL        = new Underbarrel(24, "underbarrel_uni_grip_small");
	public static final Underbarrel UNDERBARREL_SIG_MAGAZINEGRIP        = new Underbarrel(25, "underbarrel_sig_magazinegrip").setTransferableToRl("magazine_sig_med");
	public static final Underbarrel UNDERBARREL_CRAB_BIPOD_ALT        = new Underbarrel(26, "underbarrel_crab_bipod_alt");
	public static final Underbarrel UNDERBARREL_HANDLE_CRAB_ALT        = new Underbarrel(27, "underbarrel_handle_crab_alt");
	public static final Underbarrel UNDERBARREL_TYPE64_SUPRESSOR        = new Underbarrel(28, "underbarrel_type64_supressor");
	public static final Underbarrel UNDERBARREL_TOMMY_SHIT			= new Underbarrel(29, "underbarrel_tommy_shit");
	public static final Underbarrel UNDERBARREL_AMERICAN_PATCH			= new Underbarrel(30, "underbarrel_american_patch");

	public static final Underbarrel HANDLE_MACE_CLOTH			= new Underbarrel(31, "handle_mace_cloth");
	public static final Underbarrel HANDLE_MACE_RUBBER			= new Underbarrel(32, "handle_mace_rubber");
	public static final Underbarrel HANDLE_MACE_TAPE			= new Underbarrel(33, "handle_mace_tape");
	public static final Underbarrel PERFORATOR_FOREARM_GRIP			= new Underbarrel(34, "perforator_forearm_grip");

	public static final Auxiliary 	AUXILIARY_BIPOD 			= new Auxiliary(1, "auxiliary_bipod").setDriftModifierWhenShiftAndStill(0.25F).setInaccuracyModifierWhenShiftAndStill(0.25F);
	public static final Auxiliary 	AUXILIARY_STRAIGHT_PULL 	= new Auxiliary(2, "auxiliary_straight_pull").setIsAllowingReloadWhileZoomed(true);
	public static final Auxiliary 	LEFT_UNI_LCU_OFF     		= new Auxiliary(3, "left_uni_lcu_off").setToggleableTo(4).setTransferableToRl("right_uni_lcu_off");
	public static final Auxiliary 	LEFT_UNI_LCU_ON     		= new Auxiliary(4, "left_uni_lcu_on").setToggleableTo(3).setTransferableToRl("right_uni_lcu_on").setLaser(new Auxiliary.Laser(1F, 0F, 0F, 150D, false, true, false));
	public static final Auxiliary 	LEFT_UNI_FLASH_OFF     	    = new Auxiliary(5, "left_uni_flash_off").setToggleableTo(6).setTransferableToRl("right_uni_flash_off");
	public static final Auxiliary 	LEFT_UNI_FLASH_ON     		= new Auxiliary(6, "left_uni_flash_on").setToggleableTo(5).setTransferableToRl("right_uni_flash_on").setFlashActive();
	public static final Auxiliary 	LEFT_UNI_BAYONET     		= new Auxiliary(7, "left_uni_bayonet").setTransferableToRl("right_uni_bayonet");
	//костыль
	public static final Auxiliary 	LEFT_MAMMOTH_FLASH_OFF     	    = new Auxiliary(8, "underbarrel_mammoth_flash_off").setToggleableTo(9);
	public static final Auxiliary 	LEFT_MAMMOTH_FLASH_ON     		= new Auxiliary(9, "underbarrel_mammoth_flash_on").setToggleableTo(8).setFlashActive();


	public static final Ammo 		AMMO_12G_BUCKSHOT 			= new Ammo(1, "ammo_12g_buckshot").setReplacementCartridge(GunCus.CARTRIDGE_12G_BUCKSHOT);
	public static final Ammo 		AMMO_12G_DART 				= new Ammo(2, "ammo_12g_dart").setReplacementCartridge(GunCus.CARTRIDGE_12G_DART);
	public static final Ammo 		AMMO_12G_FRAG 				= new Ammo(3, "ammo_12g_frag").setReplacementCartridge(GunCus.CARTRIDGE_12G_FRAG);
	public static final Ammo 		AMMO_12G_SLUG 				= new Ammo(4, "ammo_12g_slug").setReplacementCartridge(GunCus.CARTRIDGE_12G_SLUG);
	public static final Ammo 	    RIGHT_UNI_LCU_OFF     		= new Ammo(5, "right_uni_lcu_off").setToggleableTo(6).setTransferableToRl("underbarrel_uni_lcu_off");
	public static final Ammo 	    RIGHT_UNI_LCU_ON     		= new Ammo(6, "right_uni_lcu_on").setToggleableTo(5).setTransferableToRl("underbarrel_uni_lcu_on").setLaser(new Ammo.Laser(1F, 0F, 0F, 150D, false, true, false));
	public static final Ammo 	    RIGHT_UNI_FLASH_OFF     	= new Ammo(7, "right_uni_flash_off").setToggleableTo(8).setTransferableToRl("underbarrel_uni_flash_off");
	public static final Ammo 	    RIGHT_UNI_FLASH_ON     		= new Ammo(8, "right_uni_flash_on").setToggleableTo(7).setTransferableToRl("underbarrel_uni_flash_on").setFlashActive();
	public static final Ammo 	    RIGHT_UNI_BAYONET    		= new Ammo(9, "right_uni_bayonet").setTransferableToRl("underbarrel_uni_bayonet");
	public static final Ammo 	    BUFF_KOSTIL    		= new Ammo(10, "buff_kostil");

	public static final Magazine 	MAGAZINE_QUICK_SWITCH 		= new Magazine(1, "magazine_quick_switch").setReloadTimeModifier(0.8F);
	public static final Magazine 	MAGAZINE_EXTENDED_10 		= new Magazine(2, "magazine_extended_10").setExtraCapacity(10);

	// carbine magazines
	public static final Magazine 	MAGAZINE_CARBINE		= new Magazine(3, "magazine_carbine").setReloadTimeModifier(0.8F);

	//asha mags
	public static final Magazine 	MAGAZINE_ASHA_BIG		= new Magazine(4, "magazine_asha_big");
	public static final Magazine 	MAGAZINE_ASHA_MED		= new Magazine(5, "magazine_asha_med");
	public static final Magazine 	MAGAZINE_ASHA_SMALL		= new Magazine(6, "magazine_asha_small");

	//mdr mags
	public static final Magazine 	MAGAZINE_MDR_BIG		= new Magazine(7, "magazine_mdr_big");
	public static final Magazine 	MAGAZINE_MDR_MED		= new Magazine(8, "magazine_mdr_med");
	public static final Magazine 	MAGAZINE_MDR_SMALL		= new Magazine(9, "magazine_mdr_small");

	//rpk mags
	public static final Magazine 	MAGAZINE_RPK_BIG		= new Magazine(10, "magazine_rpk_big");
	public static final Magazine 	MAGAZINE_RPK_MED		= new Magazine(11, "magazine_rpk_med");
	public static final Magazine 	MAGAZINE_RPK_SMALL		= new Magazine(12, "magazine_rpk_small");

	//uzi mags
	public static final Magazine 	MAGAZINE_UZI_BIG		= new Magazine(13, "magazine_uzi_big");
	public static final Magazine 	MAGAZINE_UZI_MED		= new Magazine(14, "magazine_uzi_med");

	//sar mags
	public static final Magazine 	MAGAZINE_SAR_MED		= new Magazine(15, "magazine_sar_med");

	//smg mags
	public static final Magazine 	MAGAZINE_SMG_BIG		= new Magazine(16, "magazine_smg_big");
	public static final Magazine 	MAGAZINE_SMG_MED		= new Magazine(17, "magazine_smg_med");
	public static final Magazine 	MAGAZINE_SMG_SMALL		= new Magazine(18, "magazine_smg_small");

	//hsmg mags
	public static final Magazine 	MAGAZINE_HSMG_BIG		= new Magazine(19, "magazine_hsmg_big");
	public static final Magazine 	MAGAZINE_HSMG_MED		= new Magazine(20, "magazine_hsmg_med");
	public static final Magazine 	MAGAZINE_HSMG_SMALL		= new Magazine(21, "magazine_hsmg_small");

	public static final Magazine 	MAGAZINE_FAL_MED		= new Magazine(22, "magazine_fal_med");

	public static final Magazine 	MAGAZINE_HMG_MED		= new Magazine(23, "magazine_hmg_med");

	public static final Magazine 	MAGAZINE_TRAUMAT_MED		= new Magazine(24, "magazine_traumat_med");

	public static final Magazine 	MAGAZINE_AUTOPISTOL_MED		= new Magazine(25, "magazine_autopistol_med");
	public static final Magazine 	MAGAZINE_AUTOPISTOL_BIG		= new Magazine(26, "magazine_autopistol_big");

	public static final Magazine 	MAGAZINE_HORN_MED		= new Magazine(27, "magazine_horn_med");
	public static final Magazine 	MAGAZINE_HORN_BIG		= new Magazine(28, "magazine_horn_big");

	public static final Magazine 	MAGAZINE_PISTOL_ALT_MED		= new Magazine(29, "magazine_pistol_alt_med");

	public static final Magazine 	MAGAZINE_TMP_MED		= new Magazine(30, "magazine_tmp_med");

	public static final Magazine 	MAGAZINE_SIG_MED		= new Magazine(31, "magazine_sig_med").setTransferableToRl("underbarrel_sig_magazinegrip");
	public static final Magazine 	MAGAZINE_SIG_BIG		= new Magazine(32, "magazine_sig_big");

	public static final Magazine 	MAGAZINE_UMP_MED		= new Magazine(33, "magazine_ump_med");

	public static final Magazine 	MAGAZINE_SVU_SMALL		= new Magazine(34, "magazine_svu_small");
	public static final Magazine 	MAGAZINE_SVU_MED		= new Magazine(35, "magazine_svu_med");
	public static final Magazine 	MAGAZINE_SVU_BIG		= new Magazine(36, "magazine_svu_big");

	public static final Magazine 	MAGAZINE_VYKHLOP_SMALL		= new Magazine(37, "magazine_vykhlop_small");
	public static final Magazine 	MAGAZINE_VYKHLOP_MED		= new Magazine(38, "magazine_vykhlop_med");
	public static final Magazine 	MAGAZINE_VYKHLOP_BIG		= new Magazine(39, "magazine_vykhlop_big");
	//public static final Magazine 	MAGAZINE_PISTOLVAS_MED		= new Magazine(31, "magazine_pistolvas_med");
	public static final Magazine 	MAGAZINE_MAMMOTH_MED		= new Magazine(40, "magazine_mammoth_med");
	public static final Magazine 	MAGAZINE_MAMMOTH_BIG		= new Magazine(41, "magazine_mammoth_big");

	public static final Magazine 	MAGAZINE_SCORPIO_MED		= new Magazine(42, "magazine_scorpio_med");
	public static final Magazine 	MAGAZINE_SCORPIO_BIG		= new Magazine(43, "magazine_scorpio_big");

	public static final Magazine 	MAGAZINE_PV_MED		= new Magazine(44, "magazine_pv_med");
	public static final Magazine 	MAGAZINE_PV_BIG		= new Magazine(45, "magazine_pv_big");

	public static final Magazine 	MAGAZINE_M60_BIG		= new Magazine(46, "magazine_m60_big");

	public static final Magazine 	MAGAZINE_PPAUTO_SMALL		= new Magazine(47, "magazine_ppauto_small");
	public static final Magazine 	MAGAZINE_PPAUTO_MED		= new Magazine(48, "magazine_ppauto_med");
	public static final Magazine 	MAGAZINE_PPAUTO_BIG		= new Magazine(49, "magazine_ppauto_big");

	public static final Magazine 	MAGAZINE_VPAUTO_SMALL		= new Magazine(50, "magazine_vpauto_small");
	public static final Magazine 	MAGAZINE_VPAUTO_MED		= new Magazine(51, "magazine_vpauto_med");
	public static final Magazine 	MAGAZINE_VPAUTO_BIG		= new Magazine(52, "magazine_vpauto_big");

	public static final Magazine 	MAGAZINE_SAR_SMALL		= new Magazine(53, "magazine_sar_small");
	public static final Magazine 	MAGAZINE_SAR_BIG		= new Magazine(54, "magazine_sar_big");

	public static final Magazine 	MAGAZINE_TASER_CARTRIDGE		= new Magazine(55, "magazine_taser_cartridge");

	public static final Magazine 	MAGAZINE_LASERPISTOL_MED		= new Magazine(56, "magazine_laserpistol_med");

	public static final Magazine 	MAGAZINE_CRAB_MED		= new Magazine(57, "magazine_crab_med");
	public static final Magazine 	MAGAZINE_CRAB_MED_ALT		= new Magazine(58, "magazine_crab_med_alt");
	public static final Magazine 	MAGAZINE_CRAB_MAXIMUM_DAKKA		= new Magazine(59, "magazine_crab_maximum_dakka");

	public static final Magazine 	MAGAZINE_NOTLUGER_MED		= new Magazine(60, "magazine_notluger_med");
	public static final Magazine 	MAGAZINE_NOTLUGER_BIG		= new Magazine(61, "magazine_notluger_big");

	public static final Magazine 	MAGAZINE_SHAUCHAT_MED		= new Magazine(62, "magazine_shauchat_med");
	public static final Magazine 	MAGAZINE_SHAUCHAT_BIG		= new Magazine(63, "magazine_shauchat_big");

	public static final Magazine 	MAGAZINE_HYP_SMALL		= new Magazine(64, "magazine_hyp_small");
	public static final Magazine 	MAGAZINE_HYP_MED		= new Magazine(65, "magazine_hyp_med");
	public static final Magazine 	MAGAZINE_HYP_BIG		= new Magazine(66, "magazine_hyp_big");

	public static final Magazine 	MAGAZINE_BAR_MED		= new Magazine(67, "magazine_bar_med");
	public static final Magazine 	MAGAZINE_BAR_BIG		= new Magazine(68, "magazine_bar_big");
	public static final Magazine 	MAGAZINE_BAR_SMALL		= new Magazine(69, "magazine_bar_small");

	public static final Magazine 	MAGAZINE_PPS_SMALL		= new Magazine(70, "magazine_pps_small");
	public static final Magazine 	MAGAZINE_PPS_MED		= new Magazine(71, "magazine_pps_med");
	public static final Magazine 	MAGAZINE_PPS_BIG		= new Magazine(72, "magazine_pps_big");

	public static final Magazine 	MAGAZINE_M21_MED		= new Magazine(73, "magazine_m21_med");
	public static final Magazine 	MAGAZINE_M21_BIG		= new Magazine(74, "magazine_m21_big");
	public static final Magazine 	MAGAZINE_M21_SMALL		= new Magazine(75, "magazine_m21_small");

	public static final Magazine 	MAGAZINE_KOROVIN_MED		= new Magazine(76, "magazine_korovin_med");
	public static final Magazine 	MAGAZINE_KOROVIN_BIG		= new Magazine(77,"magazine_korovin_big");

	public static final Magazine 	MAGAZINE_AUTOMAG_MED		= new Magazine(78, "magazine_automag_med");
	public static final Magazine 	MAGAZINE_AUTOMAG_BIG		= new Magazine(79, "magazine_automag_big");

	public static final Magazine 	MAGAZINE_PM_MED		= new Magazine(80, "magazine_pm_med");
	public static final Magazine 	MAGAZINE_PM_BIG		= new Magazine(81, "magazine_pm_big");

	public static final Magazine 	MAGAZINE_APS_MED		= new Magazine(82, "magazine_aps_med");
	public static final Magazine 	MAGAZINE_APS_BIG		= new Magazine(83, "magazine_aps_big");

	public static final Magazine 	MAGAZINE_SCORPIONEW_MED		= new Magazine(84, "magazine_scorpionew_med");
	public static final Magazine 	MAGAZINE_SCORPIONEW_DRUM		= new Magazine(85, "magazine_scorpionew_drum");
	public static final Magazine 	MAGAZINE_SCORPIONEW_BIG		= new Magazine(86, "magazine_scorpionew_big");
	public static final Magazine 	MAGAZINE_SCORPIONEW_SMALL		= new Magazine(87, "magazine_scorpionew_small");

	public static final Magazine 	MAGAZINE_OWEN_MED		= new Magazine(88, "magazine_owen_med");
	public static final Magazine 	MAGAZINE_OWEN_DRUM		= new Magazine(89, "magazine_owen_drum");
	public static final Magazine 	MAGAZINE_OWEN_BIG		= new Magazine(90, "magazine_owen_big");
	public static final Magazine 	MAGAZINE_OWEN_SMALL		= new Magazine(91, "magazine_owen_small");

	public static final Magazine 	MAGAZINE_TYPE64_MED		= new Magazine(92, "magazine_type64_med");
	public static final Magazine 	MAGAZINE_TYPE64_BIG		= new Magazine(93, "magazine_type64_big");

	public static final Magazine 	MAGAZINE_TOMMY_DRUM		= new Magazine(94, "magazine_tommy_drum");
	public static final Magazine 	MAGAZINE_TOMMY_MED		= new Magazine(95, "magazine_tommy_med");

	public static final Magazine 	MAGAZINE_MOSIN_MED		= new Magazine(96, "magazine_mosin_med");

	public static final Magazine 	MAGAZINE_M1A1_MED		= new Magazine(97, "magazine_m1a1_med");
	public static final Magazine 	MAGAZINE_M1A1_MED_ALT		= new Magazine(98, "magazine_m1a1_med_alt");
	public static final Magazine 	MAGAZINE_M1A1_SMALL		= new Magazine(99, "magazine_m1a1_small");

	public static final Magazine 	MAGAZINE_SVT_MED		= new Magazine(100, "magazine_svt_med");
	public static final Magazine 	MAGAZINE_SVT_BIG		= new Magazine(101, "magazine_svt_big");
	public static final Magazine 	MAGAZINE_SVT_SMALL		= new Magazine(102, "magazine_svt_small");

	public static final Magazine 	MAGAZINE_MAS_MED		= new Magazine(103, "magazine_mas_med");
	public static final Magazine 	MAGAZINE_MAS_BIG		= new Magazine(104, "magazine_mas_big");
	public static final Magazine 	MAGAZINE_MAS_SMALL		= new Magazine(105, "magazine_mas_small");

	public static final Magazine 	MAGAZINE_AMERICAN_DRUM		= new Magazine(106, "magazine_american_drum");

	public static final Magazine 	MAGAZINE_MG34_BIG		= new Magazine(107, "magazine_mg34_big");
	public static final Magazine 	MAGAZINE_MG34_MED		= new Magazine(108, "magazine_mg34_med");

	public static final Magazine 	MAGAZINE_VAG_MED		= new Magazine(109, "magazine_vag_med");
	public static final Magazine 	MAGAZINE_VAG_BIG		= new Magazine(110, "magazine_vag_big");

	public static final Magazine 	MAGAZINE_PITCH_BIG		= new Magazine(111, "magazine_pitch_big");
	public static final Magazine 	MAGAZINE_PITCH_MED		= new Magazine(112, "magazine_pitch_med");
	public static final Magazine 	MAGAZINE_PITCH_SMALL		= new Magazine(113, "magazine_pitch_small");

	public static final Magazine 	MAGAZINE_FG42_SMALL		= new Magazine(114, "magazine_fg42_small");
	public static final Magazine 	MAGAZINE_FG42_MED		= new Magazine(115, "magazine_fg42_med");
	public static final Magazine 	MAGAZINE_FG42_BIG		= new Magazine(116, "magazine_fg42_big");

	public static final Magazine 	MAGAZINE_RUBY_MED		= new Magazine(117, "magazine_ruby_med");
	public static final Magazine 	MAGAZINE_RUBY_BIG		= new Magazine(118, "magazine_ruby_big");

	public static final Magazine 	MAGAZINE_ARZERO_SMALL		= new Magazine(119, "magazine_arzero_small");
	public static final Magazine 	MAGAZINE_ARZERO_MED		= new Magazine(120, "magazine_arzero_med");
	public static final Magazine 	MAGAZINE_ARZERO_BIG		= new Magazine(121, "magazine_arzero_big");

	public static final Magazine 	BIGGUN_MORTAR		= new Magazine(122, "biggun_mortar");

	public static final Magazine 	NRPMAGAZ		= new Magazine(123, "nrpmagaz");

	public static final Magazine 	MAGAZINE_LONESTAR_BIG		= new Magazine(124, "magazine_lonestar_big");
	public static final Magazine 	MAGAZINE_LONESTAR_MED		= new Magazine(125, "magazine_lonestar_med");

	public static final Magazine 	MAGAZINE_CG45_BIG		= new Magazine(126, "magazine_cg45_big");
	public static final Magazine 	MAGAZINE_CG45_MED		= new Magazine(127, "magazine_cg45_med");
	public static final Magazine 	MAGAZINE_CG45_SMALL		= new Magazine(128, "magazine_cg45_small");

	public static final Magazine 	MAGAZINE_HELLCAT_SMALL		= new Magazine(129, "magazine_hellcat_small");
	public static final Magazine 	MAGAZINE_HELLCAT_MED		= new Magazine(130, "magazine_hellcat_med");

	public static final Magazine 	MAGAZINE_G3R1_SMALL		= new Magazine(131, "magazine_g3r1_small");
	public static final Magazine 	MAGAZINE_G3R1_MED		= new Magazine(132, "magazine_g3r1_med");
	public static final Magazine 	MAGAZINE_G3R1_BIG		= new Magazine(133, "magazine_g3r1_big");

	public static final Magazine 	MAGAZINE_EXCELSIOR_DRUM		= new Magazine(134, "magazine_excelsior_drum");
	public static final Magazine 	MAGAZINE_EXCELSIOR_MED		= new Magazine(135, "magazine_excelsior_med");

	public static final Magazine 	MAGAZINE_SHITREVOLVER_MED		= new Magazine(136, "magazine_shitrevolver_med");
	public static final Magazine 	MAGAZINE_SHITREVOLVER_CHAIN		= new Magazine(137, "magazine_shitrevolver_chain");

	public static final Magazine 	MAGAZINE_AKSU_BIG	= new Magazine(138, "magazine_aksu_big");
	public static final Magazine 	MAGAZINE_AKSU_MED	= new Magazine(139, "magazine_aksu_med");
	public static final Magazine 	MAGAZINE_AKSU_SMALL	= new Magazine(140, "magazine_aksu_small");

	public static final Magazine 	MAGAZINE_VAG72_SMALL	= new Magazine(141, "magazine_vag72_small");
	public static final Magazine 	MAGAZINE_VAG72_MED	= new Magazine(142, "magazine_vag72_med");
	public static final Magazine 	MAGAZINE_VAG72_BIG	= new Magazine(143, "magazine_vag72_big");

	public static final Magazine 	MAGAZINE_AKS_MED	= new Magazine(144, "magazine_aks_med");

	public static final Magazine 	MAGAZINE_SKS_MED	= new Magazine(145, "magazine_sks_med");
	public static final Magazine 	MAGAZINE_SKS_BIG	= new Magazine(146, "magazine_sks_big");

	public static final Magazine 	MAGAZINE_ATR_MED	= new Magazine(147, "magazine_atr_med");

	public static final Magazine 	WASP_MAG_SMALL	= new Magazine(148, "wasp_mag_small");
	public static final Magazine 	WASP_MAG_MEDIUM	= new Magazine(149, "wasp_mag_medium");
	public static final Magazine 	WASP_MAG_LARGE	= new Magazine(150, "wasp_mag_large");

	public static final Magazine 	BULL_MAG_SMALL	= new Magazine(151, "bull_mag_small");
	public static final Magazine 	BULL_MAG_MED	= new Magazine(152, "bull_mag_med");

	public static final Magazine 	NETTLE_MAG_SMALL	= new Magazine(153, "nettle_mag_small");
	public static final Magazine 	NETTLE_MAG_MEDIUM	= new Magazine(154, "nettle_mag_medium");

	public static final Magazine 	PERFORATOR_MAG_LARGE	= new Magazine(155, "perforator_mag_large");

	public static final Magazine 	CHEM_SPRAYER_CAN	= new Magazine(156, "chem_sprayer_can");

	public static final Paint 		PAINT_BLACK 				= new Paint(1, "paint_black");
	public static final Paint 		PAINT_BLUE 					= new Paint(2, "paint_blue");
	public static final Paint 		PAINT_GREEN 				= new Paint(3, "paint_green");
	public static final Paint 		PAINT_ORANGE 				= new Paint(4, "paint_orange");
	public static final Paint 		PAINT_PINK 					= new Paint(5, "paint_pink");
	public static final Paint 		PAINT_RED 					= new Paint(6, "paint_red");
	public static final Paint 		PAINT_TURQUOISE 			= new Paint(7, "paint_turquoise");
	public static final Paint 		PAINT_WHITE 				= new Paint(8, "paint_white");
	public static final Paint 		PAINT_YELLOW 				= new Paint(9, "paint_yellow");

	public static final Paint 		PAINT_RAIDER 				= new Paint(10, "paint_raider");
	public static final Paint 		PAINT_GOLD 					= new Paint(11, "paint_gold");

	public static final Paint 		PAINT_STICKER 					= new Paint(12, "paint_sticker");

	public static final Paint 		PAINT_KATANA 					= new Paint(13, "paint_katana");

	public static final Paint 		PAINT_TOY 					= new Paint(14, "paint_toy");

	public static final Paint 		PAINT_PURPLE 					= new Paint(15, "paint_purple");

	public static final ItemGun GUN_AK_74M 		= GunCus.createAssaultRifle("gun_ak_74m", 3, 30, 5F, GunCus.CARTRIDGE_5_45x39mm);
	public static final ItemGun GUN_M16A4 		= GunCus.createAssaultRifle("gun_m16a4", 3, 30, 5F, GunCus.CARTRIDGE_5_56x45mm);
//	public static final ItemGun GUN_CARBINE     = GunCus.createCarbine("gun_carbine", 3, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
//	public static final ItemGun GUN_HORN        = GunCus.createGun("gun_horn", 3, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
//	public static final ItemGun GUN_HORN        = GunCus.createGun("gun_horn", 3, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_TOZ         = GunCus.createGun("gun_toz", 3, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_ASHA        = GunCus.createAsha("gun_asha", 3, 30, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MDR        = GunCus.createMdr("gun_mdr", 3, 30, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_RPK        = GunCus.createRpk("gun_rpk", 3, 30, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_RPG18        = GunCus.createRpg18("gun_rpg18", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_UZI        = GunCus.createUzi("gun_uzi", 3, 30, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_REM        = GunCus.createRem("gun_rem", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAR        = GunCus.createSar("gun_sar", 3, 30, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SMG        = GunCus.createSmg("gun_smg", 3, 30, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_HSMG        = GunCus.createHsmg("gun_hsmg", 3, 30, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_DOUBLEBARREL        = GunCus.createDoublebarrel("gun_doublebarrel", 2, 2, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_FAL        = GunCus.createFal("gun_fal", 3, 30, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_HMG        = GunCus.createHmg("gun_hmg", 3, 30, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_GL        = GunCus.createGl("gun_gl", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_TRAUMAT        = GunCus.createTraumat("gun_traumat", 1, 7, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SIGNAL        = GunCus.createSignal("gun_signal", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_HEAVYREVOLVER        = GunCus.createHeavyrevolver("gun_heavyrevolver", 1, 6, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_LADY       = GunCus.createLadygun("gun_lady", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_AUTOPISTOL       = GunCus.createAutopistol("gun_autopistol", 3, 7, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_PISTOLHORN       = GunCus.createPistolhorn("gun_pistolhorn", 1, 7, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_PISTOLALT       = GunCus.createPistolalt("gun_pistol_alt", 1, 7, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_TMP       = GunCus.createTmp("gun_tmp", 3, 7, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_LIGHTREVOLVER        = GunCus.createLightrevolver("gun_lightrevolver", 1, 6, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_REVOLVER        = GunCus.createRevolver("gun_revolver", 1, 6, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_PISTOLVAS       = GunCus.createPistolvas("gun_pistolvas", 1, 7, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MAMMOTH       = GunCus.createMammoth("gun_mammoth", 1, 7, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SIG      = GunCus.createSig("gun_sig", 1, 7, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_UMP      = GunCus.createUmp("gun_ump", 3, 30, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SVU      = GunCus.createSvu("gun_svu", 1, 20, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_VYKHLOP      = GunCus.createVykhlop("gun_vykhlop", 1, 20, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MOSSBERG      = GunCus.createMossberg("gun_mossberg", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_HEAVYRIFLE      = GunCus.createHeavyrifle("gun_heavyrifle", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_EMP      = GunCus.createEmp("gun_emp", 1, 10, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_CROSSBOW      = GunCus.createCrossbow("gun_crossbow", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_COMPOUNDCROSSBOW      = GunCus.createCompoundcrossbow("gun_compoundcrossbow", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_RAILGUN      = GunCus.createRailgun("gun_railgun", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_RIG      = GunCus.createRig("gun_rig", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun SWORD_MODULAR      = GunCus.createSwordmodular("sword_modular", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm).setMelee(true);
	public static final ItemGun GUN_SIG_AUTO       = GunCus.createSigauto("gun_sig_auto", 3, 7, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SCORPIO      = GunCus.createScorpio("gun_scorpio", 3, 7, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MC255      = GunCus.createMc255("gun_mc255", 1, 6, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_M60      = GunCus.createM60("gun_m60", 5, 90, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_HEAVYSHOTGUN      = GunCus.createHeavyshotgun("gun_heavyshotgun", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_FLAMETHROWER      = GunCus.createFlamethrower("gun_flamethrower", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_FLAMETHROWER_SANITAR      = GunCus.createFlamethrowersanitar("gun_flamethrower_sanitar", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL_PISTOL_LONG_METAL      = GunCus.createSamopalpistol("gun_samopal_pistol_long_metal", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL_PISTOL_LONG_WOOD      = GunCus.createSamopalpistol("gun_samopal_pistol_long_wood", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL_PISTOL_MEDIUM_METAL      = GunCus.createSamopalpistol("gun_samopal_pistol_medium_metal", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL_PISTOL_MEDIUM_WOOD      = GunCus.createSamopalpistol("gun_samopal_pistol_medium_wood", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL_PISTOL_SHORT_METAL      = GunCus.createSamopalpistol("gun_samopal_pistol_short_metal", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL_PISTOL_SHORT_WOOD      = GunCus.createSamopalpistol("gun_samopal_pistol_short_wood", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);

	public static final ItemGun GUN_SAMOPAL_RIFLE_LONG_METAL      = GunCus.createSamopalrifle("gun_samopal_rifle_long_metal", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL_RIFLE_LONG_WOOD      = GunCus.createSamopalrifle("gun_samopal_rifle_long_wood", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL_RIFLE_MEDIUM_METAL      = GunCus.createSamopalrifle("gun_samopal_rifle_medium_metal", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL_RIFLE_MEDIUM_WOOD      = GunCus.createSamopalrifle("gun_samopal_rifle_medium_wood", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL_RIFLE_SHORT_METAL      = GunCus.createSamopalrifle("gun_samopal_rifle_short_metal", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL_RIFLE_SHORT_WOOD      = GunCus.createSamopalrifle("gun_samopal_rifle_short_wood", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_TASER      = GunCus.createTaser("gun_taser", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_LIGHTRIFLE      = GunCus.createLightrifle("gun_lightrifle", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_LASERPISTOL      = GunCus.createLaserpistol("gun_laserpistol", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);

	public static final ItemGun SWORD_KATANA      = GunCus.createSwordkatana("sword_katana", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm).setMelee(true);

	public static final ItemGun GUN_DAKKA      = GunCus.createDakka("gun_dakka", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);

	public static final ItemGun GUN_NOTLUGER      = GunCus.createNotluger("gun_notluger", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);

	public static final ItemGun GUN_SHAUCHAT      = GunCus.createShauchat("gun_shauchat", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);

	public static final ItemGun GUN_HYPERION      = GunCus.createHyperion("gun_hyperion", 1, 8, 5F, GunCus.CARTRIDGE_5_56x45mm);

	public static final ItemGun GUN_WINCHESTER      = GunCus.createWinchester("gun_winchester", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MOSSBERG2      = GunCus.createMossberg2("gun_mossberg2", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_BAR      = GunCus.createBar("gun_bar", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);

	public static final ItemGun GUN_PPS     = GunCus.createPps("gun_pps", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_M21     = GunCus.createM21("gun_m21", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_REICHSREVOLVER     = GunCus.createReichsrevolver("gun_reichsrevolver", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_KOROVIN     = GunCus.createKorovin("gun_korovin", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_AUTOMAG     = GunCus.createAutomag("gun_automag", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_PM     = GunCus.createPm("gun_pm", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_APS     = GunCus.createAps("gun_aps", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SCORPIONEW     = GunCus.createScorpionew("gun_scorpionew", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_OWEN     = GunCus.createOwen("gun_owen", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_TYPE64     = GunCus.createType64("gun_type64", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_TOMMY     = GunCus.createTommy("gun_tommy", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MOSIN     = GunCus.createMosin("gun_mosin", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAUER     = GunCus.createSauer("gun_sauer", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_M1A1     = GunCus.createM1a1("gun_m1a1", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SVT     = GunCus.createSvt("gun_svt", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MAS     = GunCus.createMas("gun_mas", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MG34     = GunCus.createMg34("gun_mg34", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_AMERICAN     = GunCus.createAmerican("gun_american", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MANLICHER     = GunCus.createManlicher("gun_manlicher", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SJORGEN     = GunCus.createSjorgen("gun_sjorgen", 1, 5, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun MACE      = GunCus.createMace("mace", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm).setMelee(true);
	public static final ItemGun GUN_VAG      = GunCus.createVag("gun_vag", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_BORCHADT      = GunCus.createBorchadt("gun_borchadt", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_PITCH      = GunCus.createPitch("gun_pitch", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun BOTTLEBROWN      = GunCus.createBottlebrown("bottlebrown", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_BIGGUN      = GunCus.createBiggun("gun_biggun", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_FG42      = GunCus.createFg42("gun_fg42", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_RUBY      = GunCus.createRuby("gun_ruby", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_NAGAN      = GunCus.createNagan("gun_nagan", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_GIGASHOT      = GunCus.createGigashot("gun_gigashot", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GRENADE      = GunCus.createGrenade("grenade", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun BAT      = GunCus.createBat("bat", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm).setMelee(true);
	public static final ItemGun GUN_HOOK      = GunCus.createHook("gun_hook", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm).setMelee(true);
	public static final ItemGun GUN_BLUNDERBASS = GunCus.createBlunderbass("gun_blunderbass", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_BITA = GunCus.createBita("gun_bita", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm).setMelee(true);
	public static final ItemGun GUN_ARZERO = GunCus.createArzero("gun_arzero", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_REVOLRIFLE = GunCus.createRevolrifle("gun_revolrifle", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_REVOLVERGARIK = GunCus.createRevolvergarik("gun_revolvergarik", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_ANKASMOL = GunCus.createAnkasmol("gun_ankasmol", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_BERGMANN = GunCus.createBergmann("gun_bergmann", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MAUSER = GunCus.createMauser("gun_mauser", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_NRPGUN = GunCus.createNrpgun("gun_nrpgun", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MUSKET = GunCus.createMusket("gun_musket", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_REMINGTON = GunCus.createRemington("gun_remington", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_THATGUN = GunCus.createThatgun("gun_thatgun", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_TRIGUN = GunCus.createTrigun("gun_trigun", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_FLINTSTOCK = GunCus.createFlintstock("gun_flintstock", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_GUNBLADE2 = GunCus.createGunblade2("gun_gunblade2", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun SHOVEL = GunCus.createShovel("shovel", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm).setMelee(true);
	public static final ItemGun CANE2 = GunCus.createCane2("cane2", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm).setMelee(true);
	public static final ItemGun GUN_ONESHOT = GunCus.createOneshot("gun_oneshot", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_REVGANGST = GunCus.createRevgangst("gun_revgangst", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_LONESTAR = GunCus.createLonestar("gun_lonestar", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_CG45 = GunCus.createCg45("gun_cg45", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_HELLCAT = GunCus.createHellcat("gun_hellcat", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_G3R1 = GunCus.createG3r1("gun_g3r1", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_RAILSHUTZE = GunCus.createRailshutze("gun_railshutze", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_EXCELSIOR = GunCus.createExcelsior("gun_excelsior", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_BN1900 = GunCus.createBn1900("gun_bn1900", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SHITREVOLVER = GunCus.createShitrevolver("gun_shitrevolver", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_AKSU = GunCus.createAksu("gun_aksu", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_VAG72 = GunCus.createVag72("gun_vag72", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MAGNUM = GunCus.createMagnum("gun_magnum", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_AKS = GunCus.createAks("gun_aks", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_DEVASTATOR = GunCus.createDevastator("gun_devastator", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SKS = GunCus.createSks("gun_sks", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_INJRIFLE = GunCus.createInjrifle("gun_injrifle", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MOPER = GunCus.createMoper("gun_moper", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_NEBULA = GunCus.createNebula("gun_nebula", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_ATR = GunCus.createAtr("gun_atr", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_BW = GunCus.createBw("gun_bw", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_BP = GunCus.createBp("gun_bp", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_RAPIER1 = GunCus.createRapier1("gun_rapier1", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm).setMelee(true);
	public static final ItemGun GUN_TITUSGUNBERD = GunCus.createTitusgunberd("gun_titusgunberd", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm).setMelee(true);
	public static final ItemGun GUN_MATEBA = GunCus.createMateba("gun_mateba", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_MATEBA2 = GunCus.createMateba("gun_mateba2", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL2 = GunCus.createSamopal2("gun_samopal2", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_HLAMBOLET = GunCus.createHlambolet("gun_hlambolet", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL1 = GunCus.createSamopal1("gun_samopal1", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL3 = GunCus.createSamopal3("gun_samopal3", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL4 = GunCus.createSamopal4("gun_samopal4", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL5 = GunCus.createSamopal5("gun_samopal5", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL6 = GunCus.createSamopal6("gun_samopal6", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL7 = GunCus.createSamopal7("gun_samopal7", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL8 = GunCus.createSamopal8("gun_samopal8", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL9 = GunCus.createSamopal9("gun_samopal9", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_SAMOPAL10 = GunCus.createSamopal10("gun_samopal10", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);
	public static final ItemGun GUN_CHEMGUN = GunCus.createChemgun("gun_chemgun", 1, 1, 5F, GunCus.CARTRIDGE_5_56x45mm);

	// ARMOR
	// public static ArmorMaterial addArmorMaterial(String name, String textureName, int durability, int[] reductionAmounts, int enchantability, SoundEvent soundOnEquip, float toughness)
	public static final ItemArmor.ArmorMaterial TEST_ARMOR_MATERIAL = EnumHelper.addArmorMaterial("testarmor", GunCus.MOD_ID + ":testarmor", 2, new int[] {1, 1, 1, 1}, 0, SoundEvents.ITEM_ARMOR_EQUIP_IRON, 0);
	public static final ArmorPiece TEST_ARMOR = new ArmorPiece("testarmor", TEST_ARMOR_MATERIAL, 4, EntityEquipmentSlot.CHEST);

	public static final ItemArmor.ArmorMaterial INVISIBLE_ARMOR_MATERIAL = EnumHelper.addArmorMaterial("invisible_armor", GunCus.MOD_ID + ":invisible_armor", 2, new int[] {1, 1, 1, 1}, 0, SoundEvents.ITEM_ARMOR_EQUIP_IRON, 0);
	public static final ArmorPiece INVISIBLE_HELMET = new ArmorPiece("invisible_helmet", INVISIBLE_ARMOR_MATERIAL, 0, EntityEquipmentSlot.HEAD);
	public static final ArmorPiece INVISIBLE_CHEST = new ArmorPiece("invisible_chest", INVISIBLE_ARMOR_MATERIAL, 0, EntityEquipmentSlot.CHEST);
	public static final ArmorPiece INVISIBLE_LEGGINGS = new ArmorPiece("invisible_leggings", INVISIBLE_ARMOR_MATERIAL, 0, EntityEquipmentSlot.LEGS);
	public static final ArmorPiece INVISIBLE_BOOTS = new ArmorPiece("invisible_boots", INVISIBLE_ARMOR_MATERIAL, 0, EntityEquipmentSlot.FEET);

	public static final ItemGCI FIRST_ITEM = new ItemGCI("fork", KMRP_TAB);
	public static ItemGCI[] static_items;
	public static ItemDyeable[] static_itemsdyeable;
	public static ItemFoodGCI[] static_itemsfood;
	public static ItemDyeableFood[] static_itemsdyeablefood;
	public static ItemShield[] static_itemsshield;
	public static ItemSword[] static_itemssword;
	@SidedProxy(clientSide = "de.cas_ual_ty.gci.client.ProxyClient", serverSide = "de.cas_ual_ty.gci.Proxy")
	public static Proxy proxy;

	public static SimpleNetworkWrapper channel;

	@Instance
	public static GunCus instance;

	public static boolean fullCreativeTabs = false;

	public static int getIDFromRl(String rl) {
		for (SoundEventGCI seg : soundEventList) {
			if (rl.equals(seg.rl)) return seg.id;
		}
		return 0;
	}

    @EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		GunCus.proxy.preInit(event);

		Configuration config = new Configuration(event.getModConfigurationDirectory());
		GunCus.fullCreativeTabs = config.getBoolean("fullCreativeTabs", "general", false, "If true, all guns come with their own creative tab containing all possible variants. Keep this off if you have JEI or similar mods installed.");
	}

	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		GunCus.proxy.init(event);
	}

	// KM
	@Mod.EventHandler
	public void onServerStart(FMLServerStartingEvent event) {
		proxy.startServer(event);
	}
	// RP

	public static ItemGun createAssaultRifle(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				.addAttachment(GunCus.OPTIC_REFLEX)
				.addAttachment(GunCus.OPTIC_COYOTE)
				.addAttachment(GunCus.OPTIC_KOBRA)
				.addAttachment(GunCus.OPTIC_HOLO)
				.addAttachment(GunCus.OPTIC_HD33)
				.addAttachment(GunCus.OPTIC_PKAS)
				//		.addAttachment(GunCus.OPTIC_IRNV)
				//		.addAttachment(GunCus.OPTIC_FLIR)
				.addAttachment(GunCus.OPTIC_M145)
				.addAttachment(GunCus.OPTIC_PRISMA)
				.addAttachment(GunCus.OPTIC_PKA)
				.addAttachment(GunCus.OPTIC_ACOG)
				.addAttachment(GunCus.OPTIC_JGM4)
				.addAttachment(GunCus.OPTIC_PSO1)

				.addAttachment(GunCus.ACCESSORY_MAGNIFIER)
				//		.addAttachment(GunCus.ACCESSORY_FLASH_LIGHT)
				//		.addAttachment(GunCus.ACCESSORY_TACTICAL_LIGHT)
				.addAttachment(GunCus.ACCESSORY_LASER_SIGHT)
				.addAttachment(GunCus.ACCESSORY_TRI_BEAM_LASER)
				.addAttachment(GunCus.ACCESSORY_GREEN_LASER_SIGHT)
				//		.addAttachment(GunCus.ACCESSORY_LASER_LIGHT_COMBO)

				.addAttachment(GunCus.BARREL_HEAVY_BARREL)
				.addAttachment(GunCus.BARREL_SUPPRESSOR)
				.addAttachment(GunCus.BARREL_LS06_SUPPRESSOR)
				.addAttachment(GunCus.BARREL_PBS4_SUPPRESSOR)
				.addAttachment(GunCus.BARREL_R2_SUPPRESSOR)
				.addAttachment(GunCus.BARREL_FLASH_HIDER)
				.addAttachment(GunCus.BARREL_COMPENSATOR)
				.addAttachment(GunCus.BARREL_MUZZLE_BRAKE)

				.addAttachment(GunCus.UNDERBARREL_ERGO_GRIP)
				.addAttachment(GunCus.UNDERBARREL_ANGLED_GRIP)
				.addAttachment(GunCus.UNDERBARREL_STUBBY_GRIP)
				.addAttachment(GunCus.UNDERBARREL_VERTICAL_GRIP)
				.addAttachment(GunCus.UNDERBARREL_FOLDING_GRIP)
				.addAttachment(GunCus.UNDERBARREL_POTATO_GRIP)

				.addAttachment(GunCus.MAGAZINE_QUICK_SWITCH)
				.addAttachment(GunCus.MAGAZINE_EXTENDED_10)

				.addAttachment(GunCus.PAINT_BLACK)
				.addAttachment(GunCus.PAINT_BLUE)
				.addAttachment(GunCus.PAINT_GREEN)
				.addAttachment(GunCus.PAINT_ORANGE)
				.addAttachment(GunCus.PAINT_PINK)
				.addAttachment(GunCus.PAINT_RED)
				.addAttachment(GunCus.PAINT_TURQUOISE)
				.addAttachment(GunCus.PAINT_WHITE)
				.addAttachment(GunCus.PAINT_YELLOW);
	}

/*	public static ItemGun createCarbine(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_CARBINE_COLIMATOR_SCOPE)
				.addAttachment(GunCus.OPTIC_CARBINE_SCOPE)

				.addAttachment(GunCus.BARREL_MEWT_SUPPRESSOR)

				.addAttachment(GunCus.MAGAZINE_CARBINE);
	}*/

	public static ItemGun createSar(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)

				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)

				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRENADELAUNCHER)

				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)

				.addAttachment(GunCus.MAGAZINE_SAR_SMALL)
				.addAttachment(GunCus.MAGAZINE_SAR_MED)
				.addAttachment(GunCus.MAGAZINE_SAR_BIG);
	}

	public static ItemGun createAsha(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)

				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRENADELAUNCHER)

				.addAttachment(GunCus.MAGAZINE_ASHA_BIG)
		  		.addAttachment(GunCus.MAGAZINE_ASHA_MED)
				.addAttachment(GunCus.MAGAZINE_ASHA_SMALL);
	}

	public static ItemGun createMdr(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)

				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)

				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRENADELAUNCHER)

				.addAttachment(GunCus.MAGAZINE_MDR_BIG)
				.addAttachment(GunCus.MAGAZINE_MDR_MED)
				.addAttachment(GunCus.MAGAZINE_MDR_SMALL);
	}

	public static ItemGun createRpk(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)

				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)

				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRENADELAUNCHER)

				.addAttachment(GunCus.LEFT_UNI_LCU_OFF)
				.addAttachment(GunCus.LEFT_UNI_LCU_ON)
				.addAttachment(GunCus.LEFT_UNI_FLASH_OFF)
				.addAttachment(GunCus.LEFT_UNI_FLASH_ON)
				.addAttachment(GunCus.LEFT_UNI_BAYONET)


				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)
				.addAttachment(GunCus.RIGHT_UNI_BAYONET)

				.addAttachment(GunCus.STOCK_RPK_BASE)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)

				.addAttachment(GunCus.MAGAZINE_RPK_BIG)
				.addAttachment(GunCus.MAGAZINE_RPK_MED)
				.addAttachment(GunCus.MAGAZINE_RPK_SMALL);
	}

	public static ItemGun createRem(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)

/*				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)*/

				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRENADELAUNCHER)

				.addAttachment(GunCus.LEFT_UNI_LCU_OFF)
				.addAttachment(GunCus.LEFT_UNI_LCU_ON)
				.addAttachment(GunCus.LEFT_UNI_FLASH_OFF)
				.addAttachment(GunCus.LEFT_UNI_FLASH_ON)
				.addAttachment(GunCus.LEFT_UNI_BAYONET)


				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)
				.addAttachment(GunCus.RIGHT_UNI_BAYONET)

				.addAttachment(GunCus.STOCK_REM_BASE)
				.addAttachment(GunCus.STOCK_REM_SHORT);
	}

	public static ItemGun createUzi(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)

				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)

				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)

				.addAttachment(GunCus.LEFT_UNI_LCU_OFF)
				.addAttachment(GunCus.LEFT_UNI_LCU_ON)
				.addAttachment(GunCus.LEFT_UNI_FLASH_OFF)
				.addAttachment(GunCus.LEFT_UNI_FLASH_ON)


				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)

				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)

				.addAttachment(GunCus.MAGAZINE_UZI_BIG)
				.addAttachment(GunCus.MAGAZINE_UZI_MED);
	}

	public static ItemGun createSmg(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)

				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)

				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)
				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)

				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)
				.addAttachment(GunCus.STOCK_SMG_BASE)

				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)


				.addAttachment(GunCus.MAGAZINE_SMG_MED)
				.addAttachment(GunCus.MAGAZINE_SMG_SMALL)
				.addAttachment(GunCus.MAGAZINE_SMG_BIG);
	}

	public static ItemGun createHsmg(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)

				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)

				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)
				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)

				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)
				.addAttachment(GunCus.STOCK_HSMG_BASE)

				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)

				.addAttachment(GunCus.MAGAZINE_HSMG_BIG)
				.addAttachment(GunCus.MAGAZINE_HSMG_MED)
				.addAttachment(GunCus.MAGAZINE_HSMG_SMALL);

	}

    public static ItemGun createDoublebarrel(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
    {
        return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.LEFT_UNI_FLASH_OFF)
				.addAttachment(GunCus.LEFT_UNI_FLASH_ON)
				.addAttachment(GunCus.LEFT_UNI_LCU_OFF)
				.addAttachment(GunCus.LEFT_UNI_LCU_ON)

				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)
				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)

				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)

				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_DOUBLEBARREL_PLANKA)

				.addAttachment(GunCus.STVOL_DOUBLEBARREL_VERTIC_LONG)
				.addAttachment(GunCus.STVOL_DOUBLEBARREL_VERTIC_SHORT)
				.addAttachment(GunCus.STVOL_DOUBLEBARREL_HORIZ_LONG)
				.addAttachment(GunCus.STVOL_DOUBLEBARREL_HORIZ_SHORT)

				.addAttachment(GunCus.STOCK_DOUBLEBARREL_SHORT)
				.addAttachment(GunCus.STOCK_DOUBLEBARREL_FULL)
				.addAttachment(GunCus.STOCK_DOUBLEBARREL_PISTOL)
				.addAttachment(GunCus.STOCK_DOUBLEBARREL_PISTOL_TELESCOPIC)
				.addAttachment(GunCus.STOCK_DOUBLEBARREL_PISTOL_FOLDING_ON)
				.addAttachment(GunCus.STOCK_DOUBLEBARREL_PISTOL_FOLDING_OFF);


	}

	public static ItemGun createFal(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge) {
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.LEFT_UNI_FLASH_OFF)
				.addAttachment(GunCus.LEFT_UNI_FLASH_ON)
				.addAttachment(GunCus.LEFT_UNI_LCU_OFF)
				.addAttachment(GunCus.LEFT_UNI_LCU_ON)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)
				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.MAGAZINE_FAL_MED)
				.addAttachment(GunCus.STOCK_FAL_BASE)
				;
	}
	public static ItemGun createHmg(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge) {
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.MAGAZINE_HMG_MED)
				;

	}

	public static ItemGun createRpg18(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_CARBINE)
				;

	}


	public static ItemGun createTraumat(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_TRAUMAT_MED)
				;

	}

	public static ItemGun createSignal(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_CARBINE)
				;

	}

	public static ItemGun createHeavyrevolver(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				;

	}

	public static ItemGun createGl(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_GL_SIGHT_UP)
				.addAttachment(GunCus.OPTIC_GL_SIGHT_DOWN)
				;

	}
	public static ItemGun createLadygun(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_CARBINE)
				;

	}
	public static ItemGun createAutopistol(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.MAGAZINE_AUTOPISTOL_BIG)
				.addAttachment(GunCus.STOCK_UNI_PISTOL)
				.addAttachment(GunCus.MAGAZINE_AUTOPISTOL_MED)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				;

	}

	public static ItemGun createPistolhorn(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.STOCK_UNI_PISTOL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.MAGAZINE_HORN_MED)
				.addAttachment(GunCus.MAGAZINE_HORN_BIG)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)

				;

	}

	public static ItemGun createPistolalt(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.STOCK_UNI_PISTOL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.MAGAZINE_PISTOL_ALT_MED)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				;

	}

	public static ItemGun createTmp(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.MAGAZINE_TMP_MED)
				.addAttachment(GunCus.STOCK_TMP_BASE)
				;

	}
	public static ItemGun createLightrevolver(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.PAINT_TOY)
				.addAttachment(GunCus.BUFF_KOSTIL)
				;

	}
	public static ItemGun createRevolver(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.PAINT_GOLD)

				;

	}

	//нужен магазин
	public static ItemGun createPistolvas(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.STOCK_UNI_PISTOL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.MAGAZINE_PV_MED)
				.addAttachment(GunCus.MAGAZINE_PV_BIG)
				;

	}

	//нужен магазин
	public static ItemGun createMammoth(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.UNDERBARREL_MAMMOTH_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_MAMMOTH_LCU_OFF)
				.addAttachment(GunCus.LEFT_MAMMOTH_FLASH_ON)
				.addAttachment(GunCus.LEFT_MAMMOTH_FLASH_OFF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.PAINT_RAIDER)
				.addAttachment(GunCus.MAGAZINE_MAMMOTH_MED)
				.addAttachment(GunCus.MAGAZINE_MAMMOTH_BIG)
				;

	}

	public static ItemGun createSig(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.STOCK_UNI_PISTOL)
				.addAttachment(GunCus.MAGAZINE_SIG_MED)
				.addAttachment(GunCus.MAGAZINE_SIG_BIG)
				;

	}

	public static ItemGun createUmp(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.MAGAZINE_UMP_MED)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.STOCK_UMP_BASE)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_SMALL)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.PAINT_STICKER)
				.addAttachment(GunCus.PAINT_PURPLE)


				;

	}

	public static ItemGun createSvu(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.LEFT_UNI_FLASH_OFF)
				.addAttachment(GunCus.LEFT_UNI_FLASH_ON)
				.addAttachment(GunCus.LEFT_UNI_LCU_OFF)
				.addAttachment(GunCus.LEFT_UNI_LCU_ON)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)
				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_SMALL)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.MAGAZINE_SVU_BIG)
				.addAttachment(GunCus.MAGAZINE_SVU_SMALL)
				.addAttachment(GunCus.MAGAZINE_SVU_MED)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				;

	}

	public static ItemGun createVykhlop(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_VYKHLOP_SILENCER)
				.addAttachment(GunCus.BARREL_VYKHLOP_COMPENSATOR)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.MAGAZINE_VYKHLOP_SMALL)
				.addAttachment(GunCus.MAGAZINE_VYKHLOP_MED)
				.addAttachment(GunCus.MAGAZINE_VYKHLOP_BIG)
				;

	}

	public static ItemGun createMossberg(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.LEFT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.LEFT_UNI_FLASH_ON)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)
				.addAttachment(GunCus.LEFT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.LEFT_UNI_LCU_ON)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.RIGHT_UNI_BAYONET)
				.addAttachment(GunCus.LEFT_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.STOCK_PISTOL_UNI_TELESCOPIC)
				.addAttachment(GunCus.STOCK_PISTOL_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_PISTOL_UNI_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_PISTOL_UNI)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_SMALL)
				.addAttachment(GunCus.STOCK_MOSSBERG_BASE)
				.addAttachment(GunCus.STOCK_MOSSBERG_OBREZ)
				;

	}

	public static ItemGun createHeavyrifle(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)

				;

	}

	public static ItemGun createEmp(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				;

	}

	public static ItemGun createCrossbow(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)

				;

	}

	public static ItemGun createCompoundcrossbow(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_SMALL)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				;

	}

	public static ItemGun createRailgun(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.STOCK_RAILGUN_BASE)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)
				;

	}

	public static ItemGun createSwordmodular(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.SWORD_FIREMODULE)
				;

	}


	public static ItemGun createSigauto(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.STOCK_UNI_PISTOL)
				.addAttachment(GunCus.MAGAZINE_SIG_MED)
				.addAttachment(GunCus.MAGAZINE_SIG_BIG)
				.addAttachment(GunCus.UNDERBARREL_SIG_MAGAZINEGRIP)
				;

	}

	public static ItemGun createScorpio(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.STOCK_SCORPIO_UNDEPLETED)
				.addAttachment(GunCus.STOCK_SCORPIO_DEPLETED)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.MAGAZINE_SCORPIO_MED)
				.addAttachment(GunCus.MAGAZINE_SCORPIO_BIG)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				;

	}

	public static ItemGun createMc255(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.LEFT_UNI_LCU_ON)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)
				.addAttachment(GunCus.LEFT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.LEFT_UNI_FLASH_ON)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)
				.addAttachment(GunCus.LEFT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.BARREL_MC255_SILENCERLONGBARREL)
				.addAttachment(GunCus.BARREL_MC255_SILENCERSHORTBARREL)
				.addAttachment(GunCus.STOCK_MC255_PISTOL_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_MC255_PISTOL_FOLDING_ON)
				.addAttachment(GunCus.STOCK_MC255_PISTOL_TELESCOPIC)
				.addAttachment(GunCus.STOCK_MC255_SHORT)
				.addAttachment(GunCus.STOCK_MC255_STOCK)
				.addAttachment(GunCus.STOCK_MC255_PISTOL)
				.addAttachment(GunCus.BARREL_MC255_SHORT)
				.addAttachment(GunCus.BARREL_MC255_LONG)
				;

	}


	public static ItemGun createRig(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_RIG_TBG)
				.addAttachment(GunCus.BARREL_RIG_FRAG)
				;

	}

	public static ItemGun createM60(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_M60_BIG)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.LEFT_UNI_LCU_ON)
				.addAttachment(GunCus.LEFT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				;

	}

	public static ItemGun createHeavyshotgun(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.STOCK_SPAS_DEPLETED)
				.addAttachment(GunCus.STOCK_SPAS_UNDEPLETED)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.STOCK_FAL_BASE)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				;

	}

	public static ItemGun createFlamethrower(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_CARBINE)
				;

	}

	public static ItemGun createFlamethrowersanitar(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_CARBINE)
				;

	}

	public static ItemGun createSamopalpistol(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_PPAUTO_SMALL)
				.addAttachment(GunCus.MAGAZINE_PPAUTO_MED)
				.addAttachment(GunCus.MAGAZINE_PPAUTO_BIG)
				.addAttachment(GunCus.STOCK_PPAUTO_DEFAULT_METAL)
				.addAttachment(GunCus.STOCK_PPAUTO_DEFAULT_WOOD)
				;

	}

	public static ItemGun createSamopalrifle(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_VPAUTO_SMALL)
				.addAttachment(GunCus.MAGAZINE_VPAUTO_MED)
				.addAttachment(GunCus.MAGAZINE_VPAUTO_BIG)
				.addAttachment(GunCus.STOCK_PPAUTO_DEFAULT_METAL)
				.addAttachment(GunCus.STOCK_PPAUTO_DEFAULT_WOOD)
				;

	}

	public static ItemGun createTaser(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_TASER_CARTRIDGE)
				;

	}

	public static ItemGun createLightrifle(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge) {
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF);
	}

	public static ItemGun createLaserpistol(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge) {
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_LASERPISTOL_MED);
	}

	public static ItemGun createSwordkatana(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge) {
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.KATANA_SHEATH)
				.addAttachment(GunCus.PAINT_KATANA);
	}

	public static ItemGun createDakka(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.UNDERBARREL_CRAB_BIPOD_ALT)
				.addAttachment(GunCus.UNDERBARREL_HANDLE_CRAB_ALT)
				.addAttachment(GunCus.BARREL_CRAB_GARBAGE_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.MAGAZINE_CRAB_MED)
				.addAttachment(GunCus.MAGAZINE_CRAB_MED_ALT)
				.addAttachment(GunCus.MAGAZINE_CRAB_MAXIMUM_DAKKA)
				.addAttachment(GunCus.STOCK_CRAB_METAL)
				.addAttachment(GunCus.STOCK_CRAB_WOOD)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)
				.addAttachment(GunCus.LEFT_UNI_FLASH_OFF)
				.addAttachment(GunCus.LEFT_UNI_FLASH_ON)
				.addAttachment(GunCus.LEFT_UNI_LCU_OFF)
				.addAttachment(GunCus.LEFT_UNI_LCU_ON)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)
				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.STOCK_FAL_BASE)
				.addAttachment(GunCus.RIGHT_UNI_BAYONET)
				.addAttachment(GunCus.LEFT_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				;

	}

	public static ItemGun createNotluger(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_NOTLUGER_MED)
				.addAttachment(GunCus.MAGAZINE_NOTLUGER_BIG)
				.addAttachment(GunCus.BARREL_NOTLUGER_LONG)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.STOCK_PISTOL_UNI_TELESCOPIC)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.STOCK_PISTOL_UNI)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX_SMALL)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.BARREL_CRAB_GARBAGE_SILENCER)
				;

	}

	public static ItemGun createShauchat(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge) {
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_SHAUCHAT_BIG)
				.addAttachment(GunCus.MAGAZINE_SHAUCHAT_MED)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.LEFT_UNI_FLASH_OFF)
				.addAttachment(GunCus.LEFT_UNI_FLASH_ON)
				.addAttachment(GunCus.LEFT_UNI_LCU_OFF)
				.addAttachment(GunCus.LEFT_UNI_LCU_ON)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_OFF)
				.addAttachment(GunCus.RIGHT_UNI_FLASH_ON)
				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.STOCK_FAL_BASE)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.STOCK_CRAB_WOOD)
				;
	}


	public static ItemGun createGun(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_CARBINE)
				;

	}

	public static ItemGun createHyperion(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_COMPENSATOR)
				.addAttachment(GunCus.BARREL_UNI_SILENCER)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_MAGNIF)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_REFLEX)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP_RNG)
				.addAttachment(GunCus.RIGHT_UNI_LCU_OFF)
				.addAttachment(GunCus.RIGHT_UNI_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_SHOTGUN)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLAMETHROWER)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_FLASH_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP_LCU_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_LCU_ON)
				.addAttachment(GunCus.STOCK_FAL_BASE)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
/*				.addAttachment(GunCus.RIGHT_UNI_BAYONET)
				.addAttachment(GunCus.LEFT_UNI_BAYONET)*/
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_ASSAULT)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_HOLO_SMALL)
				.addAttachment(GunCus.STOCK_CRAB_BASE)
				.addAttachment(GunCus.MAGAZINE_HYP_SMALL)
				.addAttachment(GunCus.MAGAZINE_HYP_MED)
				.addAttachment(GunCus.MAGAZINE_HYP_BIG)
				;

	}

	public static ItemGun createWinchester(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.STOCK_WINCHESTER_BASE)
				.addAttachment(GunCus.STOCK_WINCHESTER_SAWOFF)
				.addAttachment(GunCus.BARREL_UNI_GRENADELAUNCHER)
				;

	}

	public static ItemGun createMossberg2(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.STOCK_MOSSBERG2_BASE)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.BARREL_UNI_GRENADELAUNCHER)
				;

	}

	public static ItemGun createBar(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_BAR_MED)
				.addAttachment(GunCus.MAGAZINE_BAR_BIG)
				.addAttachment(GunCus.MAGAZINE_BAR_SMALL)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.BARREL_UNI_GRENADELAUNCHER)
				;

	}

	public static ItemGun createPps(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.STOCK_PPS_UNFOLDED)
				.addAttachment(GunCus.STOCK_PPS_FOLDED)
				.addAttachment(GunCus.MAGAZINE_PPS_SMALL)
				.addAttachment(GunCus.MAGAZINE_PPS_MED)
				.addAttachment(GunCus.MAGAZINE_PPS_BIG)
				;

	}

	public static ItemGun createM21(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_M21_MED)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.MAGAZINE_M21_BIG)
				.addAttachment(GunCus.MAGAZINE_M21_SMALL)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.BARREL_UNI_GRENADELAUNCHER)
				;

	}

	public static ItemGun createReichsrevolver(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_CARBINE)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				;

	}

	public static ItemGun createKorovin(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_KOROVIN_MED)
				.addAttachment(GunCus.MAGAZINE_KOROVIN_BIG)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				;

	}

	public static ItemGun createAutomag(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_AUTOMAG_MED)
				.addAttachment(GunCus.MAGAZINE_AUTOMAG_BIG)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.BARREL_AUTOMAG_SHORT)
				;

	}

	public static ItemGun createPm(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_PM_MED)
				.addAttachment(GunCus.MAGAZINE_PM_BIG)
				;

	}

	public static ItemGun createAps(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_APS_MED)
				.addAttachment(GunCus.MAGAZINE_APS_BIG)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.STOCK_APS_BASE)
				.addAttachment(GunCus.PAINT_BLACK)
				;

	}

	public static ItemGun createScorpionew(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_SCORPIONEW_MED)
				.addAttachment(GunCus.MAGAZINE_SCORPIONEW_DRUM)
				.addAttachment(GunCus.MAGAZINE_SCORPIONEW_BIG)
				.addAttachment(GunCus.MAGAZINE_SCORPIONEW_SMALL)
				.addAttachment(GunCus.STOCK_SCORPIONEW_DEPLETED)
				.addAttachment(GunCus.STOCK_SCORPIONEW_UNDEPLETED)
				;

	}

	public static ItemGun createOwen(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_OWEN_MED)
				.addAttachment(GunCus.MAGAZINE_OWEN_BIG)
				.addAttachment(GunCus.MAGAZINE_OWEN_DRUM)
				.addAttachment(GunCus.MAGAZINE_OWEN_SMALL)
				.addAttachment(GunCus.STOCK_OWEN_BASE)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				;

	}

	public static ItemGun createTommy(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.UNDERBARREL_TOMMY_SHIT)
				.addAttachment(GunCus.MAGAZINE_TOMMY_MED)
				.addAttachment(GunCus.MAGAZINE_TOMMY_DRUM)
				.addAttachment(GunCus.STOCK_TOMMY_BASE)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.BARREL_TOMMY_LONG)
				.addAttachment(GunCus.BARREL_TOMMY_SHORT)
				;

	}

	public static ItemGun createType64(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_TYPE64_MED)
				.addAttachment(GunCus.MAGAZINE_TYPE64_BIG)
				.addAttachment(GunCus.UNDERBARREL_TYPE64_SUPRESSOR)
				;

	}

	public static ItemGun createMosin(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_MOSIN_MED)
				.addAttachment(GunCus.STOCK_MOSIN_BASE)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.BARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				;

	}

	public static ItemGun createSauer(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_CARBINE)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.BARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				;

	}

	public static ItemGun createM1a1(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.MAGAZINE_M1A1_MED)
				.addAttachment(GunCus.MAGAZINE_M1A1_SMALL)
				.addAttachment(GunCus.MAGAZINE_M1A1_MED_ALT)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.BARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.STOCK_M1A1_BASE)
				.addAttachment(GunCus.BARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				;

	}

	public static ItemGun createSvt(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.MAGAZINE_SVT_BIG)
				.addAttachment(GunCus.MAGAZINE_SVT_MED)
				.addAttachment(GunCus.MAGAZINE_SVT_SMALL)
				.addAttachment(GunCus.BARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				;

	}

	public static ItemGun createMas(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.MAGAZINE_MAS_MED)
				.addAttachment(GunCus.MAGAZINE_MAS_BIG)
				.addAttachment(GunCus.MAGAZINE_MAS_SMALL)
				.addAttachment(GunCus.BARREL_UNI_GRENADELAUNCHER)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				;

	}

	public static ItemGun createMg34(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.MAGAZINE_MG34_BIG)
				.addAttachment(GunCus.MAGAZINE_MG34_MED)
				;

	}

	public static ItemGun createAmerican(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.UNDERBARREL_AMERICAN_PATCH)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.MAGAZINE_AMERICAN_DRUM)
				.addAttachment(GunCus.STOCK_AMERICAN_BASE)
				;

	}
	public static ItemGun createGigashot(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_GIGASHOT_HORISONTALSAWEDOFFEVENMORE)
				.addAttachment(GunCus.BARREL_GIGASHOT_HORISONTALSAWEDOFF)
				.addAttachment(GunCus.BARREL_GIGASHOT_HORISONTALSEMISCREENED)
				.addAttachment(GunCus.BARREL_GIGASHOT_HORISONTALSAWEDOFF)
				.addAttachment(GunCus.BARREL_GIGASHOT_HORISONTALFULLSCREENED)
				.addAttachment(GunCus.BARREL_GIGASHOT_HORISONTALUNSCREENED)
				.addAttachment(GunCus.BARREL_GIGASHOT_MONO)
				.addAttachment(GunCus.BARREL_GIGASHOT_MULTI)
				.addAttachment(GunCus.BARREL_GIGASHOT_VERTICAL)
				.addAttachment(GunCus.BARREL_GIGASHOT_VERTICALSCREENED)
				.addAttachment(GunCus.STOCK_GIGASHOT_EXTRACURSED)
				.addAttachment(GunCus.STOCK_GIGASHOT_HANDLECURSED)
				.addAttachment(GunCus.STOCK_GIGASHOT_BASE)
				.addAttachment(GunCus.STOCK_GIGASHOT_CURSED)
				.addAttachment(GunCus.STOCK_GIGASHOT_HANDLENORMAL)
				;

	}
	public static ItemGun createManlicher(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				;

	}

	public static ItemGun createSjorgen(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.STOCK_SJORGEN_BASE)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.BARREL_UNI_GRENADELAUNCHER)
				;

	}

	public static ItemGun createMace(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.HANDLE_MACE_CLOTH)
				.addAttachment(GunCus.HANDLE_MACE_RUBBER)
				.addAttachment(GunCus.HANDLE_MACE_TAPE)
				.addAttachment(GunCus.HEAD_MACE_DISKS)
				.addAttachment(GunCus.HEAD_MACE_ROUND)
				.addAttachment(GunCus.HEAD_MACE_SQUAER)
				;

	}

	public static ItemGun createVag(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_VAG_MED)
				.addAttachment(GunCus.MAGAZINE_VAG_BIG)
				;

	}

	public static ItemGun createBorchadt(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.STOCK_BORCHADT_BASE)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				;

	}

	public static ItemGun createPitch(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_PITCH_BIG)
				.addAttachment(GunCus.MAGAZINE_PITCH_MED)
				.addAttachment(GunCus.MAGAZINE_PITCH_SMALL)
				.addAttachment(GunCus.STOCK_PITCH_BASE)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				;

	}

	public static ItemGun createBottlebrown(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.RAG_BOTTLE_RED)
				.addAttachment(GunCus.RAG_BOTTLE_BLACK)
				.addAttachment(GunCus.RAG_BOTTLE_WHITE)
				.addAttachment(GunCus.CAP_BOTTLE_BASE)
				;

	}

	public static ItemGun createBiggun(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_BIGGUN_BASE)
				.addAttachment(GunCus.STOCK_BIGGUN_BASE)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				.addAttachment(GunCus.BIGGUN_MORTAR)
				;

	}

	public static ItemGun createFg42(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_FG42_SMALL)
				.addAttachment(GunCus.MAGAZINE_FG42_MED)
				.addAttachment(GunCus.MAGAZINE_FG42_BIG)
				.addAttachment(GunCus.STOCK_FG42_BASE)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				;

	}

	public static ItemGun createRuby(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_RUBY_BIG)
				.addAttachment(GunCus.MAGAZINE_RUBY_MED)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				;

	}
	public static ItemGun createNagan(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				;

	}

	public static ItemGun createGrenade(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.NADE_BLACK)
				.addAttachment(GunCus.NADE_BLU)
				.addAttachment(GunCus.NADE_RED)
				.addAttachment(GunCus.NADE_GBLACK)
				.addAttachment(GunCus.NADE_GBLU)
				.addAttachment(GunCus.NADE_GRED)
				.addAttachment(GunCus.NADE_SHOMPOL_BASE)
				;

	}
	public static ItemGun createBat(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(METALRINGS)
				.addAttachment(STUNNER)
				.addAttachment(PAINT_BLACK)
				;

	}
	public static ItemGun createHook(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(BARREL_HOOK)
				;

	}
	public static ItemGun createBlunderbass(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createNrpgun(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(NRPMAGAZ)
				;

	}

	public static ItemGun createBita(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(BITA_HANDLE_CLOTH)
				.addAttachment(BITA_HANDLE_RUBBER)
				.addAttachment(BITA_HANDLE_RUBBERALT)
				.addAttachment(BITA_HANDLE_LEATHER)
				.addAttachment(BITA_NAILS)
				.addAttachment(BITA_WIRE)
				.addAttachment(BITA_NONLETHALNAILS)
				;

	}
	public static ItemGun createArzero(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(MAGAZINE_ARZERO_SMALL)
				.addAttachment(MAGAZINE_ARZERO_BIG)
				.addAttachment(MAGAZINE_ARZERO_MED)
				.addAttachment(STOCK_UNI_FOLDING_ON)
				.addAttachment(STOCK_UNI_FOLDING_OFF)
				;

	}
	public static ItemGun createRevolrifle(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(OPTIC_UNI_SCOPE_SNIP)
				;

	}
	public static ItemGun createShitrevolver(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(MAGAZINE_SHITREVOLVER_CHAIN)
				.addAttachment(MAGAZINE_SHITREVOLVER_MED)
				;

	}
	public static ItemGun createRevolvergarik(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}

	public static ItemGun createAnkasmol(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createBergmann(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createMauser(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createMusket(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.UNDERBARREL_UNI_BAYONET)
				;

	}

	public static ItemGun createRemington(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				;

	}

	public static ItemGun createThatgun(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				;

	}

	public static ItemGun createTrigun(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.PAINT_PINK)
				;

	}

	public static ItemGun createFlintstock(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.PAINT_WHITE)
				;

	}

	public static ItemGun createGunblade2(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				;

	}

	public static ItemGun createShovel(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.SHOVEL_HEAD)
				.addAttachment(GunCus.SHOVEL_HEAD_SERRATED)
				;

	}
	public static ItemGun createCane2(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.CANE_HADNLE_ROUND)
				.addAttachment(GunCus.CANE_HADNLE_EAGLE)
				.addAttachment(GunCus.CANE_HADNLE_CUBE)
				.addAttachment(GunCus.CANE_HADNLE_BALL)
				.addAttachment(GunCus.CANE_HADNLE_PROPER)
				.addAttachment(GunCus.PAINT_BLACK)
				.addAttachment(GunCus.PAINT_WHITE)
				;

	}
	public static ItemGun createOneshot(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				;

	}
	public static ItemGun createRevgangst(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.PAINT_GOLD)
				;

	}
	public static ItemGun createLonestar(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_LONESTAR_BIG)
				.addAttachment(GunCus.MAGAZINE_LONESTAR_MED)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.STOCK_LONESTAR_BASE)
				;

	}
	public static ItemGun createCg45(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_CG45_BIG)
				.addAttachment(GunCus.MAGAZINE_CG45_MED)
				.addAttachment(GunCus.MAGAZINE_CG45_SMALL)
				.addAttachment(GunCus.STOCK_UNI_TELESCOPIC)
				;

	}
	public static ItemGun createHellcat(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_HELLCAT_SMALL)
				.addAttachment(GunCus.MAGAZINE_HELLCAT_MED)
				.addAttachment(GunCus.STOCK_HELLCAT_WOOD)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				;

	}
	public static ItemGun createG3r1(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_G3R1_SMALL)
				.addAttachment(GunCus.MAGAZINE_G3R1_MED)
				.addAttachment(GunCus.MAGAZINE_G3R1_BIG)
				.addAttachment(GunCus.STOCK_G3R1_WOOD)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				;

	}
	public static ItemGun createRailshutze(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				;

	}
	public static ItemGun createExcelsior(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_GRIP)
				.addAttachment(GunCus.MAGAZINE_EXCELSIOR_DRUM)
				.addAttachment(GunCus.MAGAZINE_EXCELSIOR_MED)
				;

	}
	public static ItemGun createBn1900(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BARREL_UNI_SUPRESSOR)
				.addAttachment(GunCus.PAINT_GOLD)
				;

	}

	public static ItemGun createAksu(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_AKSU_BIG)
				.addAttachment(GunCus.MAGAZINE_AKSU_MED)
				.addAttachment(GunCus.MAGAZINE_AKSU_SMALL)
				.addAttachment(GunCus.STOCK_AKSU_WOOD)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				;

	}

	public static ItemGun createVag72(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_VAG72_SMALL)
				.addAttachment(GunCus.MAGAZINE_VAG72_BIG)
				.addAttachment(GunCus.MAGAZINE_VAG72_MED)
				.addAttachment(GunCus.MGBAR_VAG72_BIG)
				.addAttachment(GunCus.MGBAR_VAG72_MED)
				.addAttachment(GunCus.MGBAR_VAG72_SMALL)
				.addAttachment(GunCus.UNDERBARREL_POTATO_GRIP)
				.addAttachment(GunCus.UNDERBARREL_STUBBY_GRIP)
				.addAttachment(GunCus.STOCK_VAG72_AXE)
				.addAttachment(GunCus.STOCK_VAG72_BASE)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				;

	}
	public static ItemGun createMagnum(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createAks(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_AKS_MED)
				.addAttachment(GunCus.STOCK_AKS_BASE)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				;

	}
	public static ItemGun createDevastator(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.PAINT_BLACK)
				;

	}
	public static ItemGun createSks(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MAGAZINE_SKS_MED)
				.addAttachment(GunCus.MAGAZINE_SKS_BIG)
				;

	}
	public static ItemGun createInjrifle(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_ON)
				.addAttachment(GunCus.STOCK_UNI_FOLDING_OFF)
				;

	}
	public static ItemGun createMoper(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.MOP_MOP)
				.addAttachment(GunCus.BRUSH_MOP)
				.addAttachment(GunCus.MECHA_MOP)
				;

	}
	public static ItemGun createNebula(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createAtr(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.OPTIC_UNI_SCOPE_SNIP)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_ON)
				.addAttachment(GunCus.UNDERBARREL_UNI_BIPOD_OFF)
				.addAttachment(GunCus.MAGAZINE_ATR_MED)
				;

	}
	public static ItemGun createBp(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createMateba(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createSamopal2(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.WASP_MAG_SMALL)
				.addAttachment(GunCus.WASP_MAG_MEDIUM)
				.addAttachment(GunCus.WASP_MAG_LARGE)
				.addAttachment(GunCus.WASP_BARREL_SHORT)
				.addAttachment(GunCus.WASP_BARREL_LONG)
				.addAttachment(GunCus.WASP_STOCK)
				;

	}
	public static ItemGun createSamopal1(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.PISTOL_BARREL_S1)
				.addAttachment(GunCus.RIFLE_BARREL_S1)
				.addAttachment(GunCus.STOCK_S1)
				;

	}
	public static ItemGun createSamopal3(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.STAPLER_BARREL_LONG)
				.addAttachment(GunCus.STAPLER_BARREL_SHORT)
				.addAttachment(GunCus.STAPLER_STOCK)
				;

	}
	public static ItemGun createSamopal4(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BULL_MAG_SMALL)
				.addAttachment(GunCus.BULL_MAG_MED)
				.addAttachment(GunCus.BULL_STOCK)
				;

	}
	public static ItemGun createSamopal5(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.SPRING_GRIP_SIGHT)
				.addAttachment(GunCus.SPRING_SIGHT)
				.addAttachment(GunCus.SPRING_STOCK)
				;

	}
	public static ItemGun createSamopal6(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createSamopal7(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.NETTLE_MAG_SMALL)
				.addAttachment(GunCus.NETTLE_MAG_MEDIUM)
				.addAttachment(GunCus.NETTLE_STOCK)
				;

	}
	public static ItemGun createSamopal8(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createSamopal9(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.PERFORATOR_FOREARM_GRIP)
				.addAttachment(GunCus.PERFORATOR_MAG_LARGE)
				.addAttachment(GunCus.PERFORATOR_STOCK)
				;

	}
	public static ItemGun createSamopal10(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.SHEEV_BARREL_LONG)
				.addAttachment(GunCus.SHEEV_BARREL_SHORT)
				.addAttachment(GunCus.SHEEV_STOCK)
				;

	}
	public static ItemGun createChemgun(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.CHEM_SPRAYER_CAN)
				;

	}
	public static ItemGun createHlambolet(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createBw(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createRapier1(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)

				;

	}
	public static ItemGun createTitusgunberd(String rl, int fireRate, int maxAmmo, float damage, ItemCartridge cartridge)
	{
		return new ItemGun(rl, fireRate, maxAmmo, damage, cartridge)
				.addAttachment(GunCus.BUFF_KOSTIL)
				;

	}
	public static final Map<Item, ModelBiped> ARMOR_MODELS = new HashMap<>();

	@EventBusSubscriber
	public static class Handler
	{
		@SubscribeEvent
		public static void registerItems(RegistryEvent.Register<Item> event)
		{
			IForgeRegistry<Item> registry = event.getRegistry();

			// BLOCKS
			for(BlockGCI block : BlockGCI.BLOCKS_LIST)
			{
				GunCus.proxy.registerItem(registry, new ItemBlockGCI(block));
//				System.out.println("Registering item/block " + block.getRegistryName());
			}

			// ATTACHMENTS
			int j;
			Attachment attachment;

			for(int i = 0; i < Attachment.ATTACHMENTS_LIST.length; ++i)
			{
				for(j = 1; j < Attachment.getAmmountForSlot(i); ++j)
				{
					attachment = Attachment.getAttachment(i, j);

					if(attachment != null && attachment.shouldRegister())
					{
						GunCus.proxy.registerItem(registry, attachment);
					}
				}
			}

			// CARTRIDGES
			for(ItemCartridge cartridge : ItemCartridge.CARTRIDGES_LIST)
			{
				GunCus.proxy.registerItem(registry, cartridge);
			}

			// GUNS
			for(ItemGun gun : ItemGun.GUNS_LIST)
			{
				GunCus.proxy.registerGun(registry, gun);
			}

			// SIMPLE ITEMS
			final ItemGCI[] items = {
					FIRST_ITEM,
//					new ItemGCI("alembic", KMRP_TAB),
					new ItemGCI("aquila01", KMRP_TAB),
					new ItemGCI("aquila02", KMRP_TAB),
					new ItemGCI("aquila03", KMRP_TAB),

//					new ItemGCI("bag01", KMRP_TAB),
//					new ItemGCI("bag02", KMRP_TAB),
//					new ItemGCI("bag03", KMRP_TAB),
//					new ItemGCI("bag04", KMRP_TAB),
					new ItemGCI("balance_scales", KMRP_TAB),
					new ItemGCI("ballistic_glasses", KMRP_TAB),
//					new ItemGCI("barrel", KMRP_TAB),
//					new ItemGCI("basket", KMRP_TAB),
//					new ItemGCI("beaker", KMRP_TAB),
					new ItemGCI("bint", KMRP_TAB),
//					new ItemGCI("blister", KMRP_TAB),
					new ItemGCI("boar_hide", KMRP_TAB),
					new ItemGCI("bomb", KMRP_TAB),
//					new ItemGCI("bottle", KMRP_TAB),
					new ItemGCI("bow_1", KMRP_TAB),
					new ItemGCI("bow_2", KMRP_TAB),
					new ItemGCI("bow_3", KMRP_TAB),
					new ItemGCI("brain", KMRP_TAB),
					new ItemGCI("buckler_evtuh", KMRP_TAB),
					new ItemGCI("bullet", KMRP_TAB),
					new ItemGCI("butcher_axe", KMRP_TAB),
					new ItemGCI("brooche", KMRP_TAB),

					new ItemGCI("camera", KMRP_TAB),
					new ItemGCI("cane", KMRP_TAB),
					new ItemGCI("capsule", KMRP_TAB),
					new ItemGCI("can_empty", KMRP_TAB),
					new ItemGCI("converter", KMRP_TAB),
					new ItemGCI("cpu", KMRP_TAB),
//					new ItemGCI("carcass1", KMRP_TAB),
//					new ItemGCI("carcass2", KMRP_TAB),
//					new ItemGCI("carcass3", KMRP_TAB),
//					new ItemGCI("carcass4", KMRP_TAB),
//					new ItemGCI("carcass5", KMRP_TAB),
//					new ItemGCI("carcass6", KMRP_TAB),
//					new ItemGCI("casket", KMRP_TAB),
//					new ItemGCI("casket2", KMRP_TAB),
					new ItemGCI("charge", KMRP_TAB),
					new ItemGCI("chopesh", KMRP_TAB),
//					new ItemGCI("cigarette_pack", KMRP_TAB),
					new ItemGCI("cigarette", KMRP_TAB),
					new ItemGCI("cigar", KMRP_TAB),
					new ItemGCI("club", KMRP_TAB),
					new ItemGCI("comb", KMRP_TAB),
//					new ItemGCI("cookie", KMRP_TAB),
					new ItemGCI("cooler", KMRP_TAB),
					new ItemGCI("copper_coin", KMRP_TAB),
					new ItemGCI("cork", KMRP_TAB),
					new ItemGCI("cow_hide", KMRP_TAB),
					new ItemGCI("crossbow", KMRP_TAB),
//					new ItemGCI("cup", KMRP_TAB),

					new ItemGCI("deer_hide", KMRP_TAB),
					new ItemGCI("daniels_glasses", KMRP_TAB),
					new ItemGCI("display", KMRP_TAB),
					new ItemGCI("drum", KMRP_TAB),
					new ItemGCI("dynamite", KMRP_TAB),

					new ItemGCI("elven_dagger", KMRP_TAB),
					new ItemGCI("elven_saber_1", KMRP_TAB),
					new ItemGCI("elven_saber_2", KMRP_TAB),
//					new ItemGCI("envelope", KMRP_TAB),

					new ItemGCI("falx", KMRP_TAB),
					new ItemGCI("flail", KMRP_TAB),
//					new ItemGCI("flask", KMRP_TAB),
					new ItemGCI("flute", KMRP_TAB),
//					new ItemGCI("flut", KMRP_TAB),

//					new ItemGCI("gas_filter", KMRP_TAB),
					new ItemGCI("gaiki", KMRP_TAB),
					new ItemGCI("goggles", KMRP_TAB),
					new ItemGCI("graphics_card", KMRP_TAB),
					new ItemGCI("golden_coin", KMRP_TAB),

					new ItemGCI("halberd", KMRP_TAB),
					new ItemGCI("halo", KMRP_TAB),
					new ItemGCI("hand", KMRP_TAB),
					new ItemGCI("holy_symbol_a", KMRP_TAB),
					new ItemGCI("holy_symbol_b", KMRP_TAB),
					new ItemGCI("holy_symbol_c", KMRP_TAB),
					new ItemGCI("hook", KMRP_TAB),
					new ItemGCI("hourglass", KMRP_TAB),

//					new ItemGCI("inkwell", KMRP_TAB),
//					new ItemGCI("iron_pint", KMRP_TAB),
					new ItemGCI("isolenta", KMRP_TAB),

					new ItemGCI("joint", KMRP_TAB),

//					new ItemGCI("keychain", KMRP_TAB),
					new ItemGCI("kitchen_knife", KMRP_TAB),
					new ItemGCI("knobstick", KMRP_TAB),

					new ItemGCI("lance", KMRP_TAB),
					new ItemGCI("leather_strips", KMRP_TAB),
					new ItemGCI("leg", KMRP_TAB),
					new ItemGCI("led", KMRP_TAB),
					new ItemGCI("lock", KMRP_TAB),

					new ItemGCI("magnifier", KMRP_TAB),
					new ItemGCI("mic", KMRP_TAB),
					new ItemGCI("motherboard", KMRP_TAB),
//					new ItemGCI("matchbox", KMRP_TAB),
					new ItemGCI("match", KMRP_TAB),
					new ItemGCI("meat_bow", KMRP_TAB),
//					new ItemGCI("medkit", KMRP_TAB),
					new ItemGCI("messer_evtuh", KMRP_TAB),
					new ItemGCI("mop", KMRP_TAB),
//					new ItemGCI("mortar_and_pestle", KMRP_TAB),
					new ItemGCI("m_star", KMRP_TAB),
					new ItemGCI("mortise_lock", KMRP_TAB),

					new ItemGCI("nail", KMRP_TAB),
					new ItemGCI("needle", KMRP_TAB),

//					new ItemGCI("paper_pile", KMRP_TAB),
					new ItemGCI("pauldron", KMRP_TAB),
					new ItemGCI("pencil", KMRP_TAB),
					new ItemGCI("photo", KMRP_TAB),
					new ItemGCI("pig_skin", KMRP_TAB),
					new ItemGCI("pitchfork", KMRP_TAB),
					new ItemGCI("protivogaz", KMRP_TAB),
					new ItemGCI("pen", KMRP_TAB),
//					new ItemGCI("plate", KMRP_TAB),
//					new ItemGCI("pot", KMRP_TAB),
//					new ItemGCI("pouch", KMRP_TAB),

                    new ItemGCI("radio", KMRP_TAB),
					new ItemGCI("ram", KMRP_TAB),
					new ItemGCI("ramrod", KMRP_TAB),
//					new ItemGCI("retort", KMRP_TAB),
					new ItemGCI("roman_helm", KMRP_TAB),
					new ItemGCI("roman_shield_1", KMRP_TAB),
					new ItemGCI("roman_shield_2", KMRP_TAB),
					new ItemGCI("roman_spear", KMRP_TAB),
					new ItemGCI("roman_sword_1", KMRP_TAB),
					new ItemGCI("roman_sword_2", KMRP_TAB),
					new ItemGCI("roman_sword_3", KMRP_TAB),
					new ItemGCI("rose", KMRP_TAB),
					new ItemGCI("rusty_fork", KMRP_TAB),

					new ItemGCI("saber", KMRP_TAB),
//					new ItemGCI("sack", KMRP_TAB),
					new ItemGCI("scalpel", KMRP_TAB),
					new ItemGCI("scimitar", KMRP_TAB),
					new ItemGCI("screwdriver", KMRP_TAB),
					new ItemGCI("scythe", KMRP_TAB),
//					new ItemGCI("sheath", KMRP_TAB),
					new ItemGCI("sheep_hide", KMRP_TAB),
//					new ItemGCI("shot_glass", KMRP_TAB),
					new ItemGCI("shpaga", KMRP_TAB),
					new ItemGCI("sickle", KMRP_TAB),
					new ItemGCI("silver_coin", KMRP_TAB),
					new ItemGCI("simple_belt", KMRP_TAB),
					new ItemGCI("sling", KMRP_TAB),
					new ItemGCI("solar", KMRP_TAB),
					new ItemGCI("speaker", KMRP_TAB),
					new ItemGCI("splint", KMRP_TAB),
					new ItemGCI("spoon", KMRP_TAB),
					new ItemGCI("stiletto", KMRP_TAB),
					new ItemGCI("stiletto2", KMRP_TAB),
					new ItemGCI("sun_glasses", KMRP_TAB),
//					new ItemGCI("suitcase", KMRP_TAB),
					new ItemGCI("swine", KMRP_TAB),
					new ItemGCI("sword", KMRP_TAB),
//					new ItemGCI("syringe", KMRP_TAB),

					new ItemGCI("table_knife", KMRP_TAB),
					new ItemGCI("tablet_a", KMRP_TAB),
					new ItemGCI("tablet_b", KMRP_TAB),
					new ItemGCI("tablet_c", KMRP_TAB),
					new ItemGCI("tablet_d", KMRP_TAB),
					new ItemGCI("tannin", KMRP_TAB),
//					new ItemGCI("teapot01", KMRP_TAB),
//					new ItemGCI("teapot02", KMRP_TAB),
//					new ItemGCI("teapot03", KMRP_TAB),
//					new ItemGCI("teapot04", KMRP_TAB),
					new ItemGCI("thr_axe", KMRP_TAB),
//					new ItemGCI("tin", KMRP_TAB),
					new ItemGCI("tobacco_pipe", KMRP_TAB),
//					new ItemGCI("toolbox", KMRP_TAB),
					new ItemGCI("training_sword", KMRP_TAB),
					new ItemGCI("tricorne_color", KMRP_TAB),
					new ItemGCI("twohanded_sword", KMRP_TAB),

					new ItemGCI("uruk_hai_sword_1", KMRP_TAB),
					new ItemGCI("uruk_hai_sword_2", KMRP_TAB),
//					new ItemGCI("vestalka_mace", KMRP_TAB),
					new ItemGCI("vest", KMRP_TAB),

//					new ItemGCI("water_skin", KMRP_TAB),
					new ItemGCI("weight", KMRP_TAB),
					new ItemGCI("wide_belt", KMRP_TAB),
//					new ItemGCI("wooden_bucket", KMRP_TAB),
					new ItemGCI("wooden_coin", KMRP_TAB),
//					new ItemGCI("wooden_cup", KMRP_TAB),
					new ItemGCI("wooden_fork2", KMRP_TAB),
					new ItemGCI("wooden_fork", KMRP_TAB),
					new ItemGCI("wooden_knife", KMRP_TAB),
					new ItemGCI("wooden_ladle", KMRP_TAB),
//					new ItemGCI("wooden_plate", KMRP_TAB),
//					new ItemGCI("wooden_plate2", KMRP_TAB),
//					new ItemGCI("wooden_shot_glass", KMRP_TAB),
					new ItemGCI("wooden_spatula", KMRP_TAB),
					new ItemGCI("wooden_spoon", KMRP_TAB),
					new ItemGCI("wooden_spoon2", KMRP_TAB),
					new ItemGCI("walkie", KMRP_TAB),

//					new ItemGCI("zvezda", KMRP_TAB),

					new ItemGCI("arrowhead_dumb", KMRP_TAB),
					new ItemGCI("arrowhead_exp", KMRP_TAB),
					new ItemGCI("arrowhead_piercing", KMRP_TAB),
					new ItemGCI("arrowhead_zubr", KMRP_TAB),
					new ItemGCI("sdcard", KMRP_TAB),
					new ItemGCI("kipyatok", KMRP_TAB),
					new ItemGCI("nagrev", KMRP_TAB),
					new ItemGCI("propeller", KMRP_TAB),
					new ItemGCI("mek", KMRP_TAB),

					new ItemGCI("anymanual", KMRP_TAB),
					new ItemGCI("blankbook", KMRP_TAB),
					new ItemGCI("bomberjacket", KMRP_TAB),
					new ItemGCI("book", KMRP_TAB),
					new ItemGCI("electroniclockpick", KMRP_TAB),
					new ItemGCI("engimanual", KMRP_TAB),
					new ItemGCI("fingerlessskingloves", KMRP_TAB),
					new ItemGCI("fingerlesssynthgloves", KMRP_TAB),
					new ItemGCI("gaijatsutacticalg", KMRP_TAB),
					new ItemGCI("medimanual", KMRP_TAB),
					new ItemGCI("rifle_loader", KMRP_TAB),
					new ItemGCI("skingloves", KMRP_TAB),
					new ItemGCI("sniper_loader", KMRP_TAB),
					new ItemGCI("speedloader", KMRP_TAB),
					new ItemGCI("survivalistknife", KMRP_TAB),
					new ItemGCI("synthgloves", KMRP_TAB),
					new ItemGCI("techmanual", KMRP_TAB),
					new ItemGCI("watch", KMRP_TAB),

					new ItemGCI("ai-chip", KMRP_TAB),
					new ItemGCI("bioniclenormalcamera", KMRP_TAB),
					new ItemGCI("blank-chip", KMRP_TAB),
					new ItemGCI("blankgastank", KMRP_TAB),
					new ItemGCI("blankproccessor", KMRP_TAB),
					new ItemGCI("blanksupergastank", KMRP_TAB),
					new ItemGCI("brokencamera", KMRP_TAB),
					new ItemGCI("brokenmesharmor", KMRP_TAB),
					new ItemGCI("combustionbarrel", KMRP_TAB),
					new ItemGCI("cybernetic-shell", KMRP_TAB),
					new ItemGCI("dampner", KMRP_TAB),
					new ItemGCI("dust-cell", KMRP_TAB),
					new ItemGCI("dustshit", KMRP_TAB),
					new ItemGCI("electroniccomp2", KMRP_TAB),
					new ItemGCI("electroniccomp3", KMRP_TAB),
					new ItemGCI("electronicscomp1", KMRP_TAB),
					new ItemGCI("flametankfuel", KMRP_TAB),
					new ItemGCI("fuelinjector", KMRP_TAB),
					new ItemGCI("fuse", KMRP_TAB),
					new ItemGCI("mechanicalcomp1", KMRP_TAB),
					new ItemGCI("mechanicalcomp2", KMRP_TAB),
					new ItemGCI("mechanicalcomp3", KMRP_TAB),
					new ItemGCI("mesharmor", KMRP_TAB),
					new ItemGCI("metalspringl", KMRP_TAB),
					new ItemGCI("napalmtank", KMRP_TAB),
					new ItemGCI("prothesicarm", KMRP_TAB),
					new ItemGCI("prothesicleg", KMRP_TAB),
					new ItemGCI("sandpaper", KMRP_TAB),
					new ItemGCI("tankextender", KMRP_TAB),
					new ItemGCI("weight-cylinder", KMRP_TAB),
					new ItemGCI("babysflail", KMRP_TAB),

//					new ItemGCI("candle_card_2", KMRP_TAB),
//					new ItemGCI("candle_card_3", KMRP_TAB),
//					new ItemGCI("candle_card_4", KMRP_TAB),
//					new ItemGCI("candle_card_5", KMRP_TAB),
//					new ItemGCI("candle_card_6", KMRP_TAB),
//					new ItemGCI("candle_card_7", KMRP_TAB),
//					new ItemGCI("candle_card_8", KMRP_TAB),
//					new ItemGCI("candle_card_9", KMRP_TAB),
//					new ItemGCI("candle_card_10", KMRP_TAB),
//					new ItemGCI("candle_card_a", KMRP_TAB),
//					new ItemGCI("candle_card_p", KMRP_TAB),
//					new ItemGCI("candle_card_r", KMRP_TAB),
//					new ItemGCI("candle_card_s", KMRP_TAB),
//
//					new ItemGCI("carbon_card_2", KMRP_TAB),
//					new ItemGCI("carbon_card_3", KMRP_TAB),
//					new ItemGCI("carbon_card_4", KMRP_TAB),
//					new ItemGCI("carbon_card_5", KMRP_TAB),
//					new ItemGCI("carbon_card_6", KMRP_TAB),
//					new ItemGCI("carbon_card_7", KMRP_TAB),
//					new ItemGCI("carbon_card_8", KMRP_TAB),
//					new ItemGCI("carbon_card_9", KMRP_TAB),
//					new ItemGCI("carbon_card_10", KMRP_TAB),
//					new ItemGCI("carbon_card_a", KMRP_TAB),
//					new ItemGCI("carbon_card_p", KMRP_TAB),
//					new ItemGCI("carbon_card_r", KMRP_TAB),
//					new ItemGCI("carbon_card_s", KMRP_TAB),
//
//					new ItemGCI("inomir_card_2", KMRP_TAB),
//					new ItemGCI("inomir_card_3", KMRP_TAB),
//					new ItemGCI("inomir_card_4", KMRP_TAB),
//					new ItemGCI("inomir_card_5", KMRP_TAB),
//					new ItemGCI("inomir_card_6", KMRP_TAB),
//					new ItemGCI("inomir_card_7", KMRP_TAB),
//					new ItemGCI("inomir_card_8", KMRP_TAB),
//					new ItemGCI("inomir_card_9", KMRP_TAB),
//					new ItemGCI("inomir_card_10", KMRP_TAB),
//					new ItemGCI("inomir_card_a", KMRP_TAB),
//					new ItemGCI("inomir_card_p", KMRP_TAB),
//					new ItemGCI("inomir_card_r", KMRP_TAB),
//					new ItemGCI("inomir_card_s", KMRP_TAB),
//
//					new ItemGCI("monster_card_2", KMRP_TAB),
//					new ItemGCI("monster_card_3", KMRP_TAB),
//					new ItemGCI("monster_card_4", KMRP_TAB),
//					new ItemGCI("monster_card_5", KMRP_TAB),
//					new ItemGCI("monster_card_6", KMRP_TAB),
//					new ItemGCI("monster_card_7", KMRP_TAB),
//					new ItemGCI("monster_card_8", KMRP_TAB),
//					new ItemGCI("monster_card_9", KMRP_TAB),
//					new ItemGCI("monster_card_10", KMRP_TAB),
//					new ItemGCI("monster_card_a", KMRP_TAB),
//					new ItemGCI("monster_card_p", KMRP_TAB),
//					new ItemGCI("monster_card_r", KMRP_TAB),
//					new ItemGCI("monster_card_s", KMRP_TAB),
//
//					new ItemGCI("card_back", KMRP_TAB),
//
//					new ItemGCI("pockerchip_black_poor", KMRP_TAB),
//					new ItemGCI("pockerchip_black_rich", KMRP_TAB),
//					new ItemGCI("pockerchip_green_poor", KMRP_TAB),
//					new ItemGCI("pockerchip_green_rich", KMRP_TAB),
//					new ItemGCI("pockerchip_red_poor", KMRP_TAB),
//					new ItemGCI("pockerchip_red_rich", KMRP_TAB),
//					new ItemGCI("pockerchip_violet_poor", KMRP_TAB),
//					new ItemGCI("pockerchip_violet_rich", KMRP_TAB),
//
//					new ItemGCI("carbon_pistol_bullet", KMRP_TAB),
//					new ItemGCI("carbon_rifle_bullet", KMRP_TAB),
//					new ItemGCI("carbon_slug", KMRP_TAB),
//					new ItemGCI("electro_capsule", KMRP_TAB),
//					new ItemGCI("substance_d_powercell", KMRP_TAB),
//					new ItemGCI("substance_powercell", KMRP_TAB),



					new ItemGCI("podsword", KMRP_TAB),

					new ItemGCI("bioear", KMRP_TAB),
					new ItemGCI("biofoot", KMRP_TAB),
					new ItemGCI("biohand", KMRP_TAB),
					new ItemGCI("biopart2", KMRP_TAB),
					new ItemGCI("biopart", KMRP_TAB),
					new ItemGCI("mehear", KMRP_TAB),
					new ItemGCI("mehfoot", KMRP_TAB),
					new ItemGCI("mehforearm", KMRP_TAB),
					new ItemGCI("mehhand", KMRP_TAB),
					new ItemGCI("mehhip", KMRP_TAB),
					new ItemGCI("mehshin", KMRP_TAB),
					new ItemGCI("mehshoulder", KMRP_TAB),
					new ItemGCI("amogus", KMRP_TAB),
					new ItemGCI("tourniqet", KMRP_TAB),
					new ItemGCI("ushm", KMRP_TAB),
					new ItemGCI("zazhim", KMRP_TAB),
					new ItemGCI("shipci", KMRP_TAB),
					new ItemGCI("mazat", KMRP_TAB),
					new ItemGCI("injectorr", KMRP_TAB),
					new ItemGCI("nakopitel_true", KMRP_TAB),
					new ItemGCI("truba", KMRP_TAB),
					new ItemGCI("robotech", KMRP_TAB),
					new ItemGCI("plotnik", KMRP_TAB),
					new ItemGCI("orujeinik", KMRP_TAB),
					new ItemGCI("piletech", KMRP_TAB),
					new ItemGCI("bionic", KMRP_TAB),
					new ItemGCI("mechanic", KMRP_TAB),
					new ItemGCI("ceemees", KMRP_TAB),
					new ItemGCI("ballistic_plate_light", KMRP_TAB),
					new ItemGCI("ballistic_plate_heavy", KMRP_TAB),
					new ItemGCI("ballistic2_armor_kit", KMRP_TAB),
					new ItemGCI("ballistic_armor_kit", KMRP_TAB),
					new ItemGCI("leather_armor_kit", KMRP_TAB),
					new ItemGCI("iron_armor_kit", KMRP_TAB),
					new ItemGCI("range_remka", KMRP_TAB),
					new ItemGCI("rubanok", KMRP_TAB),
					new ItemGCI("blacksmith", KMRP_TAB),
					new ItemGCI("radiobomb", KMRP_TAB),
					new ItemGCI("stapler", KMRP_TAB),
					new ItemGCI("deconstruct", KMRP_TAB),
					new ItemGCI("chemic", KMRP_TAB),
					new ItemGCI("zirkularka", KMRP_TAB),

					(ItemGCI) new ItemGCI3D("cue", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("grenade_gas", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("grenade_fugas", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("grenade_inc", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("trench_knife", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("rack", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("injector", KMRP_TAB).setMaxStackSize(1),
					// без позинга
					(ItemGCI) new ItemGCI3D("brick_broken", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("brick_part", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("brick_full", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("turbobrick", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("claws", KMRP_TAB).setMaxStackSize(1),
					// конец
					(ItemGCI) new ItemGCI3D("machete", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("ppc", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("detector", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("wrench", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("knuckle", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("knuckle2", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("metaldetector", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("metaldetector2", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("fireaxe", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("fireaxe2", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("battleknife", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("tanto", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("tanto2", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("hard_disc", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("pasatiji", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("psupply", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("tester", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("hexatool", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("pliers", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("solderiron", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("shocker", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("smoke_pipe", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("walking_stick", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("cards", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("jammer", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("basutosodo", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("ghiriechainsaw", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("mortar", KMRP_TAB).setMaxStackSize(1),
//					(ItemGCI) new ItemGCI3D("ghiriechainsaw", KMRP_TAB).setMaxStackSize(1),
//					(ItemGCI) new ItemGCI3D("mortar", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("miliciasword", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("cleaver", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("radiomic", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("hatchet", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("prybar", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("sawblade", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("sabre", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("domingasword", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("clevec", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("hook_pudge", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("paddle", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("pipebomb", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_axe_1h", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_axe_2h", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_axe_svaston", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_knife_filet", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_knife_kunai", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_lucerne_1h", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_knife_glad", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_cutlass", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_cleaver_dag", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_cleaver_1h", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_maul_1h", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_maul_2h", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_polearm_glaive", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_polearm_lucerne", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_polearm_spear", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_rapier_1h", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_rapier_dag", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_rapier_2h", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_saber_1h", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_saber_2h", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_hatchet_1h", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("vs_gunsword", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("dynamite_1", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("dynamite_4", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("dynamite_16", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("dynamite_melee", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("handed_drill", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("augment_drill", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("dzutte", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("polearm_spear", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("polearm_spear_white", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("shovel_1", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("kama", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("case_v1", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("case_v2", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("case_v3", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("case_v4", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("case_v5", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("case_v6", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("case_v7", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("case_v8", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("case_v9", KMRP_TAB).setMaxStackSize(1),
					(ItemGCI) new ItemGCI3D("titusgunberd", KMRP_TAB).setMaxStackSize(1),
			};
			static_items  = items;
			for (ItemGCI item: items) {
				GunCus.proxy.registerItemSimple(registry, item);
			}

			// DYEABLE ITEMS
			final ItemDyeable[] itemsdyeable = {

//					new ItemDyeable("bag_dyeable", KMRP_TAB),
//					new ItemDyeable("bottle_wine_dyeable", KMRP_TAB),
					new ItemDyeable("buckle_belt_dyeable", KMRP_TAB),

					new ItemDyeable("card_dyeable", KMRP_TAB),
					new ItemDyeable("card2_dyeable", KMRP_TAB),
					new ItemDyeable("card3_dyeable", KMRP_TAB),
					new ItemDyeable("card4_dyeable", KMRP_TAB),
					new ItemDyeable("card5_dyeable", KMRP_TAB),
					new ItemDyeable("card6_dyeable", KMRP_TAB),
					new ItemDyeable("card7_dyeable", KMRP_TAB),
					new ItemDyeable("card8_dyeable", KMRP_TAB),
					new ItemDyeable("card9_dyeable", KMRP_TAB),
					new ItemDyeable("card10_dyeable", KMRP_TAB),
					new ItemDyeable("cardj_dyeable", KMRP_TAB),
					new ItemDyeable("cardd_dyeable", KMRP_TAB),
					new ItemDyeable("cardk_dyeable", KMRP_TAB),
					new ItemDyeable("carda_dyeable", KMRP_TAB),
//					new ItemDyeable("coloda_dyeable", KMRP_TAB),
					new ItemDyeable("cloak_dyeable", KMRP_TAB),
					new ItemDyeable("cloth_belt_dyeable", KMRP_TAB),
					new ItemDyeable("cloth_dyeable", KMRP_TAB),
					new ItemDyeable("cog_dyeable", KMRP_TAB),
					new ItemDyeable("colorless_skin_dyeable", KMRP_TAB),
					new ItemDyeable("crystal_dyeable", KMRP_TAB),

					new ItemDyeable("diadem_dyeable", KMRP_TAB),
					new ItemDyeable("dice_dyeable", KMRP_TAB),
					new ItemDyeable("dress_dyeable", KMRP_TAB),

					new ItemDyeable("flamaster_dyeable", KMRP_TAB),

					new ItemDyeable("gem_baguette_dyeable", KMRP_TAB),
					new ItemDyeable("gem_french_dyeable", KMRP_TAB),
					new ItemDyeable("gem_raw_dyeable", KMRP_TAB),
					new ItemDyeable("gem_square_dyeable", KMRP_TAB),
					new ItemDyeable("grape_dyeable", KMRP_TAB),
					new ItemDyeable("gloves_dyeable", KMRP_TAB),
//					new ItemDyeable("goblet_dyeable", KMRP_TAB),

					new ItemDyeable("hood_dyeable", KMRP_TAB),
					new ItemDyeable("herb_1_dyeable", KMRP_TAB),

					new ItemDyeable("ingot_dyeable", KMRP_TAB),

					new ItemDyeable("muselet_dyeable", KMRP_TAB),
					new ItemDyeable("metal_sheet_dyeable", KMRP_TAB),
					new ItemDyeable("mushroom_1_dyeable", KMRP_TAB),
					new ItemDyeable("mushroom_2_dyeable", KMRP_TAB),
					new ItemDyeable("mushroom_3_dyeable", KMRP_TAB),
					new ItemDyeable("mushroom_4_dyeable", KMRP_TAB),
					new ItemDyeable("mushroom_5_dyeable", KMRP_TAB),
					new ItemDyeable("mushroom_6_dyeable", KMRP_TAB),
					new ItemDyeable("mushroom_7_dyeable", KMRP_TAB),

					new ItemDyeable("journal_dyeable", KMRP_TAB),

					new ItemDyeable("lens_dyeable", KMRP_TAB),

//					new ItemDyeable("pack_dyeable", KMRP_TAB),
					new ItemDyeable("pants_dyeable", KMRP_TAB),
//					new ItemDyeable("phial_1_dyeable", KMRP_TAB),
//					new ItemDyeable("phial_2_dyeable", KMRP_TAB),
					new ItemDyeable("poolball_dyeable", KMRP_TAB),
					new ItemDyeable("plastic_card_dyeable", KMRP_TAB),

					new ItemDyeable("shirt_dyeable", KMRP_TAB),
					new ItemDyeable("shossi_dyeable", KMRP_TAB),
					new ItemDyeable("spool_dyeable", KMRP_TAB),
					new ItemDyeable("spmodul_dyeable", KMRP_TAB),
					new ItemDyeable("spraycan_dyeable", KMRP_TAB),
					new ItemDyeable("skirt_dyeable", KMRP_TAB),

					new ItemDyeable("tricorne_dyeable", KMRP_TAB),
//					new ItemDyeable("tubic_dyeable", KMRP_TAB),

					new ItemDyeable("40mil_dyeable", KMRP_TAB),
//					new ItemDyeable("ammo_pack_dyeable", KMRP_TAB),
					new ItemDyeable("pistol_small_dyeable", KMRP_TAB),
					new ItemDyeable("pistol_med_dyeable", KMRP_TAB),
					new ItemDyeable("pistol_big_dyeable", KMRP_TAB),
					new ItemDyeable("rifle_small_dyeable", KMRP_TAB),
					new ItemDyeable("rifle_med_dyeable", KMRP_TAB),
					new ItemDyeable("rifle_big_dyeable", KMRP_TAB),
					new ItemDyeable("shotgun_small_dyeable", KMRP_TAB),
					new ItemDyeable("shotgun_med_dyeable", KMRP_TAB),
					new ItemDyeable("shotgun_big_dyeable", KMRP_TAB),
					new ItemDyeable("pneumat_dyeable", KMRP_TAB),
					new ItemDyeable("ring", KMRP_TAB),
					new ItemDyeable("unlethal_bullet_dyeable", KMRP_TAB),
					new ItemDyeable("eye_dyeable", KMRP_TAB),
					new ItemDyeable("dengi_dyeable", KMRP_TAB),
					new ItemDyeable("nakopitel_dyeable", KMRP_TAB),
					new ItemDyeable("sps_fugas_dyeable", KMRP_TAB),
					new ItemDyeable("sps_bops_dyeable", KMRP_TAB),



			};
			static_itemsdyeable = itemsdyeable;
			for (ItemDyeable item: itemsdyeable) {
				GunCus.proxy.registerItemSimple(registry, item);
			}

			// FOOD ITEMS
			final ItemFoodGCI[] itemsfood = {
					new ItemFoodGCI("fluid_food", KMRP_TAB),
					new ItemFoodGCI("fluid_food1", KMRP_TAB),
					new ItemFoodGCI("fluid_food2", KMRP_TAB),
					new ItemFoodGCI("fluid_food3", KMRP_TAB),
					new ItemFoodGCI("fluid_food4", KMRP_TAB),
					new ItemFoodGCI("fluid_food5", KMRP_TAB),
					new ItemFoodGCI("fluid_food6", KMRP_TAB),
					new ItemFoodGCI("fluid_food7", KMRP_TAB)
			};
			static_itemsfood = itemsfood;
			for (ItemFoodGCI item: itemsfood) {
			    GunCus.proxy.registerItemFoodGCI(registry, item);
			}

			// DYEABLE ITEMS
			final ItemDyeableFood[] itemsdyeablefood = {
					new ItemDyeableFood("fluid_food1", KMRP_TAB)
			};
			static_itemsdyeablefood = itemsdyeablefood;
			for (ItemDyeableFood item: itemsdyeablefood) {
//				GunCus.proxy.registerItemDyeableFood(registry, item);
			}

			final ItemShield[] itemsshield = {
					new ItemShield("ballistic_shield", KMRP_TAB),
					new ItemShield("ballistic_shieldsmall", KMRP_TAB),
					new ItemShield("ballistic_shield2", KMRP_TAB),
					new ItemShield("ballistic_shieldsmall_flash", KMRP_TAB),
					new ItemShield("ballistic_shield2_shock", KMRP_TAB),
					new ItemShield("ballistic_shieldsmall_shock", KMRP_TAB),
					new ItemShield("ballistic_shield2_spikes", KMRP_TAB),
					new ItemShield("ballistic_shieldsmall_spikes", KMRP_TAB),
					new ItemShield("ballistic_shield2_with_flash", KMRP_TAB),
					new ItemShield("ballistic_shield_spikes", KMRP_TAB),
					new ItemShield("ballistic_shield_fire", KMRP_TAB),
					new ItemShield("ballistic_shieldtall", KMRP_TAB),
					new ItemShield("ballistic_shieldraider", KMRP_TAB),
					new ItemShield("ballistic_shieldtall_fire", KMRP_TAB),
					new ItemShield("ballistic_shieldraider_heavy", KMRP_TAB),
					new ItemShield("ballistic_shieldtall_flash", KMRP_TAB),
					new ItemShield("ballistic_shieldraider_spikes", KMRP_TAB),
					new ItemShield("ballistic_shieldtall_shock", KMRP_TAB),
					new ItemShield("ballistic_shield_shock", KMRP_TAB),
					new ItemShield("ballistic_shieldtall_spikes", KMRP_TAB),
					new ItemShield("faustshield", KMRP_TAB),
					new ItemShield("ballons", KMRP_TAB),
					new ItemShield("tituspack", KMRP_TAB),
					new ItemShield("micolashpack", KMRP_TAB),
					new ItemShield("radiopack", KMRP_TAB),
					new ItemShield("police_shield", KMRP_TAB),
					new ItemShield("para", KMRP_TAB),
			};
			static_itemsshield = itemsshield;
			for (ItemShield item: itemsshield) {
				GunCus.proxy.registerItemSimple(registry, item);
			}


			final ItemToggleable[] itemstoggleable = {
					new ItemToggleable("td_closed", KMRP_TAB).setToggleableTo("td_open"),
					new ItemToggleable("td_open", KMRP_TAB).setToggleableTo("td_closed"),
					new ItemToggleable("jiga_closed", KMRP_TAB).setToggleableTo("jiga_open"),
					new ItemToggleable("jiga_open", KMRP_TAB).setToggleableTo("jiga_closed"),
					new ItemToggleable("plasmacut_closed", KMRP_TAB).setToggleableTo("plasmacut_open_off"),
					new ItemToggleable("plasmacut_open_off", KMRP_TAB).setToggleableTo("plasmacut_open_on"),
					new ItemToggleable("plasmacut_open_on", KMRP_TAB).setToggleableTo("plasmacut_closed"),
					new ItemToggleable("scourge", KMRP_TAB).setToggleableTo("scourge2"),
					new ItemToggleable("scourge2", KMRP_TAB).setToggleableTo("scourge"),
					new ItemToggleable("tonfasteel", KMRP_TAB).setToggleableTo("tonfasteel1"),
					new ItemToggleable("tonfasteel1", KMRP_TAB).setToggleableTo("tonfasteel2"),
					new ItemToggleable("tonfasteel2", KMRP_TAB).setToggleableTo("tonfasteel"),
					new ItemToggleable("tonfawood", KMRP_TAB).setToggleableTo("tonfawood1"),
					new ItemToggleable("tonfawood1", KMRP_TAB).setToggleableTo("tonfawood2"),
					new ItemToggleable("tonfawood2", KMRP_TAB).setToggleableTo("tonfawood"),
					new ItemToggleable("vs_balisong", KMRP_TAB).setToggleableTo("vs_balisong_closed"),
					new ItemToggleable("vs_balisong_closed", KMRP_TAB).setToggleableTo("vs_balisong"),


			};
			for (ItemToggleable item: itemstoggleable) {
				GunCus.proxy.registerItemSimple(registry, item);
			}


			for (ArmorPiece armorPiece : ArmorPiece.ARMOR_LIST) {
				GunCus.proxy.registerItem(registry, armorPiece);
			}


		}

		@SubscribeEvent
		public static void registerBlocks(RegistryEvent.Register<Block> event)
		{
			IForgeRegistry<Block> registry = event.getRegistry();

			for(BlockGCI block : BlockGCI.BLOCKS_LIST)
			{

				GunCus.proxy.registerBlock(registry, block);
//				System.out.println("Registering block " + block.getRegistryName());
			}
		}
	}
}
