package de.cas_ual_ty.gci;

import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.item.attachment.Attachment;
import de.cas_ual_ty.gci.item.attachment.EnumAttachmentType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class ContainerGunTable extends Container
{
	public InventoryCrafting craftMatrix = new InventoryCrafting(this, 3, 3);
	
	public EntityPlayer entityPlayer;
	public World world;
	public BlockPos pos;
	
	public Slot gunSlot;
	public SlotAttachment[] attachmentSlots;

	public ContainerGunTable(EntityPlayer entityPlayer, World world, BlockPos pos)
	{
		this.entityPlayer = entityPlayer;
		this.world = world;
		this.pos = pos;
		
		this.gunSlot = new Slot(this.craftMatrix, 4, 80, 35)
		{
			@Override
			public boolean isItemValid(ItemStack itemStack)
			{
				if(!this.getHasStack() && (itemStack.getItem() instanceof ItemGun))
				{
					for(EnumAttachmentType attachmentType : EnumAttachmentType.values())
					{
						if(ContainerGunTable.this.attachmentSlots[attachmentType.getSlot()].getHasStack())
						{
							return false;
						}
					}
					
					return true;
				}
				else
				{
					return false;
				}
			}
		};
		this.addSlotToContainer(this.gunSlot);
		
		this.attachmentSlots = new SlotAttachment[EnumAttachmentType.values().length];
		for(EnumAttachmentType attachmentType : EnumAttachmentType.values())
		{
			this.attachmentSlots[attachmentType.getSlot()] = new SlotAttachment(this.gunSlot, attachmentType.getSlot(), this.craftMatrix, attachmentType.getX() + attachmentType.getY() * 3, 8 + (attachmentType.getX() + 3) * 18, 17 + attachmentType.getY() * 18);
			this.addSlotToContainer(this.attachmentSlots[attachmentType.getSlot()]);
		}
		
		for (int k = 0; k < 3; ++k)
		{
			for (int i1 = 0; i1 < 9; ++i1)
			{
				this.addSlotToContainer(new Slot(entityPlayer.inventory, i1 + k * 9 + 9, 8 + i1 * 18, 84 + k * 18));
			}
		}
		
		for (int l = 0; l < 9; ++l)
		{
			this.addSlotToContainer(new Slot(entityPlayer.inventory, l, 8 + l * 18, 142));
		}
	}
	
	private boolean changing = false;
	private int attachmentAmmount = 0;



	@Override
	public void onCraftMatrixChanged(IInventory inventory)
	{
		if(!this.changing)
		{
			this.changing = true;
			
			if(inventory == this.craftMatrix)
			{
				//WITH GUN
				if(this.gunSlot.getHasStack())
				{
					int attachmentAmmount = 0;
					
					for(EnumAttachmentType attachmentType : EnumAttachmentType.values())
					{
						if(ContainerGunTable.this.attachmentSlots[attachmentType.getSlot()].getHasStack())
						{
							++attachmentAmmount;
						}
					}
					
					ItemStack gunStack = this.gunSlot.getStack();
					ItemGun gun = (ItemGun) gunStack.getItem();
					
					//+GUN -PREV
					if(this.attachmentAmmount == 0)
					{
						//+GUN -PREV -ATTACHMENTS
						if(attachmentAmmount == 0)
						{
							for(EnumAttachmentType attachmentType : EnumAttachmentType.values())
							{
								this.attachmentSlots[attachmentType.getSlot()].putStack(gun.getAttachment(gunStack, attachmentType.getSlot()).createItemStack());
								// KM
								getNBTfromGunToAttachment(this.attachmentSlots[attachmentType.getSlot()].getStack(), gunStack, attachmentType.getSlot());
								// RP
							}
						}
						//+GUN -PREV +ATTACHMENTS
						else
						{
							Attachment attachment;
							
							for(EnumAttachmentType attachmentType : EnumAttachmentType.values())
							{
								if(this.attachmentSlots[attachmentType.getSlot()].getHasStack())
								{
									attachment = (Attachment) this.attachmentSlots[attachmentType.getSlot()].getStack().getItem();
									gun.setAttachment(gunStack, attachmentType.getSlot(), attachment.getID());
									// KM
									getNBTfromGunToAttachment(this.attachmentSlots[attachmentType.getSlot()].getStack(), gunStack, attachmentType.getSlot());
									// RP
								}
								else
								{
									gun.setAttachment(gunStack, attachmentType.getSlot(), 0);
									// KM
									delAttachmentNBTfromGun(gunStack, attachmentType.getSlot());
									// RP
								}
							}
						}
						
						this.attachmentAmmount = 0;
						
						for(EnumAttachmentType attachmentType : EnumAttachmentType.values())
						{
							if(ContainerGunTable.this.attachmentSlots[attachmentType.getSlot()].getHasStack())
							{
								++this.attachmentAmmount;
							}
						}
					}
					//+GUN +PREV
					else
					{
						Attachment attachment;
						
						for(EnumAttachmentType attachmentType : EnumAttachmentType.values())
						{
							if(this.attachmentSlots[attachmentType.getSlot()].getHasStack())
							{
								attachment = (Attachment) this.attachmentSlots[attachmentType.getSlot()].getStack().getItem();
								gun.setAttachment(gunStack, attachmentType.getSlot(), attachment.getID());
								// KM
								getNBTfromGunToAttachment(this.attachmentSlots[attachmentType.getSlot()].getStack(), gunStack, attachmentType.getSlot());
								// RP
							}
							else
							{
								gun.setAttachment(gunStack, attachmentType.getSlot(), 0);
								// KM
								delAttachmentNBTfromGun(gunStack, attachmentType.getSlot());
								// RP
							}
						}
					}
				}
				//WITHOUT GUN
				else
				{
					for(EnumAttachmentType attachmentType : EnumAttachmentType.values())
					{
						this.attachmentSlots[attachmentType.getSlot()].putStack(ItemStack.EMPTY);
					}
					
					this.attachmentAmmount = 0;
				}
			}
			
			super.onCraftMatrixChanged(inventory);
			
			this.changing = false;
		}
	}

	// KM
	public void getNBTfromGunToAttachment(ItemStack attachment, ItemStack gun, int slot) {

		// кажется тут лор переходит из пушки в аттачмент
		if (!attachment.hasTagCompound() && gun.getOrCreateSubCompound("attachments").hasKey(String.valueOf(slot))) {
			attachment.setTagCompound(gun.getOrCreateSubCompound("attachments").getCompoundTag(String.valueOf(slot)));

			// кажется вот тут лор переходит из аттачмента в пушку
		} else if (attachment.hasTagCompound() && !gun.getOrCreateSubCompound("attachments").hasKey(String.valueOf(slot))) {

			// если присоединяется магазин

			// DANGEROUS! a lot of hardcoded strings depending on km_lore
//			if (attachment.getTagCompound().hasKey("module") &&
//					attachment.getTagCompound().getCompoundTag("module").getString("type").contains("магазин")) {
//				NBTTagList chamber = gun.getTagCompound().getCompoundTag("rangedFirearm").getTagList("loadedProjectilesList", 10);
//				if (chamber.tagCount() < gun.getTagCompound().getCompoundTag("rangedFirearm").get)
//
//			}

			if (attachment.getTagCompound() != null) {
				gun.getOrCreateSubCompound("attachments").setTag(String.valueOf(slot), attachment.getTagCompound());
				//теги оружия
				if(gun.getTagCompound() != null && attachment.getTagCompound().hasKey("weapontags")) {
					for (String weaponKey : attachment.getTagCompound().getCompoundTag("weapontags").getKeySet()) {
						NBTTagCompound weapon = attachment.getTagCompound().getCompoundTag("weapontags").getCompoundTag(weaponKey).copy();
						weapon.setInteger("attachment", slot);
						//штык момент
						if(weapon.hasKey("melee") && slot == 3) {
							if(weapon.getCompoundTag("melee").getInteger("reach") == 1) {
								weapon.getCompoundTag("melee").setInteger("reach", 2);
							}
						}
						if(weapon.hasKey("skillSaver")) weapon.removeTag("skillSaver");
						if(!gun.getOrCreateSubCompound("weapontags").hasKey(weaponKey)) gun.getOrCreateSubCompound("weapontags").setTag(weaponKey, weapon);
					}
				}
			}
		}
	}

	public void delAttachmentNBTfromGun(ItemStack gun, int slot) {
		if (gun.getTagCompound() != null) {
			gun.getTagCompound().getCompoundTag("attachments").removeTag(String.valueOf(slot));
			//теги оружия
			if(gun.getTagCompound().hasKey("weapontags")) {
				List<String> toDel = new ArrayList<>();;
				for (String weaponKey : gun.getTagCompound().getCompoundTag("weapontags").getKeySet()) {
					NBTTagCompound weapon = gun.getTagCompound().getCompoundTag("weapontags").getCompoundTag(weaponKey);
					if(weapon.hasKey("attachment") && weapon.getInteger("attachment") == slot) {
						toDel.add(weaponKey);
					}
				}
				for (String weaponKey : toDel) {
					gun.getTagCompound().getCompoundTag("weapontags").removeTag(weaponKey);
				}
			}
		}
	}


	// RP

	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		if (this.world.getBlockState(this.pos).getBlock() != GunCus.BLOCK_GUN_TABLE)
		{
			//KM return false;
			return true;//RP
		}
		else
		{
			return playerIn.getDistanceSq(this.pos.getX() + 0.5D, this.pos.getY() + 0.5D, this.pos.getZ() + 0.5D) <= 64.0D;
		}
	}
	
	protected void clearContainer(EntityPlayer playerIn, World worldIn)
	{
		if (!playerIn.isEntityAlive() || playerIn instanceof EntityPlayerMP && ((EntityPlayerMP)playerIn).hasDisconnected())
		{
			if(this.gunSlot.getHasStack())
			{
				playerIn.dropItem(this.gunSlot.getStack(), false);
			}
			else
			{
				for(SlotAttachment slot : this.attachmentSlots)
				{
					playerIn.dropItem(slot.getStack(), false);
				}
			}
		}
		else
		{
			if(this.gunSlot.getHasStack())
			{
				playerIn.inventory.placeItemBackInInventory(worldIn, this.gunSlot.getStack());
			}
			else
			{
				for(SlotAttachment slot : this.attachmentSlots)
				{
					playerIn.inventory.placeItemBackInInventory(worldIn, slot.getStack());
				}
			}
		}
	}
	
	@Override
	public void onContainerClosed(EntityPlayer playerIn)
	{
		super.onContainerClosed(playerIn);
		
		if (!this.world.isRemote)
		{
			this.clearContainer(playerIn, this.world);
		}
	}
	
	
	@Override
	public ItemStack slotClick(int slotId, int dragType, ClickType clickTypeIn, EntityPlayer player)
	{
		return super.slotClick(slotId, dragType, clickTypeIn, player);
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
	{
		Slot slot = this.getSlot(index);
		ItemStack itemStack = slot.getStack();
		
		if(index >= 9)
		{
			if(itemStack.getItem() instanceof ItemGun)
			{
				if(this.gunSlot.isItemValid(itemStack))
				{
					this.gunSlot.putStack(itemStack);
					slot.putStack(ItemStack.EMPTY);
				}
			}
			else if(itemStack.getItem() instanceof Attachment)
			{
				Attachment attachment = (Attachment) itemStack.getItem();
				
				if(this.attachmentSlots[attachment.getSlot()].isItemValid(itemStack))
				{
					this.attachmentSlots[attachment.getSlot()].putStack(itemStack);
					slot.putStack(ItemStack.EMPTY);
				}
			}
		}
		else
		{
			if(playerIn.inventory.addItemStackToInventory(itemStack))
			{
				slot.putStack(ItemStack.EMPTY);
			}
		}
		
		return ItemStack.EMPTY;
	}
	
	public static class SlotAttachment extends Slot
	{
		public Slot main;
		public int slot;
		
		public SlotAttachment(Slot main, int slot, IInventory inventory, int id, int x, int y)
		{
			super(inventory, id , x, y);
			
			this.main = main;
			this.slot = slot;
		}
		
		@Override
		public boolean isItemValid(ItemStack itemStack)
		{
			if(this.main.getHasStack() && !this.getHasStack() && (itemStack.getItem() instanceof Attachment))
			{
				ItemGun gun = (ItemGun) this.main.getStack().getItem();
				Attachment attachment = (Attachment) itemStack.getItem();
				
				return (attachment.getSlot() == this.slot) && gun.canSetAttachment(attachment);
			}
			else
			{
				return false;
			}
		}
	}
}
