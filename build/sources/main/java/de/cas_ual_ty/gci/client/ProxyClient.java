package de.cas_ual_ty.gci.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.KeyHandler;
import de.cas_ual_ty.gci.Proxy;
import de.cas_ual_ty.gci.block.BlockGCI;
import de.cas_ual_ty.gci.client.render.layers.LayersRegister;
import de.cas_ual_ty.gci.item.ItemBlockGCI;
import de.cas_ual_ty.gci.item.ItemFoodGCI;
import de.cas_ual_ty.gci.item.ItemGCI;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.item.armor.ArmorPiece;
import de.cas_ual_ty.gci.item.attachment.Attachment;
import de.cas_ual_ty.gci.item.attachment.EnumAttachmentType;

import de.cas_ual_ty.gci.item.dyeable.DyeColor;
import de.cas_ual_ty.gci.item.dyeable.ItemDyeableFood;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.registries.IForgeRegistry;

import static de.cas_ual_ty.gci.KeyHandler.*;

public class ProxyClient extends Proxy
{
	public static File mpmdir;

	public void registerItemRenderer(ItemGCI item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(GunCus.MOD_ID + ":" + item.getModelRL(), "inventory"));
	}
	
	public void registerItemRenderer(ItemBlockGCI item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(GunCus.MOD_ID + ":" + item.getModelRL(), "inventory"));
	}

	public void registerItemRenderer(ArmorPiece item)
	{
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(GunCus.MOD_ID + ":" + "armor/" + item.getModelRL(), "inventory"));
	}


	@Override
	public void registerItem(IForgeRegistry<Item> registry, ItemGCI item)
	{
		super.registerItem(registry, item);
		this.registerItemRenderer(item);
	}
	
	@Override
	public void registerItem(IForgeRegistry<Item> registry, ItemBlockGCI item)
	{
		super.registerItem(registry, item);
		this.registerItemRenderer(item);
	}


	@Override
	public void registerItem(IForgeRegistry<Item> registry, ArmorPiece item)
	{
		super.registerItem(registry, item);
		this.registerItemRenderer(item);

	}

	@Override
	public void registerItemSimple(IForgeRegistry<Item> registry, ItemGCI item)
	{
		super.registerItem(registry, item);
		ModelResourceLocation main = new ModelResourceLocation(GunCus.MOD_ID + ":" + "simple/" + item.getModelRL(), "inventory");
		ModelLoader.setCustomModelResourceLocation(item, 0, main);

	}

	@Override
	public void registerItemFoodGCI(IForgeRegistry<Item> registry, ItemFoodGCI item)
	{
		super.registerItem(registry, item);
		ModelResourceLocation main = new ModelResourceLocation(GunCus.MOD_ID + ":" + "simple/" + item.getModelRL(), "inventory");
		ModelLoader.setCustomModelResourceLocation(item, 0, main);

	}

	@Override
	public void registerItemDyeableFood(IForgeRegistry<Item> registry, ItemDyeableFood item)
	{
		super.registerItem(registry, item);
		ModelResourceLocation main = new ModelResourceLocation(GunCus.MOD_ID + ":" + "simple/" + item.getModelRL(), "inventory");
		ModelLoader.setCustomModelResourceLocation(item, 0, main);

	}


	@Override
	public void registerGun(IForgeRegistry<Item> registry, ItemGun gun)
	{
		super.registerGun(registry, gun);
		
		ModelResourceLocation main = new ModelResourceLocation(GunCus.MOD_ID + ":" + gun.getModelRL() + "/gun", "inventory");
		ModelLoader.setCustomModelResourceLocation(gun, 0, main);
		
		ArrayList<ModelResourceLocation> list = new ArrayList<ModelResourceLocation>();
		list.add(main);
		
		int i;
		int j;
		Attachment attachment;
		
		for(i = 0; i < EnumAttachmentType.values().length; ++i) //All layers
		{
			for(j = 0; j < Attachment.getAmmountForSlot(i); ++j) //All attachments per layer
			{
				if(gun.canSetAttachment(i, j)) //Check if attachment is compatible
				{
					attachment = Attachment.getAttachment(i, j);
					
					if(attachment != null && attachment.shouldLoadModel()) //null-attachment exists, as well as some which are not visible
					{
						list.add(new ModelResourceLocation(GunCus.MOD_ID + ":" + gun.getModelRL() + "/" + attachment.getModelRL(), "inventory")); //Add MRL to the list
					}
				}
			}
		}
		
		list.add(new ModelResourceLocation(GunCus.MOD_ID + ":" + gun.getModelRL() + "/aim", "inventory"));
		
		ModelBakery.registerItemVariants(gun, list.toArray(new ModelResourceLocation[list.size()])); //Register all attachment MRLs found so that they will be loaded
	}
	
	@Override
	public void registerBlock(IForgeRegistry<Block> registry, BlockGCI block)
	{
		super.registerBlock(registry, block);
	}
	
	@Override
	public void preInit(FMLPreInitializationEvent event)
	{
		super.preInit(event);
		
		//		generateAttachmentModels(); //TODO
		File dir = new File(event.getModConfigurationDirectory(), "..");

		this.mpmdir = new File(dir,"moreplayermodels");
		if(!this.mpmdir.exists())
			this.mpmdir.mkdir();
		
		MinecraftForge.EVENT_BUS.register(new BakeHandler());
	}
	
	@Override
	public void init(FMLInitializationEvent event)
	{
		super.init(event);
		
		//		RenderingRegistry.registerEntityRenderingHandler(EntityBullet.class, new RenderFactoryGCI());
		
		MinecraftForge.EVENT_BUS.register(new EventHandlerClient());
		LayersRegister.register();

		MinecraftForge.EVENT_BUS.register(new KeyHandler());
		ClientRegistry.registerKeyBinding(openKey);
		ClientRegistry.registerKeyBinding(toggleKey);
		ClientRegistry.registerKeyBinding(gunCusKey);
		ClientRegistry.registerKeyBinding(aimKey);

		/*MinecraftForge.EVENT_BUS.register(new ColorHandler());*/
		Minecraft.getMinecraft().getItemColors().registerItemColorHandler(new DyeColor(), GunCus.static_itemsdyeable);
		Minecraft.getMinecraft().getItemColors().registerItemColorHandler(new DyeColor(), GunCus.static_itemsdyeablefood);
		/*ClientRegistry.registerKeyBinding(containerKey);*/
	}
	
	@Override
	public EntityPlayer getClientPlayer(MessageContext ctx)
	{
		return Minecraft.getMinecraft().player;
	}
	
	public static void generateAttachmentModels()
	{
		for(ItemGun gun : ItemGun.GUNS_LIST)
		{
			try
			{
				ProxyClient.generateAttachmentModelsForGun(gun);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public static void generateAttachmentModelsForGun(String path, ItemGun gun) throws IOException
	{
		String gunName = gun.getModelRL();
		
		File baseAttachmentFile = new File(path + "/models/item/base/attachment_name.json");
		File baseGunFile = new File(path + "/models/item/base/gun_name.json");
		
		if(baseAttachmentFile.exists())
		{
			ArrayList<String> lines = new ArrayList<String>();
			
			String line;
			
			BufferedReader reader = new BufferedReader(new FileReader(baseAttachmentFile));
			
			while ((line = reader.readLine()) != null)
			{
				lines.add(line.replaceAll("gun_name", gunName));
			}
			
			reader.close();
			
			String itemDirPath = path + "/models/item/" + gun.getModelRL();
			File itemDir = new File(itemDirPath);
			
			if(!itemDir.exists())
			{
				itemDir.mkdirs();
			}
			
			int j;
			Attachment attachment;
			BufferedWriter writer;
			
			for(int i = 0; i < EnumAttachmentType.values().length; ++i)
			{
				for(j = 0; j < Attachment.getAmmountForSlot(i); ++j)
				{
					if(gun.canSetAttachment(i, j))
					{
						attachment = Attachment.getAttachment(i, j);
						
						if(attachment != null && attachment.shouldLoadModel())
						{
							String attachmentModelPath = itemDirPath + "/" + attachment.getModelRL() + ".json";
							File attachmentModel = new File(attachmentModelPath);
							
							if(!attachmentModel.exists())
							{
								attachmentModel.createNewFile();
								
								writer = new BufferedWriter(new FileWriter(attachmentModel));
								
								for(String line1 : lines)
								{
									writer.write(line1.replaceAll("attachment_name", attachment.getModelRL()));
									writer.newLine();
								}
								
								writer.close();
							}
						}
					}
				}
			}
			
			File gunModel = new File(itemDirPath + "/gun.json");
			
			if(!gunModel.exists())
			{
				gunModel.createNewFile();
				
				lines.clear();
				
				reader = new BufferedReader(new FileReader(baseGunFile));
				
				while ((line = reader.readLine()) != null)
				{
					lines.add(line.replaceAll("gun_name", gunName));
				}
				
				reader.close();
				
				writer = new BufferedWriter(new FileWriter(gunModel));
				
				for(String line1 : lines)
				{
					writer.write(line1);
					writer.newLine();
				}
				
				writer.close();
			}
		}
		else
		{
			baseAttachmentFile.mkdirs();
			baseAttachmentFile.createNewFile();
		}
	}
	
	public static void generateAttachmentModelsForGun(ItemGun gun) throws IOException
	{
		ProxyClient.generateAttachmentModelsForGun("C:\\Minecraft Coding 1.12.2\\src\\main\\resources\\assets\\gci", gun);
	}


}
